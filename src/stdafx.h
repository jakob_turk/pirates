// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <iostream>
#include <thread>
#include <fstream>
#include <vector>
#include <list>
#include <map>
#include <cassert>
#include <sstream>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <SFML/Audio.hpp>

struct sfVector2Icompare
{
	bool operator() (const sf::Vector2i& a, const sf::Vector2i& b) const
	{
		return a.y < b.y || (a.y == b.y && a.x < b.x);
	}
};

inline sf::Vector2f fVec(sf::Vector2i vect)
{
	return sf::Vector2f(vect);
}

inline sf::Vector2i iVec(sf::Vector2f vect)
{
	return sf::Vector2i(vect);
}

inline float dotF(sf::Vector2f vect1, sf::Vector2f vect2)
{
	return vect1.x * vect2.x + vect1.y * vect2.y;
}

inline float dotI(sf::Vector2i vect1, sf::Vector2i vect2)
{
	return (float)(vect1.x * vect2.x + vect1.y * vect2.y);
}

inline float lengthF(sf::Vector2f vec)
{
	return std::sqrt(dotF(vec, vec));
}

inline sf::Vector2f normalize(sf::Vector2f vec)
{
	if (vec == sf::Vector2f())
		return vec;

	float len = std::sqrt(vec.x * vec.x + vec.y * vec.y);

	return sf::Vector2f(vec.x/len, vec.y/len);
}

inline float inRad(float angle) {
	return angle * (3.1415926f / 180.0f);
}

inline float inDeg(float angle) {
	return angle * (180.0f / 3.1415926f);
}

inline sf::Vector2f rotated(sf::Vector2f point, float angle) {
	sf::Transform rotation;
	rotation.rotate(angle);
	return rotation.transformPoint(point);
}