#pragma once

#include "Menu.h"

class MenuManager
{
public:
	MenuManager();
	~MenuManager();

	void LoadMenus();

	void ChangeMenu(Menu::MenuType);

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& window);

private:
	std::map<Menu::MenuType, Menu*> _menus;
	Menu* _currentMenu;
};