#include "stdafx.h"
#include "Animation.h"
#include "Game.h"

Animation::Animation(sf::Vector2f position, sf::Vector2f velocity) :
	_currentFrame(0),
	_sinceFrameUpdate(0.0f),
	_position(position),
	_velocity(velocity),
	_angle(0.0f)
{
	//Child has to call SetPosition or Set Velocity before it's own load, otherwise the default (0, 0)
	//is saved. If called after Load, LoadFromSprite (if used) is called with default (0, 0) position.
}

Animation::~Animation()
{
}

void Animation::Load(int zLevel, bool noAutoAdd)
{
	if (!noAutoAdd)
		Game::GetGameObjectManager().Add(this);

	SetLoaded();

	SetZLevel(zLevel);
}

void Animation::Draw(sf::RenderWindow & window)
{
	if (IsLoaded())
	{
		window.draw(_frames[_currentFrame]);
	}
}

void Animation::LoadFrameSprite(std::string textureName, sf::Vector2f position, float angle)
{
	sf::Sprite frameSprite;
	ServiceLocator::GetTextures()->SetTexture(textureName, frameSprite);
	frameSprite.setOrigin(frameSprite.getLocalBounds().width / 2, frameSprite.getLocalBounds().height / 2);
	frameSprite.setPosition(position);
	if (angle != 0.0f)
		frameSprite.setRotation(inDeg(angle) - 90.0f);

	_frames.push_back(frameSprite);
}

void Animation::NextFrame(bool cicle)
{
	if (cicle) {
		++_currentFrame;

		if (_currentFrame >= _frames.size())
			_currentFrame = 0;
	}
	
	else if (_currentFrame + 1 >= _frames.size())
		SetDeletion();
	else
		++_currentFrame;
	
}

void Animation::SetScale(float xScale, float yScale)
{
	for (std::vector<sf::Sprite>::iterator fItr = _frames.begin(); fItr != _frames.end(); fItr++) {
		fItr->setScale(xScale, yScale);
	}
}

void Animation::SetRotation(float angle)
{
	for (std::vector<sf::Sprite>::iterator fItr = _frames.begin(); fItr != _frames.end(); fItr++) {
		fItr->setRotation(inDeg(angle));
	}
}

void Animation::SetAngle(float angle)
{
	_angle = angle;
}

void Animation::SetPosition(sf::Vector2f position)
{
	_position = position;
}

void Animation::SetVelocity(sf::Vector2f velocity)
{
	_velocity = velocity;
}

float Animation::GetAngle()
{
	return _angle;
}

sf::Vector2f Animation::GetPosition()
{
	return _position;
}

sf::Vector2f Animation::GetVelocity()
{
	return _velocity;
}