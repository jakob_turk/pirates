#pragma once
#include "IWindowProvider.h"

class SFMLWindow :
	public IWindowProvider
{
public:
	SFMLWindow();

	sf::RenderWindow& GetWindow();
	sf::View& GetViewport();
	sf::View& GetMinimapViewPort();

	void SetUpMinimapViewPort(float width, float height);
	void MoveViewport(sf::Vector2f moveVector, float factor = 1);
	void MoveViewportTo(sf::Vector2f newPosition);
	void SetViewport(Viewport newViewport = Default);
	void ZoomViewport(float factor);
	void ResetZoom();
	float GetZoom();

private:
	sf::RenderWindow _window;
	sf::View _viewPort;
	sf::View _minimapView;

	float _zoom;
};