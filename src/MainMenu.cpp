#include "stdafx.h"
#include "MainMenu.h"

#include "Game.h"
#include "ServiceLocator.h"
#include "BoatFactory.h"

MainMenu::MainMenu()
{
	MakeButtons();
}

void MainMenu::Call()
{
	if (!Game::GetGameObjectManager().Get("MenuBoat"))
		BoatFactory::Make(sf::Vector2f((float)(Config::SCREEN_WIDTH / 2), (float)(Config::SCREEN_HEIGHT / 2)),
			AI, BoatFaction::fDefault, (float)(std::rand() % 360), sf::Vector2f(), "MenuBoat");
}

void MainMenu::MakeButtons()
{
	sf::Vector2f playPosition = sf::Vector2f((float)(Config::SCREEN_WIDTH / 2), (float)(Config::SCREEN_HEIGHT / 5));
	_buttons[Play] = new Button(playPosition, Brown, sf::Vector2f(), new Text("Play", playPosition, 40, sf::Color::White));

	sf::Vector2f settingsPosition = sf::Vector2f((float)(Config::SCREEN_WIDTH / 2), (float)(Config::SCREEN_HEIGHT * 2 / 5));
	_buttons[SettingsButton] = new Button(settingsPosition, Brown, sf::Vector2f(), new Text("Settings", settingsPosition, 38, sf::Color::White));

	sf::Vector2f quitPosition = sf::Vector2f((float)(Config::SCREEN_WIDTH / 2), (float)(Config::SCREEN_HEIGHT * 3 / 5));
	_buttons[Quit] = new Button(quitPosition, Brown, sf::Vector2f(), new Text("Quit", quitPosition, 40, sf::Color::White));
}

void MainMenu::DealWithButtonClick(ButtonType button)
{
	switch (button) {

	case Play:
		Game::GetMenuManager().ChangeMenu(Sandbox);
		break;

	case SettingsButton:
		Game::GetMenuManager().ChangeMenu(Settings);
		break;

	case Quit:
		Game::ChangeGameState(GameState::Exiting);
		break;
	}
}
