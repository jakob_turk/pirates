#pragma once
#include "UIelement.h"

class ValueBar:
	public UIelement
{
public:
	ValueBar();

	void Load(sf::Vector2f position = sf::Vector2f(), float length = 0.0f, float fullValue = 0.0f,  
			  bool orientation = 0, BarColour colour = White, float scaleAcross = 1.0f); //orientation: 0 == horizontal, 1 == vertical

	void Draw(sf::RenderWindow & window);
	void Resize(float valueChange = 0.0f);

protected:
	float _fullValue;
	float _value;

private:
	sf::Sprite _barStart;
	sf::Sprite _barMid;
	sf::Sprite _barEnd;

	bool _orientation;
	sf::Vector2f _position;
	float _textureLength;
	float _fullLength;
	float _scaleAcross;
};