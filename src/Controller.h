#pragma once
class Boat;

class Controller
{
public:
	virtual void Control(Boat& boat) = 0;
};