#pragma once
#include "UIelement.h"

class BackgroundSheet :
	public UIelement
{
public:
	BackgroundSheet(sf::Vector2f position, float width, float height, BtnColour colour = Brown);
};