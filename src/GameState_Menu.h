#pragma once
#include "GameState.h"

class GS_Menu :
	public GameState
{
public:
	GS_Menu();
	~GS_Menu();

	void Call();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& window);
};