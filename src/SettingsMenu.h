#pragma once
#include "Menu.h"

#include "BackgroundSheet.h"

class SettingsMenu :
	public Menu
{
public:
	SettingsMenu();

protected:
	void MakeButtons();
	void DealWithButtonClick(ButtonType button);
};