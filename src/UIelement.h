#pragma once
#include "VisibleObject.h"

//The only purpouse of this subclass is to provide UIelements with the colour enums and move
//Update back out of VisibleObject (most of UIelements might just stand there, so they should
//just use this Update()

enum BtnColour {
	Brown, Beige, Gray, LBrown
};

enum BarColour {
	White, Red, Blue, Green, Yellow
};

class UIelement :
	public VisibleObject
{
public:
	UIelement();

	void Update(float elapsedTime);

	void SetShowing(bool show = true);
	bool IsShowing();

private:
	bool _showing;
};