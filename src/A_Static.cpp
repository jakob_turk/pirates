#include "stdafx.h"
#include "A_Static.h"

A_Static::A_Static(sf::Vector2f position, float angle, std::string textureName, int zLevel, bool noAutoAdd)
{
	SetPosition(position);
	SetAngle(angle);
	Load(textureName, zLevel, noAutoAdd);
}

A_Static::~A_Static()
{
}

void A_Static::Load(std::string textureName, int zLevel, bool noAutoAdd) {

	//Load Simple VisibleObject
	LoadFrameSprite(textureName, GetPosition(), GetAngle());

	Animation::Load(zLevel, noAutoAdd);
}

void A_Static::Update(float timeDelta) {}