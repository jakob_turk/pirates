#include "stdafx.h"
#include "FlameProjText.h"

#include "Game.h"

FlameProjText::FlameProjText(sf::Vector2f position, int textSize, sf::Color textColor) :
	Text("0", position, textSize, textColor)
{
}

void FlameProjText::Update(float elapsedTime)
{
	Boat* playerPtr = Game::GetGameObjectManager().Get("PlayerBoat");
	if (playerPtr) {
		if (playerPtr->GetCannon()->GetAmmo(Projectile::FireProj) != std::stoi(GetText()))
		{
			UpdateText(std::to_string(playerPtr->GetCannon()->GetAmmo(Projectile::FireProj)));
		}
	}
}
