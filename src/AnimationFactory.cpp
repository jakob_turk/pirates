#include "stdafx.h"
#include "AnimationFactory.h"

#include "A_Static.h"
#include "A_Explosion.h"
#include "A_Flame.h"
#include "A_Splash.h"
#include "A_Crew.h"

Animation* AnimationFactory::Make(Anim animationName, sf::Vector2f position, float angle, int zLevel,
								  float frameLen, float length, sf::Vector2f velocity) {
	
	Animation* new_anim = NULL;

	switch (animationName) {

	case Explosion:
		if (frameLen == 0.0f)
			frameLen = 0.2f;
		new_anim = new A_Explosion(position, velocity, frameLen, true);
		break;

	case ShortExplosion:
		if (frameLen == 0.0f)
			frameLen = 0.2f;
		new_anim = new A_Explosion(position, velocity, frameLen, false);
		break;

	case Flame1:
		if (length == 0.0f)
			length = 5.0f;
		if (frameLen == 0.0f)
			frameLen = 0.2f;
		new_anim = new A_Flame(position, velocity, length, false, frameLen, angle, zLevel);
		break;

	case Flame2:
		if (length == 0.0f)
			length = 5.0f;
		if (frameLen == 0.0f)
			frameLen = 0.2f;
		new_anim = new A_Flame(position, velocity, length, true, frameLen, angle, zLevel);
		break;

	case Flame:
		if (length == 0.0f)
			length = 5.0f;
		if (frameLen == 0.0f)
			frameLen = 0.2f;
		new_anim = new A_Flame(position, velocity, length, true, frameLen, angle, zLevel);
		new_anim = new A_Flame(position + sf::Vector2f(0.0f, 4.0f), velocity, length, false, frameLen, angle, zLevel);
		break;

	case Splash:
		new_anim = new A_Splash(position);
		break;

	case Crew:
		new_anim = MakeCrew(position, velocity, length, 0, frameLen);
		break;
	case Crew1:
		new_anim = MakeCrew(position, velocity, length, 1, frameLen);
		break;
	case Crew2:
		new_anim = MakeCrew(position, velocity, length, 2, frameLen);
		break;
	case Crew3:
		new_anim = MakeCrew(position, velocity, length, 3, frameLen);
		break;
	case Crew4:
		new_anim = MakeCrew(position, velocity, length, 4, frameLen);
		break;
	case Crew5:
		new_anim = MakeCrew(position, velocity, length, 5, frameLen);
		break;
	case Crew6:
		new_anim = MakeCrew(position, velocity, length, 6, frameLen);
		break;

	case DefaultShipSunken:
		new_anim = new A_Static(position, angle, "defaultShipSunk", 1);
		break;
	case RedShipSunken:
		new_anim = new A_Static(position, angle, "redShipSunk", 1);
		break;
	case GreenShipSunken:
		new_anim = new A_Static(position, angle, "greenShipSunk", 1);
		break;
	case BlueShipSunken:
		new_anim = new A_Static(position, angle, "blueShipSunk", 1);
		break;
	case YellowShipSunken:
		new_anim = new A_Static(position, angle, "yellowShipSunk", 1);
		break;
	case BlackShipSunken:
		new_anim = new A_Static(position, angle, "blackShipSunk", 1);
		break;
	}

	return new_anim;
}

Animation* AnimationFactory::MakeCrew(sf::Vector2f position, sf::Vector2f velocity, float length, int crewMember, float frameLen) {
	if (length == 0.0f)
		length = 5.0f;
	if (frameLen == 0.0f)
		frameLen = 0.2f;
	return new A_Crew(position, velocity, length, crewMember, frameLen);
}