#include "stdafx.h"
#include "A_Flame.h"

A_Flame::A_Flame(sf::Vector2f position, sf::Vector2f velocity, float length, bool flameType, float frameLen, float angle, int zLevel) :
	_flameType(flameType),
	_length(length),
	_duration(0.0f)
{
	_frameLen = frameLen;
	SetPosition(position);
	SetVelocity(velocity);
	Load(zLevel, angle);
}

A_Flame::~A_Flame()
{
}

void A_Flame::Load(int zLevel, float angle)
{
	//If flameType == 0 (false), load flame1 (smaller orange), if 1 (true), load flame2 (bigger yellow)
	//The left/right flippage of the frames are offset by one, so it looks cooler when both are aimated, 1 over 2
	if (!_flameType) {
		LoadFrameSprite("flame1", GetPosition(), angle);
		LoadFrameSprite("flame1f", GetPosition(), angle);
	} else {
		LoadFrameSprite("flame2", GetPosition(), angle);
		LoadFrameSprite("flame2f", GetPosition(), angle);
	}

	Animation::Load(zLevel);
}

void A_Flame::Update(float timeDelta)
{
	for (std::vector<sf::Sprite>::iterator fItr = _frames.begin(); fItr != _frames.end(); fItr++) {
		fItr->move(GetVelocity() * timeDelta);
	}

	_sinceFrameUpdate += timeDelta;
	_duration += timeDelta;

	if (_duration > _length) SetDeletion();
	else if (_sinceFrameUpdate > _frameLen) {
		_sinceFrameUpdate = 0;
		NextFrame(true);
	}
}