#include "stdafx.h"
#include "A_Splash.h"

A_Splash::A_Splash(sf::Vector2f position)
{
	_frameLen = 0.2f;
	SetPosition(position);
	Load();
}

A_Splash::~A_Splash()
{
}

void A_Splash::Load() {

	//Load Simple VisibleObjects
	LoadFrameSprite("splash1", GetPosition());
	LoadFrameSprite("splash2", GetPosition());
	LoadFrameSprite("splash3", GetPosition());

	Animation::Load(4);
}

void A_Splash::Update(float timeDelta) {

	_sinceFrameUpdate += timeDelta;
	if (_sinceFrameUpdate > _frameLen) {
		_sinceFrameUpdate = 0;
		NextFrame();
	}
}