#pragma once

class UIfactory
{
public:
	UIfactory();

	enum UIelement {
		playerHPbar
	};

	static void Make(UIelement element);
};