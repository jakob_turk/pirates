#pragma once
#include "GameState.h"

class GS_Load :
	public GameState
{
public:
	GS_Load();
	~GS_Load();

	void Call();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& window);
};
