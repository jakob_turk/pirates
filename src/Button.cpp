#include "stdafx.h"
#include "Button.h"

#include "ServiceLocator.h"

Button::Button(sf::Vector2f position, BtnColour color, sf::Vector2f size, Text* textObject) :
	_clicked(false),
	_hovered(false),
	_justClicked(false),
	_buttonPicture(NULL),
	_buttonText(textObject),
	_position(position),
	_textPosition(position),
	_picturePosition(position),
	_size(size)
{
	LoadButtonColor(color);
	SetPosition(position);

	if (size != sf::Vector2f())
		SetSize(size);

	//Button text
	/////////////
	if (textObject) {
		_buttonText = textObject;
		_textPosition = textObject->GetPosition();
	}
}

Button::~Button()
{
	if (_buttonText)
		delete _buttonText;

	if (_buttonPicture)
		delete _buttonPicture;
}

void Button::AddPicture(std::string textureName)
{
	_buttonPicture = new Simple(textureName);
	_picturePosition = _position +
		sf::Vector2f(0.0f, -GetSprite().getGlobalBounds().height * 2.0f / 49.0f);

	float scaleFactor = (GetHeight() * 37.0f / 49.0f) / _buttonPicture->GetHeight();
	_buttonPicture->SetPosition(_picturePosition);
	_buttonPicture->SetScale(sf::Vector2f(scaleFactor, scaleFactor));
}

bool Button::Hover()
{
	if (!_hovered) {
		LoadTexture(_hoverTexture);
		if (_size != sf::Vector2f())
			SetSize(_size);

		if (_clicked) {
			SetPosition(_position);
			if (_buttonText)
				_buttonText->SetPosition(_textPosition);
			if (_buttonPicture)
				_buttonPicture->SetPosition(_picturePosition);

			_justClicked = true;
		}

		_hovered = true;
		_clicked = false;

	}
	else if (_justClicked) {
		_justClicked = false;
		return true;
	}

	return false;
}

void Button::Click()
{
	if (!_clicked) {
		LoadTexture(_clickTexture);
		if (_size != sf::Vector2f())
			SetSize(sf::Vector2f(_size.x, _size.y * 45.0f / 49.0f));

		float yMove = GetSprite().getGlobalBounds().height;
		SetPosition(_position + sf::Vector2f(0.0f, yMove * 4.0f / 45.0f));
		if (_buttonText)
			_buttonText->SetPosition(_textPosition + sf::Vector2f(0.0f, yMove * 4.0f / 45.0f));
		if (_buttonPicture)
			_buttonPicture->SetPosition(_picturePosition + sf::Vector2f(0.0f, yMove * 4.0f / 45.0f));
			
		_clicked = true;
		_hovered = false;
	}
}

void Button::Clear()
{
	if (_clicked || _hovered) {
		LoadTexture(_releaseTexture);
		if (_size != sf::Vector2f())
			SetSize(_size);

		if (_clicked) {
			SetPosition(_position);
			if (_buttonText)
				_buttonText->SetPosition(_textPosition);
			if (_buttonPicture)
				_buttonPicture->SetPosition(_picturePosition);
		}

		_clicked = false;
		_hovered = false;
	}
}

void Button::Update(float elapsedTime) {}

void Button::Draw(sf::RenderWindow & renderWindow)
{
	VisibleObject::Draw(renderWindow);

	if (_buttonPicture)
		_buttonPicture->Draw(renderWindow);
	if (_buttonText)
		_buttonText->Draw(renderWindow);
}

void Button::LoadButtonColor(BtnColour color)
{
	//if clicked/hovered textures are not provided, the string atributes are set to released texture
	//they shouldn't ever be called, if not provided, but in any case:
	switch (color) {
	case Brown:
		_releaseTexture = "brownBtnLong";
		_hoverTexture = "brownBtnLong";
		_clickTexture = "brownBtnLongP";
		break;
	case LBrown:
		_releaseTexture = "beigeBtnLong";
		_hoverTexture = "beigeBtnLong";
		_clickTexture = "beigeBtnLongP";
		break;
	case Gray:
		_releaseTexture = "grayBtnLong";
		_hoverTexture = "grayBtnLong";
		_clickTexture = "grayBtnLongP";
		break;
	case Beige:
		_releaseTexture = "whiteBtnLong";
		_hoverTexture = "whiteBtnLong";
		_clickTexture = "whiteBtnLongP";
		break;
	}

	Load(_releaseTexture);
}
