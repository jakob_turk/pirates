#pragma once
#include "VisibleObject.h"

class Dot :
	public VisibleObject
{
public:
	Dot(int i, VisibleObject* obj = NULL, sf::Vector2f position = sf::Vector2f(), float scale = 1.0f, sf::Color color = sf::Color::Red);
	~Dot();

	void Update(float elapsedTime);

private:
	int _i;
	VisibleObject* _objectFollowing;
};