#include "stdafx.h"
#include "ViewControll.h"

#include "Game.h"

void ViewControll::Update(float elapsedTime)
{
	// Check for Esc click
	if (ServiceLocator::GetController()->IsKeyPressed(Esc))
		Game::ChangeGameState(GameState::Menu);

	//VIEWPORT CONTROLS
	///////////////////
	// If cursor at edge of screen
	// Config::SCREEN_MOVE_SENSITIVITY is the % of screen that registers a move
	if (ServiceLocator::GetController()->GetMousePosition().x < Config::SCREEN_MOVE_SENSITIVITY * Config::SCREEN_WIDTH / 100 ||
		ServiceLocator::GetController()->GetMousePosition().x > (100 - Config::SCREEN_MOVE_SENSITIVITY) * Config::SCREEN_WIDTH / 100 ||
		ServiceLocator::GetController()->GetMousePosition().y < Config::SCREEN_MOVE_SENSITIVITY * Config::SCREEN_HEIGHT / 100 ||
		ServiceLocator::GetController()->GetMousePosition().y > (100 - Config::SCREEN_MOVE_SENSITIVITY) * Config::SCREEN_HEIGHT / 100)

		CallMoveViewport(elapsedTime);

	// Recenter screen to ship
	if (ServiceLocator::GetController()->IsKeyPressed(Space)) {
		ServiceLocator::GetViewer()->ResetZoom();

		Boat* _playerBoat = Game::GetGameObjectManager().Get("PlayerBoat");
		if (_playerBoat) {
			ServiceLocator::GetViewer()->MoveViewportTo(ConstrainMoveViewport(_playerBoat->GetPosition()));
		}
		else // If ship dead, recenter to center map
			ServiceLocator::GetViewer()->MoveViewportTo(
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 2.0f));
	}

	// Zoom in/out
	float wheelScroll = ServiceLocator::GetController()->GetWheelScroll();
	if (wheelScroll) {

		// Only allow scroll out if SCREEN_WIDTH/HEIGHT * zoom < MAP_WIDTH/HEIGHT
		if ((Config::SCREEN_WIDTH * ServiceLocator::GetViewer()->GetZoom() < Config::MAP_WIDTH &&
			Config::SCREEN_HEIGHT * ServiceLocator::GetViewer()->GetZoom() < Config::MAP_HEIGHT) ||
			wheelScroll > 0)
		{
			ServiceLocator::GetViewer()->ZoomViewport(-wheelScroll * 0.05f + 1.0f);

			// Scroll can reveal ara outside of map - so move the viewport towards center map if needed
			sf::Vector2f adjustCenter = ConstrainMoveViewport(
				ServiceLocator::GetViewer()->GetViewport().getCenter());

			if (ServiceLocator::GetViewer()->GetViewport().getCenter() != adjustCenter)
				ServiceLocator::GetViewer()->MoveViewportTo(adjustCenter);
		}
	}
}

void ViewControll::CallMoveViewport(float elapsedTime)
{
	sf::Vector2i mousePosition = sf::Mouse::getPosition(ServiceLocator::GetViewer()->GetWindow());

	sf::Vector2f moveVector = fVec(mousePosition -
		sf::Vector2i(Config::SCREEN_WIDTH / 2, Config::SCREEN_HEIGHT / 2));
	float moveX = moveVector.x;
	float moveY = moveVector.y * Config::SCREEN_WIDTH / Config::SCREEN_HEIGHT;

	float zoom = ServiceLocator::GetViewer()->GetZoom();

	//Limit camera movement to map width & height
	if (ServiceLocator::GetViewer()->GetViewport().getCenter().x < Config::SCREEN_WIDTH * zoom / 2.0f &&
		moveVector.x < 0.0f)
		moveX = 0.0f;
	else if (ServiceLocator::GetViewer()->GetViewport().getCenter().x > Config::MAP_WIDTH - Config::SCREEN_WIDTH * zoom / 2.0f &&
		moveVector.x > 0.0f)
		moveX = 0.0f;

	if (ServiceLocator::GetViewer()->GetViewport().getCenter().y < Config::SCREEN_HEIGHT * zoom / 2.0f &&
		moveVector.y < 0.0f)
		moveY = 0.0f;
	else if (ServiceLocator::GetViewer()->GetViewport().getCenter().y > Config::MAP_HEIGHT - Config::SCREEN_HEIGHT * zoom / 2.0f &&
		moveVector.y > 0.0f)
		moveY = 0.0f;

	ServiceLocator::GetViewer()->MoveViewport(sf::Vector2f(moveX, moveY), elapsedTime);
	/*
	//Limit mouse to game window
	if (mousePosition.x < 0)
		mousePosition.x = 0;
	else if (mousePosition.x > ServiceLocator::GetViewer()->GetWindow().getSize().x)
		mousePosition.x = ServiceLocator::GetViewer()->GetWindow().getSize().x;
	if (mousePosition.y < 0)
		mousePosition.y = 0;
	else if (mousePosition.y > ServiceLocator::GetViewer()->GetWindow().getSize().y)
		mousePosition.y = ServiceLocator::GetViewer()->GetWindow().getSize().y;

	sf::Mouse::setPosition(mousePosition, ServiceLocator::GetViewer()->GetWindow());
	*/
}

sf::Vector2f ViewControll::ConstrainMoveViewport(sf::Vector2f newPosition)
{
	float newPosX = newPosition.x;
	float newPosY = newPosition.y;
	float zoom = ServiceLocator::GetViewer()->GetZoom();

	if (newPosX < Config::SCREEN_WIDTH * zoom / 2.0f)
		newPosX = Config::SCREEN_WIDTH * zoom / 2.0f;
	else if (newPosX > Config::MAP_WIDTH - Config::SCREEN_WIDTH * zoom / 2.0f)
		newPosX = Config::MAP_WIDTH - Config::SCREEN_WIDTH * zoom / 2.0f;
	if (newPosY < Config::SCREEN_HEIGHT * zoom / 2.0f)
		newPosY = Config::SCREEN_HEIGHT * zoom / 2.0f;
	else if (newPosY > Config::MAP_HEIGHT - Config::SCREEN_HEIGHT * zoom / 2.0f)
		newPosY = Config::MAP_HEIGHT - Config::SCREEN_HEIGHT * zoom / 2.0f;

	return sf::Vector2f(newPosX, newPosY);
}
