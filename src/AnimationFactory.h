#pragma once
#include "Animation.h"

enum Anim {
	Explosion,
	ShortExplosion,
	Flame1, Flame2, Flame,
	Splash,
	Crew, Crew1, Crew2, Crew3, Crew4, Crew5, Crew6,
	DefaultShipSunken, RedShipSunken, GreenShipSunken,
	BlueShipSunken, YellowShipSunken, BlackShipSunken
};

class AnimationFactory
{
public:
	static Animation* Make(Anim animationName,
						   sf::Vector2f position = sf::Vector2f(),
						   float angle = 0.0f,
						   int zLevel = 10,
						   float frameLen = 0.0f,
						   float length = 0.0f,
						   sf::Vector2f velocity = sf::Vector2f());

private:
	static Animation* MakeCrew(sf::Vector2f position, sf::Vector2f velocity, float length, int crewMember, float frameLen);
};