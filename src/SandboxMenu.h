#pragma once
#include "Menu.h"

#include "BackgroundSheet.h"

class SandboxMenu :
	public Menu
{
public:
	SandboxMenu();

protected:
	void MakeButtons();
	void DealWithButtonClick(ButtonType button);
};