#include "stdafx.h"
#include "MenuManager.h"

#include "Game.h"
#include "MainMenu.h"
#include "SettingsMenu.h"
#include "SandboxMenu.h"
class Boat;

MenuManager::MenuManager()
{
}

MenuManager::~MenuManager() {
	for (std::map<Menu::MenuType, Menu*>::iterator itr = _menus.begin(); itr != _menus.end(); ++itr)
		delete itr->second;

	_menus.clear();
}

void MenuManager::LoadMenus()
{
	_menus[Menu::Main] = new MainMenu();
	_menus[Menu::Sandbox] = new SandboxMenu();
	_menus[Menu::Settings] = new SettingsMenu();

	_currentMenu = _menus[Menu::Main];
}

void MenuManager::ChangeMenu(Menu::MenuType newMenu)
{
	std::map<Menu::MenuType, Menu*>::iterator newMenuI = _menus.find(newMenu);
	if (newMenuI != _menus.end()) {
		_currentMenu = newMenuI->second;
		_currentMenu->Call();
	}
}

void MenuManager::Update(float elapsedTime)
{
	_currentMenu->Update(elapsedTime);
}

void MenuManager::Draw(sf::RenderWindow& window)
{
	_currentMenu->Draw(window);
}