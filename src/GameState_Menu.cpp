#include "stdafx.h"
#include "GameState_Menu.h"

#include "Game.h"

GS_Menu::GS_Menu()
{
	_myGameStateType = Menu;
}

GS_Menu::~GS_Menu()
{
}

void GS_Menu::Call()
{
	Game::GetGameObjectManager().Empty();

	ServiceLocator::GetViewer()->ResetZoom();
	ServiceLocator::GetViewer()->MoveViewportTo(
		sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 2.0f));

	Game::GetGameObjectManager().GetTileManager()->LoadBackground(Config::SCREEN_WIDTH, Config::SCREEN_HEIGHT);

	Game::GetMenuManager().ChangeMenu(Menu::MenuType::Main);
}

void GS_Menu::Update(float elapsedTime)
{
	Game::GetMenuManager().Update(elapsedTime);
}

void GS_Menu::Draw(sf::RenderWindow& window)
{
	Game::GetMenuManager().Draw(window);
}