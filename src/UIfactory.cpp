#include "stdafx.h"
#include "UIfactory.h"

#include "Game.h"

#include "A_Static.h"
#include "HPbar.h"
#include "HPtext.h"
#include "CDbar.h"
#include "AmmoDisplay.h"
#include "FlameProjText.h"
#include "BackgroundPanel.h"
#include "BackgroundSheet.h"
#include "AbilityButton.h"
#include "StatusDisplay.h"

UIfactory::UIfactory()
{
}

void UIfactory::Make(UIelement element)
{
	switch (element) {
	case playerHPbar:
		{
		float length = 0.9f * Config::SCREEN_WIDTH / 2.0f;
		float fullHP = Game::GetGameObjectManager().Get("PlayerBoat")->GetFullHP();
		sf::Vector2f barPosition = sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 83.0f / 96.0f);

		BackgroundPanel* backPanel = new BackgroundPanel(
			sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 51.0f / 56.0f),
			Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 5.0f / 28.0f, LBrown);

		ValueBar* HPbarCarpet = new ValueBar();
		HPbarCarpet->Load(barPosition, length, fullHP, false, White, 1.5f);

		HPbar* playerHPbar = new HPbar("PlayerBoat", barPosition, length, false, Red, 1.5f);

		HPtext* playerHPtext = new HPtext("HP: 0/" + std::to_string((int)fullHP),
			"PlayerBoat", sf::Vector2f(Config::SCREEN_WIDTH * 53.0f / 80.0f,
									   Config::SCREEN_HEIGHT * 42.75f / 48.0f), 30);

		CDbar* playerCDbar = new CDbar(sf::Vector2f(Config::SCREEN_WIDTH * 32.0f/48.0f,
													Config::SCREEN_HEIGHT * 19.0f/20.0f),
									   Config::SCREEN_WIDTH * 2.0f / 30.0f, 2.0f);

		FlameProjText* playerFlameProjText = new FlameProjText(sf::Vector2f(Config::SCREEN_WIDTH * 25.0f / 80.0f,
																			Config::SCREEN_HEIGHT * 76.0f / 80.0f));

		A_Static* flameProj1 = new A_Static(sf::Vector2f(Config::SCREEN_WIDTH * 23.0f / 80.0f,
														 Config::SCREEN_HEIGHT * 76.0f / 80.0f), 0.0f, "flame1", 11, true);
		A_Static* flameProj2 = new A_Static(sf::Vector2f(Config::SCREEN_WIDTH * 23.0f / 80.0f,
														 Config::SCREEN_HEIGHT * 77.3f / 80.0f), 0.0f, "projectile", 11, true);
		flameProj1->SetScale(1.6f, 1.6f);
		flameProj2->SetScale(1.8f, 1.8f);

		float MMapHeight = ((float)Config::MAP_WIDTH / (float)Config::MAP_HEIGHT) * 1 / 5.0f * Config::SCREEN_HEIGHT *
			((float)Config::SCREEN_HEIGHT / (float)Config::SCREEN_WIDTH);
		BackgroundSheet* mapSheet = new BackgroundSheet(
			sf::Vector2f(Config::SCREEN_WIDTH / 10.0f, Config::SCREEN_HEIGHT - MMapHeight / 2.0f),
			Config::SCREEN_WIDTH / 5.0f, MMapHeight, LBrown);

		BackgroundSheet* abilitiesSheet = new BackgroundSheet(
			sf::Vector2f(Config::SCREEN_WIDTH * 7.0f / 8.0f, Config::SCREEN_HEIGHT*(1 - 1.0f / 10.0f)),
			Config::SCREEN_WIDTH / 4.0f, Config::SCREEN_HEIGHT / 5.0f, Beige);

		StatusDisplay* statusDisp = new StatusDisplay(sf::Vector2f(Config::SCREEN_WIDTH * 76.8f / 80.0f, Config::SCREEN_HEIGHT*(1 - 10.0f / 100.0f)));

		A_Static* horizontalStatusLine = new A_Static(sf::Vector2f(Config::SCREEN_WIDTH * 67.5f / 80.0f,
																   Config::SCREEN_HEIGHT*(1 - 8.0f / 100.0f)),
													  inRad(180.0f), "defaultFlag", 11, true);
		A_Static* verticalStatusLine = new A_Static(sf::Vector2f(Config::SCREEN_WIDTH * 73.9f / 80.0f,
																 Config::SCREEN_HEIGHT*(90.3f / 100.0f)),
													  0.0f, "defaultFlag", 11, true);
		horizontalStatusLine->SetScale(1.0f, 11.8f);
		verticalStatusLine->SetScale(1.0f, 7.0f);

		Game::GetGameObjectManager().Add("UI", backPanel);
		Game::GetGameObjectManager().Add("UI", HPbarCarpet);
		Game::GetGameObjectManager().Add("UI", playerHPbar);
		Game::GetGameObjectManager().Add("UI", playerHPtext);
		Game::GetGameObjectManager().Add("UI", playerCDbar);
		Game::GetGameObjectManager().Add("UI", playerFlameProjText);
		Game::GetGameObjectManager().Add("UI", flameProj1);
		Game::GetGameObjectManager().Add("UI", flameProj2);
		Game::GetGameObjectManager().Add("UI", mapSheet);
		Game::GetGameObjectManager().Add("UI", abilitiesSheet);
		Game::GetGameObjectManager().Add("UI", statusDisp);
		Game::GetGameObjectManager().Add("UI", horizontalStatusLine);
		Game::GetGameObjectManager().Add("UI", verticalStatusLine);

		//UI elements below are GameObjects and add their own sprites as "UI"

		AmmoDisplay* playerAmmoX = new AmmoDisplay(sf::Vector2f(Config::SCREEN_WIDTH * 37.0f / 80.0f, Config::SCREEN_HEIGHT * 45.75f / 48.0f),
			Projectile::ExplosiveProj);
		AmmoDisplay* playerAmmo = new AmmoDisplay(sf::Vector2f(Config::SCREEN_WIDTH * 23.0f / 80.0f, Config::SCREEN_HEIGHT * 43.5f / 48.0f),
			Projectile::NormalProj);

		//Before making ability buttons (that register themselves into UIcontrolls) clear
		//any previous UIcontrolls buttons (now empty pointers)
		UIcontrolls::ClearAbilityButtons();
		AbilityButton* abily1 = new AbilityButton(SetFireProj);
		AbilityButton* abily2 = new AbilityButton(SetNormalProj);
		AbilityButton* abily3 = new AbilityButton(SetExplosiveProj);
		AbilityButton* abily4 = new AbilityButton(Anchor);

		Game::GetGameObjectManager().Add(playerAmmoX);
		Game::GetGameObjectManager().Add(playerAmmo);
		Game::GetGameObjectManager().Add(abily1);
		Game::GetGameObjectManager().Add(abily2);
		Game::GetGameObjectManager().Add(abily3);
		Game::GetGameObjectManager().Add(abily4);
		}
		break;
	}
}
