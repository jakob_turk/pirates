#pragma once
#include "VisibleObject.h"
#include "Projectile.h"
#include "Boat.h"
#include "Animation.h"

#include "TileManager.h"

class GameObjectManager
{
public:
	GameObjectManager();
	~GameObjectManager();

	void Add(std::string name, VisibleObject* gameObject);
	void Add(Animation* animation);
	void Add(GameObject* otherObject);
	void AddDrawable(VisibleObject* drawable);
	void AddMinimapDrawable(VisibleObject* drawable);
	void Remove(std::string name);
	void Empty(bool soft = false);	//soft means no background clear
	int GetObjectCount() const;
	Boat* Get(std::string name) const;
	std::map<std::string, Boat*> GetBoats() const;
	TileManager* GetTileManager() const;

	void DrawAll(sf::RenderWindow& renderWindow);
	void UpdateAll(float timeDelta);

private:
	sf::Clock clock;

	std::map<std::string, Boat*> _boatObjects;
	std::vector<Projectile*> _projectiles;
	std::vector<VisibleObject*> _visibleObjects;
	std::list<GameObject*> _gameObjects;
	std::vector<VisibleObject*> _UIobjects;
	//A lot of explaining and defence needed here...
	//_boatObjects separated because they're the only ones named (available in Get() )
	//_projectiles seaprated because they need to be updated separated from boats (and all together)
	//_visibleObjects is the generic table (join with projectiles?)
	//_gameObjects separated because you don't call Draw() on them
	//	-This is a list because it is the only one of these containers that was unrelentingly throwing
	//	 all sorts of errors because of erasure and adding of elements during iteration. It is the
	//	 shortest of the bunch so it is not too expensive to have as list. Unclear why others don't behave so.
	//_UIObjects separated because their Draw is called differently

	//_drawTable is a map of z levels, each with coresponding vector of objects
	//each VisibleObject has to be registered in it's level acording to it's RegisterDraw every tick
	//this is terrible, but currently how things seem to be designed - a solve would be smart pointers!! TODO
	//default is 5 (z ordering is then just order of registering new objects)
	std::map<int, std::vector<VisibleObject*>> _drawTable;
	//The second draw table, _minimapDrawTable is a simpler table of objects to be drawn on the minimap
	//Just a vector, no zLevels. Means things will be drawn randomly, but for a minimap that is OK
	std::vector<VisibleObject*> _minimapDrawTable;

	TileManager* _tileManager;


	struct BoatDeallocator
	{
		void operator()(const std::pair<std::string, Boat*> & p) const
		{
			delete p.second;
		}
	};
	struct ProjectileDeallocator
	{
		void operator()(const Projectile* p) const
		{
			delete p;
		}
	};
	struct VisibleDeallocator
	{
		void operator()(const VisibleObject* p) const
		{
			delete p;
		}
	};
	struct GameObjectDeallocator
	{
		void operator()(const GameObject* p) const
		{
			delete p;
		}
	};
};
