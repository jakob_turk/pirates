#include "stdafx.h"
#include "BackgroundSheet.h"

BackgroundSheet::BackgroundSheet(sf::Vector2f position, float width, float height, BtnColour colour)
{
		std::string colourString;

		switch (colour) {
		case Brown:
			colourString = "brown";
			break;
		case LBrown:
			colourString = "beige";
			break;
		case Gray:
			colourString = "gray";
			break;
		case Beige:
			colourString = "white";
			break;
		}

		Load(colourString + "Sheet");
		SetPosition(position);
		GetSprite().setScale(width / GetWidth(), height / GetHeight());
}
