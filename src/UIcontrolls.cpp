#include "stdafx.h"
#include "UIcontrolls.h"

std::vector<Ability> UIcontrolls::_clickedAbilities = std::vector<Ability>();
std::vector<AbilityButton*> UIcontrolls::_abilityButtons;

void UIcontrolls::AddAbilityButton(AbilityButton * newAbilityButton)
{
	_abilityButtons.push_back(newAbilityButton);
}

std::vector<AbilityButton*> UIcontrolls::GetAbilityButtons()
{
	return _abilityButtons;
}

void UIcontrolls::ClearAbilityButtons()
{
	_abilityButtons.clear();
	_clickedAbilities = std::vector<Ability>();
}

void UIcontrolls::ClickAbility(Ability ability)
{
	_clickedAbilities.push_back(ability);
}

std::vector<Ability> UIcontrolls::GetClickedAbilities()
{
	std::vector<Ability> returnAbilities = _clickedAbilities;
	_clickedAbilities = std::vector<Ability>();

	return returnAbilities;
}
