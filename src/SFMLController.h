#pragma once
#include "IControllerProvider.h"

class SFMLController :
	public IControllerProvider
{
public:
	SFMLController();

	void PollEvents();
	std::vector<sf::Event>& GetEvents();

	sf::Vector2i GetMousePosition();
	bool IsMousePressed();
	bool IsKeyPressed(Key key);
	float GetWheelScroll();

private:
	std::vector<sf::Event> _events;

	bool _mousePressed;
	float _wheelScrolled;
	std::vector<Key> _pressedKeys;

	void AddPressedKey(Key key);
	void RemovePressedKey(Key key);
};