#pragma once
#include "Button.h"

#include "UIcontrolls.h"
#include "IControllerProvider.h"

class AbilityButton :
	public GameObject
{
public:
	AbilityButton(Ability ability);

	void Update(float elapsedTime);

	void MakeNotActive();

private:
	Button* _button;
	Ability _ability;
	Key _key;
};