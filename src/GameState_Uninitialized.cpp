#include "stdafx.h"
#include "GameState_Uninitialized.h"

GS_Unitialized::GS_Unitialized() {
	_myGameStateType = Uninitialized;
}

GS_Unitialized::~GS_Unitialized() {
}

void GS_Unitialized::Call() {}

void GS_Unitialized::Update(float elapsedTime) {}

void GS_Unitialized::Draw(sf::RenderWindow& window) {}