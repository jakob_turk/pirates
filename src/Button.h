#pragma once
#include "UIelement.h"

#include "Text.h"
#include "Simple.h"

class Button:
	public UIelement
{
public:
	Button(sf::Vector2f position, BtnColour color = Brown, sf::Vector2f size = sf::Vector2f(), Text* textObject = NULL);
	~Button();

	void AddPicture(std::string textureName);
	virtual void LoadButtonColor(BtnColour color);

	bool Hover(); //returns 0 if normal hover, 1 if if right after click (so if the button has been clicked)
	void Click();
	void Clear();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow & renderWindow);

protected:
	std::string _releaseTexture;
	std::string _hoverTexture;
	std::string _clickTexture;
private:
	Text* _buttonText;
	Simple* _buttonPicture;

	sf::Vector2f _position;
	sf::Vector2f _textPosition;
	sf::Vector2f _picturePosition;
	sf::Vector2f _size;

	bool _clicked;
	bool _hovered;
	bool _justClicked;
};