#pragma once
#include "MovableObject.h"

#include "AnimationFactory.h"

//Forward declare Boat for boat collision
class Boat;
enum BoatFaction {
	fDefault, fRed, fGreen, fBlue, fYellow, fBlack, N_FACTIONS = fBlack
};

class Projectile :
	public MovableObject
{
public:
	enum ProjectileType {
		NormalProj, FireProj, ExplosiveProj
	};

	Projectile(sf::Vector2f position, float angle, BoatFaction ownerFaction, ProjectileType type);

	void Update(float elapsedTime);
	void BoatCollision(Boat& boat);
	void Miss();

	BoatFaction GetFaction();
	float GetDamage();

	static std::map<ProjectileType, std::vector<float>> PROJECTILE_DATA;

private:
	ProjectileType _type;

	float _damage;
	float _reach;
	Animation* _flame;

	float _dist;
	float _distFromExplosion;
	BoatFaction _faction;
};