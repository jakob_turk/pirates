#include "stdafx.h"
#include "Controller_AI.h"

#include "Game.h"

Controller_AI::Controller_AI() :
	_currentState(Patrol),
	_lastChangeCourse(0),
	_changeCoursePeriod(4),
	_lastFollow(0),
	_followPeriod(0),
	_hasFled(false),
	_lastChangeCannon(0),
	_changeCannonPeriod(2),
	_followedBoat(NULL)
{
}

void Controller_AI::Control(Boat& boat) 
{
	//If currently in state that requires an enemy ship, check for it's existance.
	//If present, define distanceToEnemy as it is needed in all non-Patrol states, else
	//set state to patroling (enemy destroyed)
	float distanceToEnemy = 0.0f;
	if (_currentState != Patrol) {

		_followedBoat = Game::GetGameObjectManager().Get(_followedBoatName);

		if (!_followedBoat || _followedBoat->IsSinking())
			_currentState = Patrol;
		else
			distanceToEnemy = dotF(_followedBoat->GetPosition() - boat.GetPosition(),
								   _followedBoat->GetPosition() - boat.GetPosition());
	}

	//Try to change cannon on set period while not Patroling
	if (_currentState != Patrol && (int)std::time(0) - _lastChangeCannon > _changeCannonPeriod) {

		int randomizer = std::rand() % 10;

		if (randomizer < 5 && boat.GetCannon()->GetAmmo(Projectile::FireProj))
			boat.GetCannon()->SetCannonType(Projectile::FireProj);
		else if (randomizer < 7 && boat.GetCannon()->GetAmmo(Projectile::ExplosiveProj))
			boat.GetCannon()->SetCannonType(Projectile::ExplosiveProj);
		else
			boat.GetCannon()->SetCannonType(Projectile::NormalProj);

		_lastChangeCannon = (int)std::time(0);
	}
	//In case all ammo of a type (except normal) is spent, this will always swap
	//to cannon with remaining ammo (NormalProj if nothing else) after max 1 _changeCannonPeriod

	//Change AI state if needed
	///////////////////////////

	//vector length check in all of these is done with length**2 comparison
	//so as to avoid that std::sqrt
	switch (_currentState) {
	case Patrol:
	{
		std::map<std::string, Boat*> boatsToCheck;
		if (Game::GetGameState() == GameState::Playing && !Config::SANDBOX_FACTIONS_AGGRESSIVE) {
			if (Game::GetGameObjectManager().Get("PlayerBoat"))
				boatsToCheck.insert(std::pair<std::string, Boat*>
					("PlayerBoat", Game::GetGameObjectManager().Get("PlayerBoat")));
			else
				break;
		}
		else
			boatsToCheck = Game::GetGameObjectManager().GetBoats();

		for (auto otherBoat : boatsToCheck) {
			if (otherBoat.second->GetFaction() == boat.GetFaction())
				continue;
			if (dotF(otherBoat.second->GetPosition() - boat.GetPosition(),
					 otherBoat.second->GetPosition() - boat.GetPosition()) <
				std::pow(Projectile::PROJECTILE_DATA[Projectile::NormalProj][1] * 1.25f, 2)) {
				_followedBoat = otherBoat.second;
				_followedBoatName = otherBoat.first;
				_currentState = MoveToFireRange;
				break;
			}
		}
		break;
	}
	case MoveToFireRange:
		if (distanceToEnemy <
			std::pow(Projectile::PROJECTILE_DATA[boat.GetCannon()->GetCannonType()][1] * 0.90f, 2))
			_currentState = InFireRange;
		else if (distanceToEnemy >
				 std::pow(Projectile::PROJECTILE_DATA[boat.GetCannon()->GetCannonType()][1] * 1.25f, 2))
			GoToPatroling();
		CheckPanic(boat);
		break;
	case InFireRange:
		if (distanceToEnemy <
			std::pow(Projectile::PROJECTILE_DATA[boat.GetCannon()->GetCannonType()][1] * 0.75f, 2))
			_currentState = InBoardingRange;
		if (distanceToEnemy >
			std::pow(Projectile::PROJECTILE_DATA[boat.GetCannon()->GetCannonType()][1] * 0.9f, 2))
			_currentState = MoveToFireRange;
		CheckPanic(boat);
		break;
	case InBoardingRange:
		if (distanceToEnemy >
			std::pow(Projectile::PROJECTILE_DATA[boat.GetCannon()->GetCannonType()][1] * 0.75f, 2))
			_currentState = InFireRange;
		CheckPanic(boat);
		break;
	case Flee:
		if (distanceToEnemy >
			std::pow(Projectile::PROJECTILE_DATA[boat.GetCannon()->GetCannonType()][1] * 1.25f, 2))
			GoToPatroling();
		break;
	}

	//Handle current AI state
	/////////////////////////
	switch (_currentState) {
	case Patrol:
		if ((int)std::time(0) - _lastChangeCourse > _changeCoursePeriod) {

			//Set target to random point on map every 3 seconds
			boat.SetTargetLocation(sf::Vector2f(
				(float)(std::rand() % Config::MAP_WIDTH),
				(float)(std::rand() % Config::MAP_HEIGHT)));
			_lastChangeCourse = (int)std::time(0);
		}
		break;

	case MoveToFireRange:
		//Move toward enemy ship
		SetTarget(boat, _followedBoat->GetPosition());
		break;

	case InFireRange:
		//Stop movement
		if (boat.GetSpeed() != 0.0f)
			SetTarget(boat, sf::Vector2f(0.0f, 0.0f));
		//Shoot at enemy boat with perfect accuracy
		boat.GetCannon()->Shoot(boat.GetPosition(), _followedBoat->GetPosition(), boat.GetFaction());
		break;

	case InBoardingRange:
		//Move away from enemy ship (target location is enemy ship + 2*(vec from enemy towards boat)
		SetTarget(boat, _followedBoat->GetPosition() +
						2.0f*(boat.GetPosition() - _followedBoat->GetPosition()));
		//Also shoot at enemy boat
		boat.GetCannon()->Shoot(boat.GetPosition(), _followedBoat->GetPosition(), boat.GetFaction(), 20);
		break;

	case Flee:
		//Move away from enemy ship (target location is enemy ship + 2*(vec from enemy towards boat)
		SetTarget(boat, _followedBoat->GetPosition() +
						2.0f*(boat.GetPosition() - _followedBoat->GetPosition()));
		//Also shoot wildly at enemy boat
		boat.GetCannon()->Shoot(boat.GetPosition(), _followedBoat->GetPosition(), boat.GetFaction(), 80);
		break;
	}
}

void Controller_AI::SetTarget(Boat& boat, sf::Vector2f position)
{
	boat.SetTargetLocation(position);
}

void Controller_AI::GoToPatroling()
{
	_currentState = Patrol;
}

void Controller_AI::CheckPanic(Boat & boat)
{
	if (!_hasFled && boat.GetHP() / boat.GetFullHP() < 0.3f || boat.IsSinking()) {
		_hasFled = true;
		_currentState = Flee;
	}
}
