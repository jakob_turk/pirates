#include "stdafx.h"
#include "StatusDisplay.h"

#include "A_Static.h"
#include "A_SailIcon.h"

StatusDisplay::StatusDisplay(sf::Vector2f position) :
	_currentStatus(StatusIcon::Idle)
{
	_icons.insert(std::pair<StatusIcon, Animation*>(StatusIcon::Idle, new A_Static(position, 0.0f, "defaultSail", 11, true)));
	_icons.insert(std::pair<StatusIcon, Animation*>(StatusIcon::Sailing, new A_SailIcon(position)));
	_icons.insert(std::pair<StatusIcon, Animation*>(StatusIcon::Anchored, new A_Static(position, 0.0f, "anchor", 11, true)));
	_icons.insert(std::pair<StatusIcon, Animation*>(StatusIcon::Sinking, new A_Static(position, 0.0f, "defaultSailDmg2", 11, true)));
}

void StatusDisplay::Draw(sf::RenderWindow & renderWindow)
{
	_icons[_currentStatus]->Draw(renderWindow);
}

void StatusDisplay::Update(float elapsedTime)
{
	Boat* playerBoat = Game::GetGameObjectManager().Get("PlayerBoat");
	if (!playerBoat)
		return;
		//If player boat is lost (presumably by sinking), the display icon stays on sinking

	if (playerBoat->IsSinking()) {
		if (_currentStatus != StatusIcon::Sinking)
			_currentStatus = StatusIcon::Sinking;
	}
	else if (playerBoat->IsAnchored()) {
		if (_currentStatus != StatusIcon::Anchored)
			_currentStatus = StatusIcon::Anchored;
	}
	else if (playerBoat->GetSpeed() != 0.0f) {
		if (_currentStatus != StatusIcon::Sailing)
			_currentStatus = StatusIcon::Sailing;
	}
	else {
		if (_currentStatus != StatusIcon::Idle)
			_currentStatus = StatusIcon::Idle;
	}

	_icons[_currentStatus]->Update(elapsedTime);
}
