#include "stdafx.h"
#include "HPbar.h"

#include "Game.h"

HPbar::HPbar(std::string entity, sf::Vector2f position, float length,
			 bool orientation, BarColour colour, float scaleAcross):
	_followedObject(entity)
{
	Load(position, length, Game::GetGameObjectManager().Get(_followedObject)->GetFullHP(), 
		 orientation, colour, scaleAcross);
}

void HPbar::Update(float elapsedTime)
{
	Boat* followedObjectPtr = Game::GetGameObjectManager().Get(_followedObject);
	if (followedObjectPtr) {
		Resize(followedObjectPtr->GetHP());
	}
	else
		SetDeletion();
}
