#include "stdafx.h"
#include "HPtext.h"

#include "Game.h"

HPtext::HPtext(std::string text, std::string followedObject, sf::Vector2f position, int textSize, sf::Color textColor)
	: Text(text, position, textSize, textColor),
	_followedObject(followedObject)
{
}

void HPtext::Update(float elapsedTime)
{
	Boat* followedObjectPtr = Game::GetGameObjectManager().Get(_followedObject);
	if (followedObjectPtr) {
		//The HP text is always HP: x/y. Trouble is one doesn't know how many digits x has. So you look
		//the length of string - 5 (HP: /) - length of full hp...disgostang. TO DO: fix
		if (followedObjectPtr->GetHP() != std::stof(GetText().substr(4,
				GetText().length() - 5 - std::to_string((int)followedObjectPtr->GetFullHP()).length())))
		{
			UpdateText("HP: " + std::to_string((int)followedObjectPtr->GetHP()) +
				"/" + std::to_string((int)followedObjectPtr->GetFullHP()));
		}
	}
}
