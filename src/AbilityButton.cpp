#include "stdafx.h"
#include "AbilityButton.h"

#include "Game.h"

AbilityButton::AbilityButton(Ability ability)
{
	_ability = ability;

	float buttonWidth = 73.0f;
	float buttonHeight = 73.0f;
	sf::Vector2f buttonPos;
	std::string buttonLetter = "";
	std::string iconName;
	BtnColour btnCol = LBrown;

	switch (_ability) {

	case SetNormalProj:
		//Button position
		buttonPos = sf::Vector2f(
			Config::SCREEN_WIDTH * 62.8f / 80.0f,
			Config::SCREEN_HEIGHT*(1 - 11.1f / 80.0f));

		buttonLetter = "Q";
		iconName = "projectile";
		_key = Key::Q;
		btnCol = Brown;	//Set normal to first picked ability
		break;

	case SetFireProj:
		//Button position
		buttonPos = sf::Vector2f(
			Config::SCREEN_WIDTH * 67.1f / 80.0f,
			Config::SCREEN_HEIGHT*(1 - 11.1f / 80.0f));
		
		buttonLetter = "W";
		iconName = "flame1";
		_key = Key::W;
		break;

	case SetExplosiveProj:
		//Button position
		buttonPos = sf::Vector2f(
			Config::SCREEN_WIDTH * 71.4f / 80.0f,
			Config::SCREEN_HEIGHT*(1 - 11.1f / 80.0f));

		buttonLetter = "E";
		iconName = "explosion2";
		_key = Key::E;
		break;

	case Anchor:
		//Button position
		buttonPos = sf::Vector2f(
			Config::SCREEN_WIDTH * 67.0f / 80.0f,
			Config::SCREEN_HEIGHT*(1 - 3.4f / 80.0f));
		buttonHeight = 45.0f;
		buttonWidth = 225.0f;

		buttonLetter = "Shift";
		iconName = "explosion2";
		_key = Key::Shift;
		break;
	}

	//Make the button
	_button = new Button(buttonPos, btnCol, sf::Vector2f(buttonWidth, buttonHeight),
		new Text(buttonLetter,
			buttonPos + sf::Vector2f(-buttonWidth * 13.0f / 40.0f, buttonHeight * 7.0f / 40.0f)));

	//Add picture
	_button->AddPicture(iconName);

	//Add object to GameObjectManager
	//The abilityButton (GameObject) itself must still be added to gameobjectmanager!
	//This just adds the button (VisibleObject)
	Game::GetGameObjectManager().Add("UI", _button);

	//Add AbilityButton (this) to the static UIcontrolls
	UIcontrolls::AddAbilityButton(this);
}

void AbilityButton::Update(float elapsedTime)
{
	//First check if the button is being clicked
	sf::Vector2i mousePos = ServiceLocator::GetController()->GetMousePosition();

	if ( (_button->GetSprite().getGlobalBounds().contains((float)mousePos.x, (float)mousePos.y) &&
		  ServiceLocator::GetController()->IsMousePressed()) ||
		 ServiceLocator::GetController()->IsKeyPressed(_key))
	{
		_button->Click();

		if (_ability != Anchor) {
			//Recolor buttons (this is extremely non OOB)
			for (AbilityButton* aButton : UIcontrolls::GetAbilityButtons())
				aButton->MakeNotActive();

			_button->LoadButtonColor(Brown);
		}

		UIcontrolls::ClickAbility(_ability);
	}
	else
		_button->Hover();
}

void AbilityButton::MakeNotActive()
{
	_button->LoadButtonColor(LBrown);
}
