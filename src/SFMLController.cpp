#include "stdafx.h"
#include "SFMLController.h"

#include "Game.h"

SFMLController::SFMLController() :
	_mousePressed(false),
	_wheelScrolled(0.0f)
{
}

void SFMLController::PollEvents()
{
	_events.clear();

	sf::Window& mainWindow = ServiceLocator::GetViewer()->GetWindow();
	sf::Event currentEvent;

	//wheelScrolled is set to true during even polling (if the wheel is scrolled)
	//so that VIEWPORT CONTROLS easily deals with it where it is supposed to (after polling)
	_wheelScrolled = 0.0;

	while (mainWindow.pollEvent(currentEvent))
	{
		if (currentEvent.type == sf::Event::EventType::Closed) {
			Game::ChangeGameState(GameState::Exiting);
			break;
		}
		else if (currentEvent.type == sf::Event::MouseButtonPressed)
			_mousePressed = true;

		else if (currentEvent.type == sf::Event::MouseButtonReleased)
			_mousePressed = false;

		else if (currentEvent.type == sf::Event::MouseWheelScrolled)
			_wheelScrolled = currentEvent.mouseWheelScroll.delta;

		else if (currentEvent.type == sf::Event::KeyPressed) {

			switch (currentEvent.key.code)
			{
			case sf::Keyboard::Escape:
				AddPressedKey(Esc);
				break;
			case sf::Keyboard::Space:
				AddPressedKey(Space);
				break;
			case sf::Keyboard::LShift:
				AddPressedKey(Shift);
				break;
			case sf::Keyboard::RShift:
				AddPressedKey(Shift);
				break;
			case sf::Keyboard::A:
				AddPressedKey(A);
				break;
			case sf::Keyboard::Q:
				AddPressedKey(Q);
				break;
			case sf::Keyboard::W:
				AddPressedKey(W);
				break;
			case sf::Keyboard::E:
				AddPressedKey(E);
				break;
			}
		}

		else if (currentEvent.type == sf::Event::KeyReleased) {

			switch (currentEvent.key.code)
			{
			case sf::Keyboard::Escape:
				RemovePressedKey(Esc);
				break;
			case sf::Keyboard::Space:
				RemovePressedKey(Space);
				break;
			case sf::Keyboard::LShift:
				RemovePressedKey(Shift);
				break;
			case sf::Keyboard::RShift:
				RemovePressedKey(Shift);
				break;
			case sf::Keyboard::A:
				RemovePressedKey(A);
				break;
			case sf::Keyboard::Q:
				RemovePressedKey(Q);
				break;
			case sf::Keyboard::W:
				RemovePressedKey(W);
				break;
			case sf::Keyboard::E:
				RemovePressedKey(E);
				break;
			}
		}
		_events.push_back(currentEvent);
	}
}

void SFMLController::AddPressedKey(Key key) 
{
	std::vector<Key>::iterator findKey = find(_pressedKeys.begin(), _pressedKeys.end(), key);
	if (findKey == _pressedKeys.end())
		_pressedKeys.push_back(key);
}

void SFMLController::RemovePressedKey(Key key)
{
	std::vector<Key>::iterator findKey = find(_pressedKeys.begin(), _pressedKeys.end(), key);
	if (findKey != _pressedKeys.end())
		_pressedKeys.erase(findKey);
}

std::vector<sf::Event>& SFMLController::GetEvents()
{
	return _events;
}

sf::Vector2i SFMLController::GetMousePosition()
{
	return sf::Mouse::getPosition(ServiceLocator::GetViewer()->GetWindow());
}

bool SFMLController::IsMousePressed()
{
	return _mousePressed;
}

bool SFMLController::IsKeyPressed(Key key)
{
	std::vector<Key>::iterator findKey = find(_pressedKeys.begin(), _pressedKeys.end(), key);
	return findKey != _pressedKeys.end();	
}

float SFMLController::GetWheelScroll()
{
	return _wheelScrolled;
}
