#pragma once
#include "VisibleObject.h"

class MovableObject :
	public VisibleObject
{
public:
	MovableObject();
	~MovableObject();

	virtual void Update(float elapsedTime);
	void Turn(float elapsedTime);

	float GetSpeed();
	float GetAngle();
	sf::Vector2f GetVelocity();

	void SetVelocity(float newSpeed, float newAngle); //in rad!s
	void SetVelocity(sf::Vector2f newVelocity);

protected:
	void SetStartingMaxSpeed(float startingMaxSpeed);
	void SetMaxSpeed(float maxSpeed = -1.0f);
	void SetAngularVel(float maxAngularVel);
	void SetStartingFriction(float friction);
	void SetFriction(float friction);

	float GetStartingMaxSpeed();
	float GetMaxSpeed();
	float GetStartingFriction();

private:
	float _speed;
	float _angle;

	float _startingMaxSpeed;
	float _maxSpeed;
	float _minSpeed;
	float _angularVel;
	float _maxAngleChange;
	float _startingFriction;
	float _friction;
};