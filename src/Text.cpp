#include "stdafx.h"
#include "Text.h"

#include "ServiceLocator.h"

Text::Text(std::string text, sf::Vector2f position, int textSize, sf::Color textColor)
{
	_font = ServiceLocator::GetTextures()->GetFont();
	_text.setFont(_font);
	_text.setString(text);
	_text.setCharacterSize(textSize);
	_text.setFillColor(textColor);
	_text.setOrigin(_text.getLocalBounds().width / 2, _text.getLocalBounds().height);
	_text.setPosition(position);

	SetLoaded();
}

void Text::Update(float elapsedTime)
{
}

void Text::Draw(sf::RenderWindow & renderWindow)
{
	if (IsLoaded())
		renderWindow.draw(_text);
}

std::string Text::GetText()
{
	return _text.getString();
}

void Text::UpdateText(std::string newText)
{
	_text.setString(newText);
}

void Text::UpdatePosition(sf::Vector2f moveVector) {
	_text.move(moveVector);
}

void Text::SetPosition(sf::Vector2f newPosition)
{
	_text.setPosition(newPosition);
}

sf::Vector2f Text::GetPosition() {
	return _text.getPosition();
}
