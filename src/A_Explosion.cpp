#include "stdafx.h"
#include "A_Explosion.h"

A_Explosion::A_Explosion(sf::Vector2f position, sf::Vector2f velocity, float frameLen, bool thirdFrame) :
	_thirdFrame(thirdFrame)
{
	_frameLen = frameLen;
	SetPosition(position);
	SetVelocity(velocity);
	Load();
}

void A_Explosion::Load() {

	//Load Simple VisibleObjects
	LoadFrameSprite("explosion1", GetPosition());
	LoadFrameSprite("explosion2", GetPosition());
	if (_thirdFrame)
		LoadFrameSprite("explosion3", GetPosition());

	Animation::Load();
}

void A_Explosion::Update(float timeDelta)
{
	for (std::vector<sf::Sprite>::iterator fItr = _frames.begin(); fItr != _frames.end(); fItr++) {
		fItr->move(GetVelocity() * timeDelta);
	}

	_sinceFrameUpdate += timeDelta;
	if (_sinceFrameUpdate > _frameLen) {
		_sinceFrameUpdate = 0;
		NextFrame();
	}
}