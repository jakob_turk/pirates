#include "stdafx.h"
#include "ValueBar.h"

ValueBar::ValueBar()
{
}

void ValueBar::Load(sf::Vector2f position, float length, float fullValue, bool orientation, 
					BarColour colour, float scaleAcross)
{
	_orientation = orientation;
	_position = position;
	_fullLength = length;
	_fullValue = fullValue;
	_value = fullValue;
	_scaleAcross = scaleAcross;

	std::string textureName = "Bar";

	switch (colour) {
	case White:
		textureName = "white" + textureName;
		break;

	case Red:
		textureName = "red" + textureName;
		break;

	case Blue:
		textureName = "blue" + textureName;
		break;

	case Green:
		textureName = "greet" + textureName;
		break;

	case Yellow:
		textureName = "yellow" + textureName;
		break;
	}

	if (!orientation) {
		LoadTextureToSprite(textureName + "Left", _barStart);
		_barStart.setOrigin(0.0f, _barStart.getLocalBounds().height / 2.0f);
		_barStart.setPosition(_position + sf::Vector2f(-(_fullLength / 2.0f + _barStart.getLocalBounds().width), 0.0f));

		LoadTextureToSprite(textureName + "Mid", _barMid);
		_barMid.setOrigin(0.0f, _barMid.getLocalBounds().height / 2.0f);
		_barMid.setPosition(_position + sf::Vector2f(-_fullLength / 2.0f, 0.0f));

		LoadTextureToSprite(textureName + "Right", _barEnd);
		_barEnd.setOrigin(0.0f, _barEnd.getLocalBounds().height / 2.0f);
		_barEnd.setPosition(_position + sf::Vector2f(_fullLength / 2.0f, 0.0f));

		_textureLength = _barMid.getLocalBounds().width;

		_barMid.setScale((_fullLength / _textureLength), _scaleAcross);
		_barStart.setScale(1.0f, _scaleAcross);
		_barEnd.setScale(1.0f, _scaleAcross);
	}
	else {
		LoadTextureToSprite(textureName + "Bot", _barStart);
		_barStart.setOrigin(_barStart.getLocalBounds().width / 2.0f, 0.0f);
		_barStart.setPosition(_position + sf::Vector2f(0.0f, _fullLength / 2.0f));

		LoadTextureToSprite(textureName + "MidV", _barMid);
		_barMid.setOrigin(_barMid.getLocalBounds().width / 2.0f, _barMid.getLocalBounds().height);
		_barMid.setPosition(_position + sf::Vector2f(0.0f, _fullLength / 2.0f));

		LoadTextureToSprite(textureName + "Top", _barEnd);
		_barEnd.setOrigin(_barEnd.getLocalBounds().width / 2.0f, _barEnd.getLocalBounds().height);
		_barEnd.setPosition(_position + sf::Vector2f(0.0f, -_fullLength / 2.0f));
		
		_textureLength = _barMid.getLocalBounds().height;

		_barMid.setScale(_scaleAcross, (_fullLength / _textureLength));
		_barStart.setScale(_scaleAcross, 1.0f);
		_barEnd.setScale(_scaleAcross, 1.0f);
	}

	//This might only call SetLoaded(), possibly more in the future
	VisibleObject::Load("");
}

void ValueBar::Draw(sf::RenderWindow & window)
{
	if (IsLoaded() && IsShowing()) {
		window.draw(_barStart);
		window.draw(_barMid);
		window.draw(_barEnd);
	}
}

void ValueBar::Resize(float newValue)
{
	if (_value != newValue) {

		if (!_orientation) {
			_barMid.setScale((newValue / _fullValue) * (_fullLength / _textureLength), _scaleAcross);
			_barEnd.move(-((_value - newValue) / _fullValue) * _fullLength, 0.0f);
		}
		else {
			_barMid.setScale(_scaleAcross, (newValue / _fullValue) * (_fullLength / _textureLength));
			_barEnd.move(0.0f, ((_value - newValue) / _fullValue) * _fullLength);
		}

		_value = newValue;
	}
}
