#pragma once
#include "VisibleObject.h"

class Simple :
	public VisibleObject
{
public:
	Simple(std::string textureName);

	void Update(float elapsedTime);
};