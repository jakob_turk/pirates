#include "stdafx.h"
#include "GameConfig.h"


//Game settings
std::string Config::GAME_NAME = "Pirates";
int Config::SCREEN_WIDTH = 1600;
int Config::SCREEN_HEIGHT = 900;
int Config::TILE_SIZE = 64;
int Config::SCREEN_MOVE_SENSITIVITY = 1;	//% of screen (at border) that makes the screen move

//Gameplay settings
int Config::BOAT_HP = 100;
int Config::BOAT_SPEED = 160;

//Level settings
int Config::MAP_WIDTH = Config::SCREEN_WIDTH;
int Config::MAP_HEIGHT = Config::SCREEN_HEIGHT;

bool Config::TUTORIAL = true;
int Config::TUTORIAL_STAGE = 0;

//Sandbox level settings
int Config::SANDBOX_FACTIONS_N = 1;
int Config::SANDBOX_BOATS_N = 1;
bool Config::SANDBOX_FACTIONS_AGGRESSIVE = false;
