#pragma once
#include "SFML/Window.hpp"
#include "SFML/Graphics.hpp"

#include "ServiceLocator.h"
#include "GameObjectManager.h"
#include "MenuManager.h"
#include "GameState.h"

class Game
{
public:
	static void Start();

	static GameObjectManager& GetGameObjectManager();
	static MenuManager& GetMenuManager();
	static GameState::GameStateType GetGameState();

	static void ChangeGameState(GameState::GameStateType newGameState);

private:
	static bool IsExiting();
	static void SetGameState(GameState::GameStateType newGameState);

	static void GameLoop();

	static GameState* _gameState;
	static GameObjectManager _gameObjectManager;
	static MenuManager _menuManager;
	static sf::Clock _clock;

	static std::map<GameState::GameStateType, GameState*> _gameStates;
	static GameState::GameStateType _newGameState;

};
