#pragma once
#include "Animation.h"

class A_Crew :
	public Animation
{
public:
	A_Crew(sf::Vector2f position, sf::Vector2f velocity = sf::Vector2f(), 
		float length = 5, int crewMember = 0, float frameLen = 0.5f);
	~A_Crew();

	void Load();
	void Update(float timeDelta);
private:
	int _crewMember;
	float _length;
	float _duration;
};