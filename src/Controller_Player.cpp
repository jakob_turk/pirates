#include "stdafx.h"
#include "Controller_Player.h"

#include "Game.h"
#include "UIcontrolls.h"

void Controller_Player::Control(Boat& boat)
{
	// First loop over events and handle the events (clicks through UI follow)
	//This only covers abilities not on the UI board - those are checked by their ability buttons
	//but handled here in the next switch

	//Check for right click - movement
	if (ServiceLocator::GetController()->IsMousePressed())
		SetTargetLocation(boat);

	//Check for A press - shooting (this specifically allows for rapid shooting by keeping A key pressed)
	if (ServiceLocator::GetController()->IsKeyPressed(A))
		Shoot(boat);

	//Check for Shift press - anchoring
	if (!boat.IsAnchored() &&  ServiceLocator::GetController()->IsKeyPressed(Shift))
		boat.Anchor();

	//Check for Shift release - unanchoring
	if ( boat.IsAnchored() && !ServiceLocator::GetController()->IsKeyPressed(Shift))
		boat.Unanchor();


	// Then handle UI clicks
	for (auto clickedAbility : UIcontrolls::GetClickedAbilities()) {
		switch (clickedAbility) {
		case Ability::None:
			break;

		case Ability::SetNormalProj:
			boat.GetCannon()->SetCannonType(Projectile::NormalProj);
			break;

		case Ability::SetFireProj:
			boat.GetCannon()->SetCannonType(Projectile::FireProj);
			break;

		case Ability::SetExplosiveProj:
			boat.GetCannon()->SetCannonType(Projectile::ExplosiveProj);
			break;
		}
	}
}

void Controller_Player::SetTargetLocation(Boat & boat)
{
	boat.SetTargetLocation(ServiceLocator::GetViewer()->GetWindow().mapPixelToCoords(
								sf::Mouse::getPosition(ServiceLocator::GetViewer()->GetWindow())));
}

void Controller_Player::Shoot(Boat& boat)
{
	boat.GetCannon()->Shoot(boat.GetPosition(),
							ServiceLocator::GetViewer()->GetWindow().mapPixelToCoords(
								sf::Mouse::getPosition(ServiceLocator::GetViewer()->GetWindow())),
							boat.GetFaction());
}
