#include "StdAfx.h"
#include "VisibleObject.h"

#include "ServiceLocator.h"
#include "Game.h"

VisibleObject::VisibleObject() :
	_zLevel(5)
{}

VisibleObject::~VisibleObject() {}

void VisibleObject::Load(std::string mainTextureName)
{
	//If a child needs to load other sprites, it may do so in its Load overload and at the end calls VisibleObject::Load

	if (mainTextureName != "") {
		LoadTexture(mainTextureName);
		_sprite.setOrigin(GetSprite().getLocalBounds().width / 2, GetSprite().getLocalBounds().height / 2);

		SetVertices();
		SetRadius();
	}
	SetLoaded();
}

void VisibleObject::LoadTexture(std::string mainTextureName)
{
	LoadTextureToSprite(mainTextureName, GetSprite());
}

void VisibleObject::LoadTextureToSprite(std::string mainTextureName, sf::Sprite& sprite)
{
	ServiceLocator::GetTextures()->SetTexture(mainTextureName, sprite);
}

void VisibleObject::SetVertices() {
	_vertices.push_back( sf::Vector2i((int)_sprite.getLocalBounds().left, (int)_sprite.getLocalBounds().top));
	_vertices.push_back( sf::Vector2i((int)(_sprite.getLocalBounds().left + _sprite.getLocalBounds().width), (int)_sprite.getLocalBounds().top) );
	_vertices.push_back( sf::Vector2i((int)(_sprite.getLocalBounds().left + _sprite.getLocalBounds().width), (int)(_sprite.getLocalBounds().top + _sprite.getLocalBounds().height)) );
	_vertices.push_back( sf::Vector2i((int)_sprite.getLocalBounds().left, (int)(_sprite.getLocalBounds().top + _sprite.getLocalBounds().height)));
}

void VisibleObject::SetRadius()
{
	_radius = 0.5f*std::sqrt(_sprite.getLocalBounds().height * _sprite.getLocalBounds().height +
							 _sprite.getLocalBounds().width * _sprite.getLocalBounds().width);
}

void VisibleObject::Draw(sf::RenderWindow & renderWindow) {

	if (IsLoaded()) {
		renderWindow.draw(_sprite);
	}
}
//If a child has multiple sprites, it has to overload Draw to draw all of them
//There could be a vector of sprites, and than stuff would handle that,
//but currently 99% of objects would have only one sprite in it so it deasn't make sense right now.

float VisibleObject::GetWidth() const {

	return _sprite.getLocalBounds().width;
}

float VisibleObject::GetHeight() const {

	return _sprite.getLocalBounds().height;
}

float VisibleObject::GetRadius() const
{
	return _radius;
}

sf::FloatRect VisibleObject::GetRect() const
{
	return _sprite.getGlobalBounds();
}

sf::Vector2f VisibleObject::GetPosition() const {

	if (IsLoaded()) {
		return _sprite.getPosition();
	}
	return sf::Vector2f();
}

float VisibleObject::GetRotation() const {
	float rotat = _sprite.getRotation() + 90.0f;
	if (rotat < 180.0f) return inRad(rotat);
	else				return inRad(rotat - 360.0f);
}

int VisibleObject::GetZLevel() const {
	return _zLevel;
}

void VisibleObject::SetPosition(float x, float y)
{
	if (IsLoaded()) {
		_sprite.setPosition(x, y);
	}
}

void VisibleObject::SetPosition(sf::Vector2f position) {

	if (IsLoaded()) {
		_sprite.setPosition(position);
	}
}

void VisibleObject::SetRotation(float angle) {

	if (IsLoaded()) {
		_sprite.setRotation(inDeg(angle) - 90.0f);
	}
}

void VisibleObject::SetZLevel(int zLevel) {
	if (IsLoaded()) {
		_zLevel = zLevel;
	}
}

void VisibleObject::SetScale(sf::Vector2f scale)
{
	if (IsLoaded()) {
		_sprite.setScale(scale);
	}
}

void VisibleObject::SetSize(sf::Vector2f size)
{
	if (IsLoaded()) {
		_sprite.setScale(size.x / _sprite.getTextureRect().width, size.y / _sprite.getTextureRect().height);
	}
}

std::vector<sf::Vector2i> VisibleObject::GetVertices() {

	return _vertices;
}

sf::Sprite& VisibleObject::GetSprite()
{
	return _sprite;
}