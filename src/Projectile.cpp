#include "stdafx.h"
#include "Projectile.h"

#include "Game.h"

//Map of projectile data: (damage, reach, speed)
std::map<Projectile::ProjectileType, std::vector<float>> Projectile::PROJECTILE_DATA = {
	{Projectile::NormalProj, std::vector<float>{10.0f, 600.0f, 400.0f}},
	{Projectile::FireProj, std::vector<float>{20.0f, 500.0f, 380.0f}},
	{Projectile::ExplosiveProj, std::vector<float>{30.0f, 400.0f, 300.0f}}
};

Projectile::Projectile(sf::Vector2f position, float angle, BoatFaction ownerFaction, ProjectileType type) :
	_type(type),
	_dist(0.0f),
	_distFromExplosion(0.0f),
	_faction(ownerFaction),
	_flame(NULL)
{
	Load("projectile");
	Game::GetGameObjectManager().Add("projectile", this);
	assert(IsLoaded());

	SetZLevel(6);
	SetPosition(position);

	_damage = PROJECTILE_DATA[_type][0];
	_reach = PROJECTILE_DATA[_type][1];
	SetMaxSpeed(PROJECTILE_DATA[_type][2]);
	SetVelocity(GetMaxSpeed(), angle);

	if (_type == FireProj)
		_flame = AnimationFactory::Make(Anim::Flame1, GetPosition() +
													  rotated(sf::Vector2f(-17.0f, 0.0f), inDeg(angle)),
										angle, 2, 0.1f, 6.0f, GetVelocity());
}

float Projectile::GetDamage() {
	return _damage;
}

BoatFaction Projectile::GetFaction() {
	return _faction;
}

//Overloads Update to add deletion of off screen projectiles
void Projectile::Update(float elapsedTime)
{
	//Checking out of bounds is a little redundant now, as projectiles have finite reach and are allowed a bit
	//off-screen even. Still here to evade any possible mishaps and rogue projectiles.
	if ( GetPosition().y < 0 - GetHeight() - 100.0f ||
		 GetPosition().y > Config::MAP_HEIGHT + GetHeight() + 100.0f ||
		 GetPosition().x < 0 - GetWidth() - 100.0f ||
		 GetPosition().x > Config::MAP_WIDTH + GetWidth() + 100.0f)

		SetDeletion();

	if (_dist > _reach)
		Miss();

	//MakeExplosion for Explosive projectile
	if (_type == ExplosiveProj && _distFromExplosion > 30.0f) {
		AnimationFactory::Make(Anim::ShortExplosion, GetPosition());
		_distFromExplosion = 0.0f;
	}

	MovableObject::Update(elapsedTime);
	_dist += GetSpeed() * elapsedTime;
	if (_type == ExplosiveProj)
		_distFromExplosion += GetSpeed() * elapsedTime;
}

void Projectile::BoatCollision(Boat& boat)
{
	boat.GetHit(_damage);
	AnimationFactory::Make(Anim::Explosion, GetPosition(), 0.0f, 10, 0.0f, 0.0f, 0.8f*boat.GetVelocity() + 0.1f*GetVelocity());
	if (_flame) {
		_flame->SetDeletion();
	}

	SetDeletion();
}

void Projectile::Miss()
{
	AnimationFactory::Make(Anim::Splash, GetPosition());

	if (_flame) {
		_flame->SetVelocity(sf::Vector2f(0.0f, 0.0f));
		_flame->SetRotation(0.0f);
	}
	SetDeletion();
}