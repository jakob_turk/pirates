#include "stdafx.h"
#include "Tutorial.h"

#include "Game.h"
#include "BoatFactory.h"

#include "PopUp.h"
#include "Controller_AI.h"

/*
Stages of tutorial:
0 - init Welcome
1 - Control the ship by right clicking anywhere on the map
2 - Control the viewport by dragging your mouse to the edges of the screen and scrolling
3 - Pressing space will always move the viewport back to your ship
4 - Throw your anchor down by pressing SHIFT - this allows you to quickly stop or turn in place
5 - Shoot in the direction of your mouse by clicking A
6 - Notice the ammo count below. You only have 10 cannon balls to use up quickly,
    but they will always reload after a short period.
7 - There are 2 more cannon ball types - burning and explosive. Each does more damage, but
    has shorter reach than the last. To choose the type of cannon balls currently in use,
    press Q, W or E to swap to appropriate cannon.
8 - Great, now try shooting again with A!
9 - Unlike classic cannon balls, these special stronger projectiles will run out - you only
    have 20 burning projectiles and 3 explosives to spend.
10 - You know the basics, now find an enemy ship and sink it!
11 - Dank.
 */

Tutorial::Tutorial():
	_tutorialStage(0)
{
	Config::TUTORIAL_STAGE = 0;

	Game::GetGameObjectManager().Add(new PopUp(
		sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 2.0f),
		sf::Vector2f(Config::SCREEN_WIDTH / 4.0f, Config::SCREEN_HEIGHT / 5.0f),
		"Here's a quick guide to\nsailing these seas",
		[]() {
			Config::TUTORIAL_STAGE++;
		}));
}

void Tutorial::Update(float ElapsedTime)
{
	if (_tutorialStage != Config::TUTORIAL_STAGE) {
		++_tutorialStage;
		switch (_tutorialStage) {
		case 1:
			Game::GetGameObjectManager().Add(new PopUp(
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 2.0f),
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 6.5f),
				"Control the ship by right clicking anywhere on the map",
			[]() {
					Config::TUTORIAL_STAGE++;
				}));
			break;
		case 2:
			Game::GetGameObjectManager().Add(new PopUp(
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 3.0f / 4.0f),
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 5.0f),
				"Control the viewport by dragging your mouse\nto the edges of the screen and scrolling",
				[]() {
					Config::TUTORIAL_STAGE++;
				}));
			break;
		case 3:
			Game::GetGameObjectManager().Add(new PopUp(
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 3.0f / 4.0f),
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 5.0f),
				"Pressing space will always\nmove the viewport back to your ship",
				[]() {
					Config::TUTORIAL_STAGE++;
				}));
			break;
		case 4:
			Game::GetGameObjectManager().Add(new PopUp(
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 3.0f / 4.0f),
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 5.0f),
				"Throw your anchor down by pressing SHIFT.\nThis allows you to quickly stop or turn in place",
				[]() {
					Config::TUTORIAL_STAGE++;
				}));
			break;
		case 5:
			Game::GetGameObjectManager().Add(new PopUp(
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 3.0f / 4.0f),
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 6.5f),
				"Shoot in the direction of your mouse by pressing A",
				[]() {
					Config::TUTORIAL_STAGE++;
				}, 0));
			break;
		case 6:
			Game::GetGameObjectManager().Add(new PopUp(
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 3.0f / 4.0f),
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 4.5f),
				"Notice the ammo count below.\nYou only have 10 cannon balls to use up quickly,\n"
				"but they will always reload after a short period.",
				[]() {
					Config::TUTORIAL_STAGE++;
				}, 0));
			break;
		case 7:
			Game::GetGameObjectManager().Add(new PopUp(
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 3.0f / 4.0f),
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 5.0f),
				"There are 2 more cannon ball types:\n burning and explosive. ",
				[]() {
					Config::TUTORIAL_STAGE++;
				}, 0));
			break;
		case 8:
			Game::GetGameObjectManager().Add(new PopUp(
				sf::Vector2f(Config::SCREEN_WIDTH *3.0f / 4.0f, Config::SCREEN_HEIGHT * 14.0f / 20.0f),
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 4.5f),
				"Each does more damage, but has shorter reach than the last.\n"
				"To choose the type of cannon balls currently in use,\n"
				"press Q, W or E to swap to appropriate cannon.",
				[]() {
					Config::TUTORIAL_STAGE++;
				}, 0));
			break;
		case 9:
			Game::GetGameObjectManager().Add(new PopUp(
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 3.0f / 4.0f),
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 6.5f),
				"Great, now try shooting again with A!",
				[]() {
					Config::TUTORIAL_STAGE++;
				}, 0));
			break;
		case 10:
			Game::GetGameObjectManager().Add(new PopUp(
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 3.0f / 4.0f),
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 4.5f),
				"Unlike classic cannon balls, these special stronger projectiles\nwill run out - you only "
				"have 20 burning projectiles\nand 3 explosives to spend.",
				[]() {
					Config::TUTORIAL_STAGE++;
				}, 0));
			break;
		case 11:
			Game::GetGameObjectManager().Add(new PopUp(
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 3.0f / 4.0f),
				sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 6.5f),
				"You know the basics, now find an enemy ship and sink it!",
				[]() {
					//Next stage reached by winning
					BoatFactory::Make(sf::Vector2f(2000.0f, 1500.0f), AI, BoatFaction::fBlack, 0.0f,
						sf::Vector2f(), "TutorialBoat");
					//Tutorial set to false to simply make scorer work again
					Config::TUTORIAL = false;
				}, 0));
			break;
		}
	}
}
