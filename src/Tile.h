#pragma once
#include "VisibleObject.h"

class Tile :
	public VisibleObject
{
public:
	enum TileType {
		Sea, BackgroundSea,
		Desert1, Desert2, Desert3,
		DesertCoastNW, DesertCoastN, DesertCoastNE, DesertCoastE,
		DesertCoastSE, DesertCoastS, DesertCoastSW, DesertCoastW,
		DesertPondSE, DesertPondSW, DesertPondNW, DesertPondNE,
		GrassPondSE, GrassPondSW, GrassPondNW, GrassPondNE,
		GrassCoastNW, GrassCoastN1, GrassCoastN2, GrassCoastNE, GrassCoastE1, GrassCoastE2,
		GrassCoastSE, GrassCoastS1, GrassCoastS2, GrassCoastSW, GrassCoastW1, GrassCoastW2,
		Grass1, Grass2, Grass3, Grass4,
		ShallowCoastNW, ShallowCoastN, ShallowCoastNE, ShallowCoastE,
		ShallowCoastSE, ShallowCoastS, ShallowCoastSW, ShallowCoastW,
		ShallowIsleSE, ShallowIsleSW, ShallowIsleNW, ShallowIsleNE,
		Special1, Special2, Special3, Special4, Special5, Special6
	};

	Tile(TileType tileType, sf::Vector2f position, sf::IntRect tileSize = sf::IntRect());
	~Tile();

	void Update(float elapsedTime);

	void Load(sf::IntRect tileSize = sf::IntRect());

	sf::Vector2f GetPosition();
	TileType GetTyleType();

private:
	void SetVertices();
	std::string GetTextureName(Tile::TileType tileType);
	
	sf::Vector2f _position;
	TileType _tileType;
};