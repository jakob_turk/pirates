#pragma once
#include "GameObject.h"

#include "Button.h"

class RadioButton:
	public Button
{
public:
	RadioButton(sf::Vector2f position, sf::Vector2f size = sf::Vector2f());

	void LoadButtonColor(BtnColour color);
};