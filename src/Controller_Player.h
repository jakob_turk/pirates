#pragma once
#include "Controller.h"

class Controller_Player :
	public Controller
{
public:
	void Control(Boat& boat);

private:
	void SetTargetLocation(Boat& boat);
	void Shoot(Boat& boat);
};