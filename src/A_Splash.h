#pragma once
#include "Animation.h"

class A_Splash :
	public Animation
{
public:
	A_Splash(sf::Vector2f position);
	~A_Splash();

	void Load();
	void Update(float timeDelta);
};