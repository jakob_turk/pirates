#include "stdafx.h"
#include "SettingsMenu.h"

#include "Game.h"

#include "RadioButton.h"

SettingsMenu::SettingsMenu()
{
	MakeButtons();

	_menuSheet = new BackgroundSheet(
		sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 2.0f),
		Config::SCREEN_WIDTH * 3.0f / 4.0f, Config::SCREEN_HEIGHT * 3.0f / 4.0f, LBrown);
}

void SettingsMenu::MakeButtons()
{
	//Menu title
	_texts.push_back(new Text("Settings",
		sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 12.0f / 64.0f), 45));

	//HP number tags
	_texts.push_back(new Text("100", sf::Vector2f(Config::SCREEN_WIDTH * 25.0f / 50.0f, Config::SCREEN_HEIGHT * 21.0f / 64.0f), 25));
	_texts.push_back(new Text("500", sf::Vector2f(Config::SCREEN_WIDTH * 28.0f / 50.0f, Config::SCREEN_HEIGHT * 21.0f / 64.0f), 25));
	_texts.push_back(new Text("1000", sf::Vector2f(Config::SCREEN_WIDTH * 31.0f / 50.0f, Config::SCREEN_HEIGHT * 21.0f / 64.0f), 25));

	//Factions radio buttons
	_texts.push_back(new Text("Boat HP:",
		sf::Vector2f(Config::SCREEN_WIDTH * 20.0f / 50.0f, Config::SCREEN_HEIGHT * 23.0f / 64.0f), 30));

	_buttons[HP1] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 25.0f / 50.0f, Config::SCREEN_HEIGHT * 23.0f / 64.0f));
	_buttons[HP2] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 28.0f / 50.0f, Config::SCREEN_HEIGHT * 23.0f / 64.0f));
	_buttons[HP3] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 31.0f / 50.0f, Config::SCREEN_HEIGHT * 23.0f / 64.0f));
	DealWithButtonClick(HP1);

	//Speed number tags
	_texts.push_back(new Text("1", sf::Vector2f(Config::SCREEN_WIDTH * 25.0f / 50.0f, Config::SCREEN_HEIGHT * 27.0f / 64.0f), 25));
	_texts.push_back(new Text("2", sf::Vector2f(Config::SCREEN_WIDTH * 28.0f / 50.0f, Config::SCREEN_HEIGHT * 27.0f / 64.0f), 25));
	_texts.push_back(new Text("3", sf::Vector2f(Config::SCREEN_WIDTH * 31.0f / 50.0f, Config::SCREEN_HEIGHT * 27.0f / 64.0f), 25));


	//Boats radio buttons
	_texts.push_back(new Text("Boat speed:",
		sf::Vector2f(Config::SCREEN_WIDTH * 20.0f / 50.0f, Config::SCREEN_HEIGHT * 29.0f / 64.0f), 30));

	_buttons[speed1] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 25.0f / 50.0f, Config::SCREEN_HEIGHT * 29.0f / 64.0f));
	_buttons[speed2] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 28.0f / 50.0f, Config::SCREEN_HEIGHT * 29.0f / 64.0f));
	_buttons[speed3] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 31.0f / 50.0f, Config::SCREEN_HEIGHT * 29.0f / 64.0f));
	DealWithButtonClick(speed1);

	//Faction aggresion
	_texts.push_back(new Text("Factions attack each other:",
		sf::Vector2f(Config::SCREEN_WIDTH * 17.0f / 50.0f, Config::SCREEN_HEIGHT * 35.0f / 64.0f), 30));
	
	_buttons[factionAggression] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 25.0f / 50.0f, Config::SCREEN_HEIGHT * 35.0f / 64.0f));


	//Back button
	sf::Vector2f backPosition = sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 52.0f / 64.0f);
	_buttons[Back] = new Button(backPosition, Brown, sf::Vector2f(), new Text("Back", backPosition, 40, sf::Color::White));
}

void SettingsMenu::DealWithButtonClick(ButtonType button)
{
	switch (button) {
	case HP1:
		_buttons[HP1]->LoadButtonColor(LBrown);
		_buttons[HP2]->LoadButtonColor(Brown);
		_buttons[HP3]->LoadButtonColor(Brown);
		Config::BOAT_HP = 100;
		break;
	case HP2:
		_buttons[HP1]->LoadButtonColor(Brown);
		_buttons[HP2]->LoadButtonColor(LBrown);
		_buttons[HP3]->LoadButtonColor(Brown);
		Config::BOAT_HP = 500;
		break;
	case HP3:
		_buttons[HP1]->LoadButtonColor(Brown);
		_buttons[HP2]->LoadButtonColor(Brown);
		_buttons[HP3]->LoadButtonColor(LBrown);
		Config::BOAT_HP = 1000;
		break;
	case speed1:
		_buttons[speed1]->LoadButtonColor(LBrown);
		_buttons[speed2]->LoadButtonColor(Brown);
		_buttons[speed3]->LoadButtonColor(Brown);
		Config::BOAT_SPEED = 160;
		break;
	case speed2:
		_buttons[speed1]->LoadButtonColor(Brown);
		_buttons[speed2]->LoadButtonColor(LBrown);
		_buttons[speed3]->LoadButtonColor(Brown);
		Config::BOAT_SPEED = 220;
		break;
	case speed3:
		_buttons[speed1]->LoadButtonColor(Brown);
		_buttons[speed2]->LoadButtonColor(Brown);
		_buttons[speed3]->LoadButtonColor(LBrown);
		Config::BOAT_SPEED = 300;
		break;
	case factionAggression:
		if (Config::SANDBOX_FACTIONS_AGGRESSIVE) {
			_buttons[factionAggression]->LoadButtonColor(Brown);
			Config::SANDBOX_FACTIONS_AGGRESSIVE = false;
		}
		else {
			_buttons[factionAggression]->LoadButtonColor(LBrown);
			Config::SANDBOX_FACTIONS_AGGRESSIVE = true;
		}
		break;

	case Back:
		Game::GetMenuManager().ChangeMenu(Main);
		break;
	}
}
