#include "stdafx.h"
#include "SandboxMenu.h"

#include "Game.h"

#include "RadioButton.h"

SandboxMenu::SandboxMenu()
{
	MakeButtons();

	_menuSheet = new BackgroundSheet(
		sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 2.0f),
		Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT / 2.0f, LBrown);
}

void SandboxMenu::MakeButtons()
{
	//Menu title
	_texts.push_back(new Text("Test level",
		sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 19.0f / 64.0f), 40));

	//Number tags
	_texts.push_back(new Text("1", sf::Vector2f(Config::SCREEN_WIDTH * 25.0f / 50.0f, Config::SCREEN_HEIGHT * 26.0f / 64.0f), 25));
	_texts.push_back(new Text("2", sf::Vector2f(Config::SCREEN_WIDTH * 28.0f / 50.0f, Config::SCREEN_HEIGHT * 26.0f / 64.0f), 25));
	_texts.push_back(new Text("3", sf::Vector2f(Config::SCREEN_WIDTH * 31.0f / 50.0f, Config::SCREEN_HEIGHT * 26.0f / 64.0f), 25));

	//Factions radio buttons
	_texts.push_back(new Text("Number of factions:",
		sf::Vector2f(Config::SCREEN_WIDTH * 20.0f / 50.0f, Config::SCREEN_HEIGHT * 29.0f / 64.0f), 25));

	_buttons[nFactions1] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 25.0f / 50.0f, Config::SCREEN_HEIGHT * 29.0f / 64.0f));
	_buttons[nFactions2] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 28.0f / 50.0f, Config::SCREEN_HEIGHT * 29.0f / 64.0f));
	_buttons[nFactions3] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 31.0f / 50.0f, Config::SCREEN_HEIGHT * 29.0f / 64.0f));
	DealWithButtonClick(nFactions1);

	//Boats radio buttons
	_texts.push_back(new Text("Number of boats\nper faction:",
		sf::Vector2f(Config::SCREEN_WIDTH * 20.0f / 50.0f, Config::SCREEN_HEIGHT * 34.0f / 64.0f), 25));

	_buttons[nBoats1] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 25.0f / 50.0f, Config::SCREEN_HEIGHT * 33.0f / 64.0f));
	_buttons[nBoats2] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 28.0f / 50.0f, Config::SCREEN_HEIGHT * 33.0f / 64.0f));
	_buttons[nBoats3] = new RadioButton(sf::Vector2f(Config::SCREEN_WIDTH * 31.0f / 50.0f, Config::SCREEN_HEIGHT * 33.0f / 64.0f));
	DealWithButtonClick(nBoats1);

	//Tutorial button
	sf::Vector2f tutorialPosition = sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 40.0f / 64.0f);
	_buttons[Tutorial] = new Button(tutorialPosition, Brown, sf::Vector2f(), new Text("Tutorial", tutorialPosition, 40, sf::Color::White));

	//Back button
	sf::Vector2f backPosition = sf::Vector2f(Config::SCREEN_WIDTH / 3.0f, Config::SCREEN_HEIGHT * 45.0f / 64.0f);
	_buttons[Back] = new Button(backPosition, Brown, sf::Vector2f(), new Text("Back", backPosition, 40, sf::Color::White));

	//Play button
	sf::Vector2f playPosition = sf::Vector2f((float)(Config::SCREEN_WIDTH * 2.0f / 3.0f), (float)(Config::SCREEN_HEIGHT * 45.0f / 64.0f));
	_buttons[Play] = new Button(playPosition, Brown, sf::Vector2f(), new Text("Play", playPosition, 40, sf::Color::White));
}

void SandboxMenu::DealWithButtonClick(ButtonType button)
{
	switch (button) {
	case nFactions1:
		_buttons[nFactions1]->LoadButtonColor(LBrown);
		_buttons[nFactions2]->LoadButtonColor(Brown);
		_buttons[nFactions3]->LoadButtonColor(Brown);
		Config::SANDBOX_FACTIONS_N = 1;
		break;
	case nFactions2:
		_buttons[nFactions1]->LoadButtonColor(Brown);
		_buttons[nFactions2]->LoadButtonColor(LBrown);
		_buttons[nFactions3]->LoadButtonColor(Brown);
		Config::SANDBOX_FACTIONS_N = 2;
		break;
	case nFactions3:
		_buttons[nFactions1]->LoadButtonColor(Brown);
		_buttons[nFactions2]->LoadButtonColor(Brown);
		_buttons[nFactions3]->LoadButtonColor(LBrown);
		Config::SANDBOX_FACTIONS_N = 3;
		break;
	case nBoats1:
		_buttons[nBoats1]->LoadButtonColor(LBrown);
		_buttons[nBoats2]->LoadButtonColor(Brown);
		_buttons[nBoats3]->LoadButtonColor(Brown);
		Config::SANDBOX_BOATS_N = 1;
		break;
	case nBoats2:
		_buttons[nBoats1]->LoadButtonColor(Brown);
		_buttons[nBoats2]->LoadButtonColor(LBrown);
		_buttons[nBoats3]->LoadButtonColor(Brown);
		Config::SANDBOX_BOATS_N = 2;
		break;
	case nBoats3:
		_buttons[nBoats1]->LoadButtonColor(Brown);
		_buttons[nBoats2]->LoadButtonColor(Brown);
		_buttons[nBoats3]->LoadButtonColor(LBrown);
		Config::SANDBOX_BOATS_N = 3;
		break;

	case Tutorial:
		Config::TUTORIAL = true;
		Config::TUTORIAL_STAGE = 0;
		Game::ChangeGameState(GameState::Load);
		break;

	case Play:
		Config::TUTORIAL = false;
		Config::TUTORIAL_STAGE = 0;
		Game::ChangeGameState(GameState::Load);
		break;

	case Back:
		Game::GetMenuManager().ChangeMenu(Main);
		break;
	}
}
