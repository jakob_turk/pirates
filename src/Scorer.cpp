#include "stdafx.h"
#include "Scorer.h"

#include "Game.h"

#include "EndScreen.h"

Scorer::Scorer()
{
}

void Scorer::Update(float elapsedTime)
{
	if (End())
		SetDeletion();
}

bool Scorer::End()
{
	if (Config::TUTORIAL) {
		return false;
	}

	if (!Game::GetGameObjectManager().Get("PlayerBoat")) {
		Loose();
		return true;
	}
	else if (Game::GetGameObjectManager().GetBoats().size() > 5)
		//So this is a bit of a daring optimization - only check each ship for !IsDinking if only
		//5 or less still exist. Meaning, if player starts sinking and after that final 5 boats start
		//sinking, player will loose, although they should win
		return false;
	else {
		for (auto boatObject : Game::GetGameObjectManager().GetBoats()) {
			if (boatObject.first != "PlayerBoat" && !boatObject.second->IsSinking())
				return false;
		}
		Win();
		return true;
	}
}

void Scorer::Loose()
{
	Game::GetGameObjectManager().Add(new EndScreen(false));
}

void Scorer::Win()
{
	if (Config::TUTORIAL_STAGE) {
		Config::TUTORIAL = true;
		++Config::TUTORIAL_STAGE;
	}
	Game::GetGameObjectManager().Add(new EndScreen(true));
}
