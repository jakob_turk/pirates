#include "stdafx.h"
#include "GameObjectManager.h"

#include "Game.h"
#include "Collider.h"
#include "ServiceLocator.h"
#include "IWindowProvider.h"

GameObjectManager::GameObjectManager()
{
}

GameObjectManager::~GameObjectManager()
{
	std::for_each(_boatObjects.begin(), _boatObjects.end(), BoatDeallocator());
	std::for_each(_projectiles.begin(), _projectiles.end(), ProjectileDeallocator());
	std::for_each(_UIobjects.begin(), _UIobjects.end(), VisibleDeallocator());
	std::for_each(_visibleObjects.begin(), _visibleObjects.end(), VisibleDeallocator());
	std::for_each(_gameObjects.begin(), _gameObjects.end(), GameObjectDeallocator());
}

void GameObjectManager::Add(std::string name, VisibleObject* gameObject)
{
	if (name == "projectile")
		_projectiles.push_back((Projectile*)gameObject);
	else if (name == "UI")
		_UIobjects.push_back(gameObject);
	else if (name == "other")
		_visibleObjects.push_back(gameObject);
	else 
		_boatObjects.insert(std::pair<std::string, Boat*>(name, (Boat*)gameObject));
}

void GameObjectManager::Add(Animation* animation)
{
	_visibleObjects.push_back((VisibleObject*)animation);
}

void GameObjectManager::Add(GameObject* gameObject)
{
	_gameObjects.push_back(gameObject);
}

void GameObjectManager::AddDrawable(VisibleObject* drawable)
{
	std::map<int, std::vector<VisibleObject*>>::iterator soughtZLevel = _drawTable.find(drawable->GetZLevel());
	if (soughtZLevel != _drawTable.end()) {
		soughtZLevel->second.push_back(drawable);
	} else {
		std::vector<VisibleObject*> levelDrawables;
		levelDrawables.push_back(drawable);
		_drawTable.insert(std::pair<int, std::vector<VisibleObject*>> (drawable->GetZLevel(), levelDrawables));
	}
}

void GameObjectManager::AddMinimapDrawable(VisibleObject * drawable)
{
	_minimapDrawTable.push_back(drawable);
}

void GameObjectManager::Remove(std::string name)
{
	std::map<std::string, Boat*>::iterator results = _boatObjects.find(name);
	if (results != _boatObjects.end()) {
		delete results->second;
		_boatObjects.erase(results);
	}
}

void GameObjectManager::Empty(bool soft)
{
	for (auto boatObj : _boatObjects)
		delete boatObj.second;
	_boatObjects.clear();
	for (Projectile* proj : _projectiles)
		delete proj;
	_projectiles.clear();
	for (VisibleObject* UIel : _UIobjects)
		delete UIel;
	_UIobjects.clear();
	for (VisibleObject* obj : _visibleObjects)
		delete obj;
	_visibleObjects.clear();
	for (GameObject* othr : _gameObjects)
		delete othr;
	_gameObjects.clear();

	if (!soft)
		_tileManager->Empty();
}

Boat* GameObjectManager::Get(std::string name) const
{
	std::map<std::string, Boat*>::const_iterator results = _boatObjects.find(name);
	if (results == _boatObjects.end())
		return NULL;
	return results->second;
}

std::map<std::string, Boat*>  GameObjectManager::GetBoats() const
{
	return _boatObjects;
}

//This is used mainly by GameState Loader
TileManager* GameObjectManager::GetTileManager() const
{
	return _tileManager;
}

int GameObjectManager::GetObjectCount() const
{
	return _boatObjects.size() +
		   _projectiles.size() +
		   _visibleObjects.size() +
		   _gameObjects.size() +
		   _UIobjects.size();
}


void GameObjectManager::DrawAll(sf::RenderWindow& renderWindow)
{
	//Tiles are of course drawn first (zLevel=0)
	_tileManager->DrawAll(renderWindow);

	//Cover the ordered(!) map of zLevels from 1 ->, drawing each VisibleObject in vectors of each level
	for (std::map<int, std::vector<VisibleObject*>>::iterator i = _drawTable.begin(); i != _drawTable.end(); i++) {
		
		for (std::vector<VisibleObject*>::iterator drawableItr = i->second.begin(); drawableItr != i->second.end(); ++drawableItr) {

			(*drawableItr)->Draw(renderWindow);
		}
	}

	_drawTable.clear();

	//Separately draw UI above the game
	ServiceLocator::GetViewer()->SetViewport(IWindowProvider::Default);

	for (VisibleObject* UIelement : _UIobjects)
		UIelement->Draw(renderWindow);

	if (Game::GetGameState() == GameState::Playing)
	{
		//Draw the second viewport, minimap, on top of the UI
		ServiceLocator::GetViewer()->SetViewport(IWindowProvider::Minimap);
		
		_tileManager->DrawAll(renderWindow);
		for (VisibleObject* minimapElement : _minimapDrawTable)
			minimapElement->Draw(renderWindow);

		_minimapDrawTable.clear();
	}

	//After drawing to alternate viewports, set view back to game
	ServiceLocator::GetViewer()->SetViewport(IWindowProvider::Game);
}

void GameObjectManager::UpdateAll(float timeDelta)
{
	//Check for collisions firt, so that update can deal with it in the same frame
	for (std::map<std::string, Boat*>::iterator itr = _boatObjects.begin(); itr != _boatObjects.end(); ++itr) {
		//Check for collision with other boat objects
		for (std::map<std::string, Boat*>::iterator itrj = std::next(itr); itrj != _boatObjects.end(); ++itrj) {
			Collider::BoatBoatCollision(itr->second, itrj->second);
		}
	}

	//Then update projectiles, so boats will deal with that too
	for (unsigned int i = 0; i < _projectiles.size(); ) {

		//Check if it wants deletion
		if (_projectiles[i]->WantsDeletion()) {
			delete _projectiles[i];
			_projectiles.erase(_projectiles.begin() + i);
		}
		else {
			//Check for collisions with boat objects
			for (std::map<std::string, Boat*>::iterator iBoat = _boatObjects.begin(); iBoat != _boatObjects.end(); ++iBoat) {
				//Pass collision with self
				if (iBoat->second->GetFaction() == _projectiles[i]->GetFaction()) {
					continue;
				}

				if (Collider::SATCollision(*_projectiles[i], *iBoat->second)) {
					_projectiles[i]->BoatCollision(*iBoat->second);
				}

			}

			//Finally update
			_projectiles[i]->Update(timeDelta);
			AddDrawable(_projectiles[i]);
			++i;
		}
	}

	//Finally update all boats
	for (std::map<std::string, Boat*>::iterator itr = _boatObjects.begin(); itr != _boatObjects.end(); ) {

		//Check if it wants deletion
		if (itr->second->WantsDeletion()) {
			delete itr->second;
			itr = _boatObjects.erase(itr);
		}
		//Else update
		else {
			itr->second->Update(timeDelta);
			AddDrawable(itr->second);

			if (Game::GetGameState() == GameState::Playing)
				AddMinimapDrawable(itr->second);

			++itr;
		}
	}

	//Finally update all other visible objects
	for (std::vector<VisibleObject*>::iterator itr = _visibleObjects.begin(); itr != _visibleObjects.end(); ) {

		//Check if it wants deletion
		if ((*itr)->WantsDeletion()) {
			delete *itr;
			itr = _visibleObjects.erase(itr);
		}
		//Else update
		else {
			(*itr)->Update(timeDelta);
			AddDrawable(*itr);
			++itr;
		}
	}

	//And the very final end update invisible objects
	for (std::list<GameObject*>::iterator itr = _gameObjects.begin(); itr != _gameObjects.end(); ) {

		//Check if it wants deletion
		if ((*itr)->WantsDeletion()) {
			delete *itr;
			itr = _gameObjects.erase(itr);
		}
		//Else update
		else {
			(*itr)->Update(timeDelta);
			++itr;
		}
	}

	//And then also after that update UI
	for (std::vector<VisibleObject*>::iterator itr = _UIobjects.begin(); itr != _UIobjects.end(); ) {

		//Check if it wants deletion
		if ((*itr)->WantsDeletion()) {
			delete *itr;
			itr = _UIobjects.erase(itr);
		}
		//Else update
		else {
			(*itr)->Update(timeDelta);
			++itr;
		}
	}
}