#pragma once

//This interface includes SettingsProvider as a window will always need access to some settings save there
#include "GameConfig.h"

class IWindowProvider
{
public:
	enum Viewport {
		Default, Game, Minimap
	};

	virtual sf::RenderWindow& GetWindow() = 0;
	virtual sf::View& GetViewport() = 0;
	virtual sf::View& GetMinimapViewPort() = 0;

	virtual void SetUpMinimapViewPort(float width, float height) = 0;
	virtual void MoveViewport(sf::Vector2f moveVector, float factor = 1) = 0;
	virtual void MoveViewportTo(sf::Vector2f newPosition) = 0;
	virtual void SetViewport(Viewport newViewport = Default) = 0;
	virtual void ZoomViewport(float factor) = 0;
	virtual void ResetZoom() = 0;
	virtual float GetZoom() = 0;
};