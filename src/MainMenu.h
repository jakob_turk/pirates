#pragma once
#include "Menu.h"

class MainMenu :
	public Menu
{
public:
	MainMenu();

	void Call();

protected:
	void MakeButtons();
	void DealWithButtonClick(ButtonType button);
};