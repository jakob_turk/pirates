#include "stdafx.h"
#include "Collider.h"

#include "TileManager.h"

sf::Vector2f Collider::_latestMTVobjA = sf::Vector2f(0.0f, 0.0f);
sf::Vector2f Collider::_latestMTVobjB = sf::Vector2f(0.0f, 0.0f);

void Collider::BoatBoatCollision(Boat * boatA, Boat * boatB)
{
	if (SphereCollision(*boatA, *boatB, 1.0f)) {
		if ((!boatA->IsSinking() || !boatB->IsSinking()) &&
			SATCollision(*boatA, *boatB, false)) {
			boatA->BoatCollision(Collider::GetMTVa(), boatB);
			boatB->BoatCollision(Collider::GetMTVb(), boatA);
		}
		else {
			boatA->SetPreventCollision(boatB);
			boatB->SetPreventCollision(boatA);
		}
	}
}

sf::Vector2f Collider::BoatTileColission(VisibleObject* object, std::vector<Tile*> tiles)
{
	std::vector<sf::Vector2f> MTVs = std::vector<sf::Vector2f>();

	for (Tile* tile : tiles) {
		if (SATCollision(*object, *tile))
			MTVs.push_back(GetMTVa());
	}

	sf::Vector2f finalMTV = sf::Vector2f();
	for (sf::Vector2f MTV : MTVs) {
		finalMTV += MTV;
	}

	//Currently not dependent on overlap, just the axis normal.
	return normalize(finalMTV);
}

bool Collider::SphereCollision(VisibleObject & objectA, VisibleObject & objectB, float radiusFactor)
{
	return (objectA.GetPosition().x - objectB.GetPosition().x) *
		   (objectA.GetPosition().x - objectB.GetPosition().x) +
		   (objectA.GetPosition().y - objectB.GetPosition().y) *
		   (objectA.GetPosition().y - objectB.GetPosition().y)
			<
		   (objectA.GetRadius() + objectB.GetRadius()) * radiusFactor *
		   (objectA.GetRadius() + objectB.GetRadius()) * radiusFactor;
}

bool Collider::SATCollision(VisibleObject& objectA, VisibleObject& objectB, bool sphereCheck)
{
	//Sphere collision check
	////////////////////////
	if (sphereCheck)
		if (!SphereCollision(objectA, objectB))
			return false;

	//SAT collision
	///////////////
	std::vector<sf::Vector2f> projectionAxes;

	GetProjectionAxes(projectionAxes, objectA);
	int aEdges = projectionAxes.size();
	GetProjectionAxes(projectionAxes, objectB);

	float overlap = -1.0f;
	int smallestAxisI;

	for (unsigned int i = 0; i < projectionAxes.size(); ++i) {

		std::vector<float> projA = GetProjection(projectionAxes[i], objectA);
		std::vector<float> projB = GetProjection(projectionAxes[i], objectB);

		float projOverlap = ProjectionsOverlap(projA, projB);
		if (!projOverlap) {
			return false;
		} else {
			if ( projOverlap < overlap || overlap == -1.0f) {
				overlap = projOverlap;
				smallestAxisI = i;
			}
		}
	}
	//Currently not dependent on overlap, just the axis normal. Collision shouldn't be bouncier if a boat boinks deeper into another ship.
	if (smallestAxisI < aEdges) {
		_latestMTVobjA = -projectionAxes[smallestAxisI];
		_latestMTVobjB =  projectionAxes[smallestAxisI];
	} else {
		_latestMTVobjA =  projectionAxes[smallestAxisI];
		_latestMTVobjB = -projectionAxes[smallestAxisI];
	}

	return true;

}

void Collider::GetProjectionAxes(std::vector<sf::Vector2f>& projectionAxes, VisibleObject& object)
{
	for (unsigned int i = 0; i < object.GetVertices().size(); ++i) {
		sf::Transform rotation;
		rotation.rotate(inDeg(object.GetRotation()) - 90.0f);

		sf::Vector2f edge = rotation.transformPoint(fVec(object.GetVertices()[(i + 1) % object.GetVertices().size()]))
						  - rotation.transformPoint(fVec(object.GetVertices()[i]));
		projectionAxes.push_back( normalize( sf::Vector2f(edge.y, -edge.x) ) );
	}
}

std::vector<float> Collider::GetProjection(sf::Vector2f& Axis, VisibleObject& object)
{
	sf::Transform rotation;
	rotation.rotate(inDeg(object.GetRotation()) - 90.0f);
	
	float min = dotF(Axis, object.GetPosition() + rotation.transformPoint(fVec(object.GetVertices()[0]) - object.GetSprite().getOrigin()));
	float max = min;

	for (unsigned int i = 1; i < object.GetVertices().size(); ++i) {
		float product = dotF( Axis, object.GetPosition() + rotation.transformPoint( fVec(object.GetVertices()[i]) - object.GetSprite().getOrigin() ) );

		if		(product < min) min = product;
		else if (product > max) max = product;
	}

	std::vector<float> projection;
	projection.push_back(min);
	projection.push_back(max);

	return projection;
}

float Collider::ProjectionsOverlap(std::vector<float> proj1, std::vector<float> proj2)
{
	return std::max(0.0f, std::min(proj1[1], proj2[1]) - std::max(proj1[0], proj2[0]));
}

sf::Vector2f Collider::GetMTVa()
{
	return _latestMTVobjA;
}

sf::Vector2f Collider::GetMTVb()
{
	return _latestMTVobjB;
}

