#pragma once
#include "VisibleObject.h"

#include "Tile.h"
#include "Boat.h"

class Collider
{
public:
	static void BoatBoatCollision(Boat* boatA, Boat* boatB);
	static sf::Vector2f BoatTileColission(VisibleObject* object, std::vector<Tile*> tiles);

	static bool SphereCollision(VisibleObject& objectA, VisibleObject& objectB, float radiusFactor = 1.0f);
	static bool SATCollision(VisibleObject& objectA, VisibleObject& objectB, bool sphereCheck = true);
	static sf::Vector2f GetMTVa();
	static sf::Vector2f GetMTVb();

private:
	static void GetProjectionAxes(std::vector<sf::Vector2f>& projectionAxes, VisibleObject& object);
	static std::vector<float> GetProjection(sf::Vector2f& Axes, VisibleObject& object);
	static float ProjectionsOverlap(std::vector<float> proj1, std::vector<float> proj2);

	static sf::Vector2f _latestMTVobjA;
	static sf::Vector2f _latestMTVobjB;
};