#pragma once
class Boat;

class Pathfinder
{
public:
	Pathfinder();

	std::vector<float> FollowTargetLocation(sf::Vector2f position, sf::Vector2f targetLocation, sf::FloatRect boatRect);
private:
	std::vector<float> CrowFlight(sf::Vector2f position, sf::Vector2f target);
	void Astar();

	//Aux functions
	enum Heuristic {
		Euclidean, Manhattan, DiagonalManhattan
	};
	float AStarHeuristic(sf::Vector2i position, sf::Vector2i target, Heuristic heuristic = DiagonalManhattan);
	void MakePath(std::map<sf::Vector2i, sf::Vector2i, sfVector2Icompare> connections,
					sf::Vector2i end, bool forceCrowFlight, sf::Vector2f accurateTarget);

	//Pathfinding variables
	int _searchPrecision;	//This is the width of one square A* considers a node

	sf::Vector2f _currentTarget;
	sf::Vector2f _currentPosition;	//Only needed for Astar
	std::vector<sf::Vector2f> _currentPath;

	//Threading variables
	bool _searching;
	bool _searchDone;
	std::thread _aStarThread;
};