#pragma once

class ITextureProvider
{
public:
	virtual void SetTexture(std::string textureName, sf::Sprite& sprite, sf::IntRect textSize = sf::IntRect()) = 0;
	virtual std::vector<std::vector<int>> GetVertices(std::string textureName) = 0;
	virtual sf::Font GetFont() = 0;
};