#include "stdafx.h"
#include "SFMLTextures.h"

SFMLTextures::SFMLTextures()
{
	//TEXTURE SHEETS LOADING
	////////////////////////
	std::ifstream txPathsFile("data/texture_paths");
	std::string line;

	assert(txPathsFile.is_open());

	while (std::getline(txPathsFile, line))
	{
		//Lines are comented by a starting #
		if (line[0] == '#')
			continue;

		//Split line into: textureName, filePath, top, left, width, height
		std::istringstream buffer(line);
		std::istream_iterator<std::string> begin(buffer), end;
		std::vector<std::string> splitLine(begin, end);

		//If there are less than 6 elements in line, something is wrong
		if (splitLine.size() < 6) continue;

		//If pathfile is "SPRITESHEET" or "TILESHEET", the line only contains info about the location of texture on the sheet,
		//so save that in appropriate container. Otherwise save the texture in _textures (sheets too!)
		if		(splitLine[1] == "SPRITESHEET")
			_spriteSheetTextures.insert(std::pair<std::string, sf::IntRect>
				(splitLine[0], sf::IntRect(std::stoi(splitLine[2]),
										   std::stoi(splitLine[3]),
										   std::stoi(splitLine[4]),
										   std::stoi(splitLine[5]))));
		else if (splitLine[1] == "UISHEET")
			_uiSheetTextures.insert(std::pair<std::string, sf::IntRect>
				(splitLine[0], sf::IntRect(std::stoi(splitLine[2]),
										   std::stoi(splitLine[3]),
										   std::stoi(splitLine[4]),
										   std::stoi(splitLine[5]))));
		// In case of tiles, there is land rectangle data present after location and size
		else if (splitLine[1] == "TILESHEET") {
			_tileSheetTextures.insert(std::pair<std::string, sf::IntRect>
				(splitLine[0], sf::IntRect(std::stoi(splitLine[2]),
										   std::stoi(splitLine[3]),
										   std::stoi(splitLine[4]),
										   std::stoi(splitLine[5]))));

			std::vector<std::vector<int>> vertices;
			if (std::stoi(splitLine[6])) {
				for (int i = 0; i < 4; ++i) {
					std::vector<int> tempVertex;

					tempVertex.push_back(std::stoi(splitLine[7 + i*2]));
					tempVertex.push_back(std::stoi(splitLine[8 + i*2]));

					vertices.push_back(tempVertex);
				}
			}
			_tilesLandVertices.insert(std::pair<std::string, std::vector<std::vector<int>>>(splitLine[0], vertices));
		}
		else {
			sf::Texture newTexture;

			if (!std::stoi(splitLine[4]) && !std::stoi(splitLine[5]))
				newTexture.loadFromFile(splitLine[1]);
			else
				newTexture.loadFromFile(splitLine[1], sf::IntRect(std::stoi(splitLine[2]),
																  std::stoi(splitLine[3]),
																  std::stoi(splitLine[4]),
																  std::stoi(splitLine[5])));
			if (splitLine[0] == "backgroundSea")
				newTexture.setRepeated(true);

			_textures.insert(std::pair<std::string, sf::Texture>(splitLine[0], newTexture));
		}
	}
	txPathsFile.close();

	//FONT LOADING
	//////////////
	if (!_menuFont.loadFromFile("images/FontMini.ttf")) {
	}
}

void SFMLTextures::SetTexture(std::string textureName, sf::Sprite& sprite, sf::IntRect spriteSize) {
	//spriteSize is only used for sprites with a repeated texture (background and such)
	//It is disregarded for spritesheet and tilesheet texture (texture would spread across sheet, not repeat)

	std::map<std::string, sf::IntRect>::const_iterator sprSheetResult = _spriteSheetTextures.find(textureName);
	if (sprSheetResult != _spriteSheetTextures.end()) {
		sprite.setTexture(_textures["SPRITESHEET"]);
		sprite.setTextureRect(sprSheetResult->second);
		return;
	}
	std::map<std::string, sf::IntRect>::const_iterator tilSheetResult = _tileSheetTextures.find(textureName);
	if (tilSheetResult != _tileSheetTextures.end()) {
		sprite.setTexture(_textures["TILESHEET"]);
		sprite.setTextureRect(tilSheetResult->second);
		return;
	}
	std::map<std::string, sf::IntRect>::const_iterator uiSheetResult = _uiSheetTextures.find(textureName);
	if (uiSheetResult != _uiSheetTextures.end()) {

		sprite.setTexture(_textures["UISHEET"]);
		sprite.setTextureRect(uiSheetResult->second);
		return;
	}
	std::map<std::string, sf::Texture>::iterator textureResult = _textures.find(textureName);
	if (textureResult != _textures.end()) {
		sprite.setTexture(textureResult->second);

		if (spriteSize != sf::IntRect()) {
			sprite.setTextureRect(spriteSize);
		}
		return;
	}
	else
		sprite.setTexture(_textures["blank"]);
}

std::vector<std::vector<int>> SFMLTextures::GetVertices(std::string textureName)
{
	std::map<std::string, std::vector<std::vector<int>>>::iterator vertsFind = _tilesLandVertices.find(textureName);
	if (vertsFind != _tilesLandVertices.end())
		return vertsFind->second;
	else
		return std::vector<std::vector<int>>();
}

sf::Font SFMLTextures::GetFont() {
	return _menuFont;
}


