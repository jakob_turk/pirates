#include "stdafx.h"
#include "ServiceLocator.h"

IControllerProvider* ServiceLocator::_controllerProvider;
IAudioProvider* ServiceLocator::_audioProvider;
ITextureProvider* ServiceLocator::_textureProvider;
IWindowProvider* ServiceLocator::_windowProvider;

void ServiceLocator::RegisterService(IControllerProvider* provider) {

	_controllerProvider = provider;
}

void ServiceLocator::RegisterService(IAudioProvider* provider) {

	_audioProvider = provider;
}

void ServiceLocator::RegisterService(ITextureProvider* provider) {

	_textureProvider = provider;
}

void ServiceLocator::RegisterService(IWindowProvider* provider) {

	_windowProvider = provider;
}

IControllerProvider* ServiceLocator::GetController() {

	return _controllerProvider;
}

IAudioProvider* ServiceLocator::GetAudio() {

	return _audioProvider;
}

ITextureProvider* ServiceLocator::GetTextures() {

	return _textureProvider;
}

IWindowProvider* ServiceLocator::GetViewer() {

	return _windowProvider;
}