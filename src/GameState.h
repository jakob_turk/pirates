#pragma once

class GameState
{
public:
	GameState();
	~GameState();

	enum GameStateType {
		Uninitialized, Intro, Menu,
		Load, Playing, Paused, Exiting
	};

	GameStateType GetGameStateType();
	virtual void Call() = 0;

	virtual void Update(float elapsedTime) = 0;
	virtual void Draw(sf::RenderWindow& window) = 0;

protected:
	GameStateType _myGameStateType;
};