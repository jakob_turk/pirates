#include "stdafx.h"
#include "E_BoatSink.h"

#include "Game.h"

E_BoatSink::E_BoatSink(sf::Vector2f position, float angle):
	_timePassed(0.0f),
	_position(position),
	_angle(angle)
{
	_doneAnims = std::vector<bool>(5, 0);

	Load();

	//E_BoatSink is only the animation thet plays with the sinking of the ship. 
}

E_BoatSink::~E_BoatSink()
{
}

void E_BoatSink::Load() {
	Game::GetGameObjectManager().Add((GameObject*)this);

	SetLoaded();
}

void E_BoatSink::Update(float elapsedTime) {
	_timePassed += elapsedTime;

	//The explosion template here is
	//  new A_Explosion(centre of ship + rotation of specific point on it,
	//		speed of animation (0), frameLength(in sec), 3rd frame presence)
	if (!_doneAnims[0] && _timePassed > 0.0f) {
		_doneAnims[0] = true;

		AnimationFactory::Make(Anim::ShortExplosion, _position + rotated(sf::Vector2f(-10.0f, -50.0f), _angle),
							   0.0f, 10, 0.3f);

	} else if (!_doneAnims[1] && _timePassed > 0.2f) {
		_doneAnims[1] = true;

		AnimationFactory::Make(Anim::Flame, _position + rotated(sf::Vector2f(0.0f, 50.0f), _angle),
							   0.0f, 10, 0.0f, 5.0f);
		AnimationFactory::Make(Anim::ShortExplosion, _position + rotated(sf::Vector2f(0.0f, 54.0f), _angle),
							   0.0f, 10, 0.3f);

	} else if (!_doneAnims[2] && _timePassed > 0.2f) {
		_doneAnims[2] = true;

		//Crew abandons ship!
		//Random number of crew members (2 - 6)
		int nCrew = rand() % 5 + 2;
		//They spawn at y -20 - 20 and x -22 - -18 && 18 - 22 (ship coordinates)
		for (int i = 0; i < nCrew; i++) {
			int jumpSide = (rand() % 2) * 2 - 1;
			sf::Vector2f jumpPosition = sf::Vector2f((float)(jumpSide * (rand()%4 + 20)),
				(float)(rand() % 40 - 20));

			AnimationFactory::Make(Anim::Crew, _position + rotated(jumpPosition, _angle),	//spawn: boat pos + rotated random spawn 
								   0.0f, 10, 0.0f, (float)(rand() % 4 + 8),
								   rotated((float)jumpSide * sf::Vector2f(15.0f, 0.0f), _angle));		//swim direction: just away from ship
		}

		//The big explosion
		AnimationFactory::Make(Anim::Flame, _position + rotated(sf::Vector2f(0.0f, 0.0f), _angle),
							   0.0f, 10, 0.0f, 5.0f);
		AnimationFactory::Make(Anim::Flame, _position + rotated(sf::Vector2f(13.0f, -10.0f), _angle),
							   0.0f, 10, 0.0f, 5.0f);
		AnimationFactory::Make(Anim::Explosion, _position + rotated(sf::Vector2f(), _angle),
							   0.0f, 10, 0.5f);

	} else if (!_doneAnims[3] && _timePassed > 0.5f) {
		_doneAnims[3] = true;

		AnimationFactory::Make(Anim::Flame, _position + rotated(sf::Vector2f(12.0f, -40.0f), _angle),
							   0.0f, 10, 0.0f, 4.0f);
		AnimationFactory::Make(Anim::ShortExplosion, _position + rotated(sf::Vector2f(20.0f, -40.0f), _angle),
							   0.0f, 10, 0.3f);

	} else if (!_doneAnims[4] && _timePassed > 1.0f) {
		_doneAnims[4] = true;

		AnimationFactory::Make(Anim::Explosion, _position + rotated(sf::Vector2f(0.0f, -40.0f), _angle),
							   0.0f, 10, 0.2f);

		SetDeletion();
	}
}