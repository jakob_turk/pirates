#include "stdafx.h"
#include "CDbar.h"

#include "Game.h"

CDbar::CDbar(sf::Vector2f position, float length, float scaleAcross)
{
	Load(position, length, 1.0f, false, Yellow, scaleAcross);
}

void CDbar::Update(float elapsedTime)
{
	//All resizing here is done with respect to 1.0f, as GetCurrentCD returns a ratio of 1
	Boat* boatPtr = Game::GetGameObjectManager().Get("PlayerBoat");
	if (boatPtr) {
		float barLen = boatPtr->GetCannon()->GetCurrentCD();

		if (IsShowing()) {
			if (barLen > 0.0f)
				Resize(barLen);
			else
				SetShowing(false);
		}
		else if (barLen > 0.0f){
			SetShowing(true);
			Resize(barLen);
		}
	}
	else
		SetDeletion();
}
