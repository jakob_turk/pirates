#include "stdafx.h"
#include "PopUp.h"

#include "Game.h"

PopUp::PopUp(sf::Vector2f position, sf::Vector2f size, std::string text,
	std::function<void()> endFunction, int killStage):
	_killStage(0)
{
	_popUpPanel = new BackgroundPanel(position, size.x, size.y, LBrown);
	_popUpText = new Text(text, position - 
		sf::Vector2f(0, _popUpPanel->GetHeight() * 4.0f / 10.0f), 25);
	sf::Vector2f buttonPosition = position +
		sf::Vector2f(0, (size.y - _popUpPanel->GetHeight() * 2.0f) / 2.0f);
	_okButton = new Button(buttonPosition, Brown, sf::Vector2f(), new Text("OK", buttonPosition, 25));
	_okFunction = endFunction;
	_killStage = killStage;

	Game::GetGameObjectManager().Add("UI", _popUpPanel);
	Game::GetGameObjectManager().Add("UI", _popUpText);
	Game::GetGameObjectManager().Add("UI", _okButton);
}

void PopUp::Update(float elapsedTime)
{
	if (_killStage && Config::TUTORIAL_STAGE >= _killStage) {
		Kill();
		return;
	}

	sf::Vector2i mousePos = ServiceLocator::GetController()->GetMousePosition();

	if (_okButton->GetSprite().getGlobalBounds().contains((float)mousePos.x, (float)mousePos.y)) {
		if (!ServiceLocator::GetController()->IsMousePressed()) {
			if (_okButton->Hover()) {
				Kill();
			}
		}
		else {
			_okButton->Click();
		}
	}
}

void PopUp::Kill()
{
	//Call function
	_okFunction();

	//Delete pop up
	_popUpPanel->SetDeletion();
	_popUpText->SetDeletion();
	_okButton->SetDeletion();
	SetDeletion();
}
