#include "stdafx.h"
#include "Pathfinder.h"

#include "ServiceLocator.h"
#include "TileManager.h"

#include "0devDot.h"

Pathfinder::Pathfinder() :
	_searchPrecision(Config::TILE_SIZE * 3),
	_searching(false),
	_searchDone(false)
{
	//Set A* search precision - per tile is too heavy and unnecessary as boats are bigger then a tile.
}

std::vector<float> Pathfinder::FollowTargetLocation(sf::Vector2f position, sf::Vector2f targetLocation,
													sf::FloatRect boatRect)
{
	std::vector<float> speedAndAngle;

	//Thread maintenance
	if (_searching && _searchDone) {
		_aStarThread.join();
		_searchDone = false;
		_searching = false;
	}

	//If target location == st::Vector2f() or target inside boat rect, return no movement
	if (targetLocation == sf::Vector2f(0.0f, 0.0f) ||
		boatRect.contains(targetLocation))
	{
		speedAndAngle.push_back(0.0f);
		speedAndAngle.push_back(0.0f);
		return speedAndAngle;
	}

	//Check if target has changed - this would call for path recalculation
	if (targetLocation != _currentTarget) {
		//If target is land just go for direct one node path
		if (TileManager::IsLand(TileManager::GetTileAt(targetLocation))) {
			_currentTarget = targetLocation;
			_currentPath.clear();
			_currentPath.push_back(targetLocation);
		}

		//Else find A* path
		else {
			if (!_searching) {
				_searching = true;
				_currentTarget = targetLocation;
				_currentPosition = position;
				_aStarThread = std::thread(&Pathfinder::Astar, this);
			}
		}
	}

	if (_currentPath.empty()) {
		return std::vector<float>{0.0f, 0.0f};
	}

	//Pop last element of path if it has been reached, unless it is the last one
	if (std::abs(_currentPath.back().x - position.x) < _searchPrecision &&
		std::abs(_currentPath.back().y - position.y) < _searchPrecision) {
		if (_currentPath.size() > 1) {
			_currentPath.pop_back();
		}
	}

	//Return direct path to last element of the path
	return CrowFlight(position, _currentPath.back());
	//Returned value is vector<float> (speed, angle)
}

std::vector<float> Pathfinder::CrowFlight(sf::Vector2f position, sf::Vector2f target)
{
	std::vector<float> speedAndAngle;

	float newSpeed = (target.x - position.x) * (target.x - position.x) +
					 (target.y - position.y) * (target.y - position.y);

	speedAndAngle.push_back(newSpeed);
	speedAndAngle.push_back(std::atan2(target.y - position.y,
									   target.x - position.x));

	return speedAndAngle;
}

//This is always ran in a thread
void Pathfinder::Astar()
{
	//Recalibrate position and target as int vectors
	sf::Vector2i position = sf::Vector2i(_currentPosition);
	sf::Vector2i target = sf::Vector2i(_currentTarget);

	//Flag that is set to true if A* reached land on way
	bool landFreeWay = true;

	std::vector<sf::Vector2i> checkedSet;
	std::vector<sf::Vector2i> toCheck{position};

	std::map<sf::Vector2i, sf::Vector2i, sfVector2Icompare> bestConnections;
	std::map<sf::Vector2i, float, sfVector2Icompare> gScore;
	std::map<sf::Vector2i, float, sfVector2Icompare> fScore;

	gScore.insert(std::pair<sf::Vector2i, float>(position, 0.0f));
	fScore.insert(std::pair<sf::Vector2i, float>(position, AStarHeuristic(position, target)));

	while (toCheck.size()) {

		//Find lowest fScore in toCheck
		float minF = -1.0;
		sf::Vector2i current;
		std::vector<sf::Vector2i>::iterator currentItr;
		for (std::vector<sf::Vector2i>::iterator lowFitr = toCheck.begin(); lowFitr != toCheck.end(); ++lowFitr) {
			float currentFScore = fScore[*lowFitr];
			if (currentFScore < minF || minF < 0.0f) {
				minF = currentFScore;
				current = *lowFitr;
				currentItr = lowFitr;
			}
		}

		//new Dot(0, NULL, fVec(current), 3.0f, sf::Color::Green);

		//If goal reached, end
		if ((std::abs(current.x - target.x) <= _searchPrecision / 2 &&
			 std::abs(current.y - target.y) <= _searchPrecision / 2))
		{
			MakePath(bestConnections, current, landFreeWay, _currentTarget);
			_searchDone = true;
			return;
		}

		toCheck.erase(currentItr);
		checkedSet.push_back(current);

		//Ignore current, if it has land (overlap is typically (2*_searchPrecision)**2 in tiles, so wider than one A* precision)
		//Do consider it if it is the first one (meaning we are starting at the coast)
		if (TileManager::IsLand(TileManager::GetOverlapTiles(sf::FloatRect((float)(current.x - _searchPrecision),
																		   (float)(current.y - _searchPrecision),
																		   (float)2 * _searchPrecision,
																		   (float)2 * _searchPrecision))) &&
			(std::abs(current.x - position.x) > _searchPrecision / 2 ||
			 std::abs(current.y - position.y) > _searchPrecision / 2))
		{
			landFreeWay = false;
			continue;
		}

		//Check all neighbours
		for (int i = -1; i < 2; ++i) {
			for (int j = -1; j < 2; ++j) {
				if (!i && !j)
					continue;

				sf::Vector2i neighbor = current + sf::Vector2i(j*_searchPrecision, i*_searchPrecision);

				//Ignore if off-map
				if (neighbor.x < 0 ||
					neighbor.y < 0 ||
					neighbor.x > Config::MAP_WIDTH ||
					neighbor.y > Config::MAP_HEIGHT ||
				//Ignore already avaluated neighbours
					std::find(checkedSet.begin(), checkedSet.end(), neighbor) != checkedSet.end())
					continue;

				float distToCurrent = 0.0f;
				if (!i || !j)
					distToCurrent = _searchPrecision*1.0f;
				else
					distToCurrent = _searchPrecision*1.414f;
				float tentative_gScore = gScore[current] + distToCurrent;

				if (std::find(toCheck.begin(), toCheck.end(), neighbor) == toCheck.end())
					toCheck.push_back(neighbor);
				else if (tentative_gScore >= gScore[neighbor])
					continue;

				//Best connection for now
				bestConnections[neighbor] = current;
				gScore[neighbor] = tentative_gScore;
				fScore[neighbor] = tentative_gScore + AStarHeuristic(current, target);
			}
		}
	}

	_searchDone = true;
}

float Pathfinder::AStarHeuristic(sf::Vector2i position, sf::Vector2i target, Heuristic heuristic)
{
	switch (heuristic) {
	case Manhattan:
		//Manhattan distance
		return (float)(std::abs(target.x - position.x) + std::abs(target.y - position.y));
	case Euclidean:
		//Euclidean distance
		return lengthF(fVec(target - position));
	case DiagonalManhattan:
		//Manhattan distance taking into account 4 diagonal directions
		return (float)(std::min(std::abs((target - position).x), std::abs((target - position).y))*1.4142 +
					   std::max(std::abs((target - position).x), std::abs((target - position).y)) -
						std::min(std::abs((target - position).x), std::abs((target - position).y)));
	}

	//Should never happen, but return default fScore value
	return -1.0f;
}

void Pathfinder::MakePath(std::map<sf::Vector2i, sf::Vector2i, sfVector2Icompare> connections,
							sf::Vector2i end, bool forceCrowFlight, sf::Vector2f accurateTarget)
{
	_currentPath.clear();
	
	//If no land on way, just make the only node the accurate target
	if (forceCrowFlight) {
		_currentPath.push_back(accurateTarget);
		return;
	}

	_currentPath.push_back(fVec(end));
	//new Dot(0, NULL, fVec(end), 3.0f);

	std::map<sf::Vector2i, sf::Vector2i, sfVector2Icompare>::iterator currentItr = connections.find(end);
	while (currentItr != connections.end()) {
		//new Dot(0, NULL, fVec(currentItr->second), 3.0f);
		_currentPath.push_back(fVec(currentItr->second));
		currentItr = connections.find(currentItr->second);
	}
}
