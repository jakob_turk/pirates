#pragma once
#include "GameState.h"

class GS_Playing :
	public GameState
{
public:
	GS_Playing();
	~GS_Playing();

	void Call();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& window);
};