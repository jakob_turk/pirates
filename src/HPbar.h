#pragma once
#include "ValueBar.h"

class HPbar :
	public ValueBar
{
public:
	HPbar(std::string entity, sf::Vector2f position = sf::Vector2f(), float length = 100.0f,
		  bool orientation = 0, BarColour colour = Red, float scaleAcross = 1.0f);

	void Update(float elapsedTime);

private:
	std::string _followedObject;
};