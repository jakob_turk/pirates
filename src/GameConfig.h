#pragma once

class Config
{
public:
	//Game settings
	static std::string GAME_NAME;
	static int SCREEN_WIDTH;
	static int SCREEN_HEIGHT;
	static int TILE_SIZE;
	static int SCREEN_MOVE_SENSITIVITY;

	static int BOAT_HP;
	static int BOAT_SPEED;

	//Level settings
	static int MAP_WIDTH;
	static int MAP_HEIGHT;
	
	static bool TUTORIAL;
	static int TUTORIAL_STAGE;

	//Sandbox level settings
	static int SANDBOX_FACTIONS_N;
	static int SANDBOX_BOATS_N;
	static bool SANDBOX_FACTIONS_AGGRESSIVE;
};