#pragma once
#include "GameObject.h"

class ViewControll :
	public GameObject
{
public:
	void Update(float elapsedTime);

private:
	void CallMoveViewport(float elapsedTime);
	sf::Vector2f ConstrainMoveViewport(sf::Vector2f newPosition);
};