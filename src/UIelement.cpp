#include "stdafx.h"
#include "UIelement.h"

UIelement::UIelement() :
	_showing(true)
{
}

void UIelement::Update(float elapsedTime)
{
}

void UIelement::SetShowing(bool show)
{
	_showing = show;
}

bool UIelement::IsShowing()
{
	return _showing;
}