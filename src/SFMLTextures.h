#pragma once
#include "ITextureProvider.h"

class SFMLTextures :
	public ITextureProvider
{
public:
	SFMLTextures();

	void SetTexture(std::string textureName, sf::Sprite& sprite, sf::IntRect spriteSize);
	std::vector<std::vector<int>> GetVertices(std::string textureName);
	sf::Font GetFont();

private:
	std::map<std::string, sf::Texture> _textures;
	std::map<std::string, sf::IntRect> _spriteSheetTextures;
	std::map<std::string, sf::IntRect> _tileSheetTextures;
	std::map<std::string, std::vector<std::vector<int>>> _tilesLandVertices;
	std::map<std::string, sf::IntRect> _uiSheetTextures;

	sf::Font _menuFont;
};