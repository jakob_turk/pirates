#pragma once
#include "Animation.h"

class A_Static :
	public Animation
{
public:
	A_Static(sf::Vector2f position, float angle, std::string textureName, int zLevel = 5, bool noAutoAdd = false);
	~A_Static();

	void Load(std::string textureName, int zLevel, bool noAutoAdd);
	void Update(float timeDelta);
};