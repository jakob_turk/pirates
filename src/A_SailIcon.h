#pragma once
#include "Animation.h"

class A_SailIcon :
	public Animation
{
public:
	A_SailIcon(sf::Vector2f position);

	void Load();
	void Update(float elapsedTime);
private:
	sf::Vector2f _offset;
	bool _atOffset;
};