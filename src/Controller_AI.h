#pragma once
#include "Controller.h"

class Controller_AI :
	public Controller
{
public:
	enum AIstate {
		Patrol, MoveToFireRange, InFireRange, InBoardingRange, Flee
	};

	Controller_AI();

	void Control(Boat& boat);
private:
	AIstate _currentState;

	//Movement variables
	int _changeCoursePeriod;
	int _lastChangeCourse;
	int _followPeriod;
	int _lastFollow;
	bool _hasFled;

	//Fire variables
	std::string _followedBoatName;
	Boat* _followedBoat;
	int _changeCannonPeriod;
	int _lastChangeCannon;

	//Aux functions
	void SetTarget(Boat& boat, sf::Vector2f position);
	void GoToPatroling();
	void CheckPanic(Boat& boat);
};