#pragma once
#include "Animation.h"

class A_Flame :
	public Animation
{
public:
	A_Flame(sf::Vector2f position, sf::Vector2f velocity = sf::Vector2f(),
		float length = 5, bool flameType = 0, float frameLen = 0.2f, float angle = 0.0f, int zLevel = 10.0f);
	~A_Flame();

	void Load(int zLevel, float angle);
	void Update(float timeDelta);
private:
	bool _flameType;
	float _length;
	float _duration;
};