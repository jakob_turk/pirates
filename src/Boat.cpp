#include "StdAfx.h"
#include "Boat.h"

#include "Collider.h"
#include "TileManager.h"
#include "GameConfig.h"

#include "AnimationFactory.h"


Boat::Boat(std::string id, Controller* controller, BoatFaction faction) :
	_id(id),
	_faction(faction),
	_fullHP((float)Config::BOAT_HP),
	_damage(30.0f),
	_damaged1(false),
	_damaged2(false),
	_currentTravelState(Sailing),
	_completelyStranded(false),
	_deathTimer(6.0f),
	_collisionTimer(1.0f),
	_anchorTimer(1.0f),
	_rushTimer(1.3f),
	_rushing(false),
	_anchored(false),
	_preventingCollision(NULL),
	controller_(controller)
{
	Load(FactionString() + "Ship");
	assert(IsLoaded());
	
	_HP = _fullHP;
	SetStartingMaxSpeed((float)Config::BOAT_SPEED);
	SetMaxSpeed();
	SetAngularVel((float)Config::BOAT_SPEED * 0.375f);
	SetStartingFriction(2.0f);
	SetFriction(GetStartingFriction());

	pathfinder_ = new Pathfinder();
	_cannon = new Cannon();
}

Boat::~Boat()
{
	delete controller_;
	delete _cannon;
}

//Overload set vertices with a more accurate polygon
void Boat::SetVertices()
{
	_vertices.push_back(sf::Vector2i(32, 0));
	_vertices.push_back(sf::Vector2i(60, 25));
	_vertices.push_back(sf::Vector2i(64, 58));
	_vertices.push_back(sf::Vector2i(32, 112));
	_vertices.push_back(sf::Vector2i(1, 58));
	_vertices.push_back(sf::Vector2i(5, 25));
}

void Boat::SetTargetLocation(sf::Vector2f newTarget)
{
	if (_currentTravelState == Sailing || _currentTravelState == InShallows)
		_targetLocation = sf::Vector2i((int)newTarget.x, (int)newTarget.y);
}

sf::Vector2f Boat::GetTargetLocation()
{
	return sf::Vector2f((float)_targetLocation.x, (float)_targetLocation.y);
}

std::string Boat::GetID()
{
	return _id;
}

BoatFaction Boat::GetFaction()
{
	return _faction;
}

float Boat::GetDamage()
{
	return _damage * ((_HP/_fullHP) + 1.0f) / 2.0f;
}

float Boat::GetFullHP()
{
	return _fullHP;
}

float Boat::GetHP()
{
	return _HP;
}

std::string Boat::GetTextureName()
{
	if (_damaged2)
		return FactionString() + "ShipDmg2";
	else if (_damaged1)
		return FactionString() + "ShipDmg1";
	else
		return FactionString() + "Ship";
}

void Boat::GetHit(float damage)
{
	_HP -= damage;

	if (_HP < 0.0f)
		_HP = 0.0f;
}

void Boat::Anchor(float timer)
{
	if (timer) {
		_rushing = true;
		_rushTimer = 1.3f;
		_anchorTimer = 1.0f;
	}
	_anchored = true;
	SetFriction(GetStartingFriction()*80.0f);
	AnimationFactory::Make(Anim::Splash, GetPosition() + rotated(sf::Vector2f(-65, 0), inDeg(GetRotation())));
}

void Boat::Unanchor()
{
	_anchored = false;
	SetFriction(GetStartingFriction());
}

bool Boat::IsAnchored()
{
	return _anchored;
}

bool Boat::IsSailing()
{
	return _currentTravelState == Sailing || _currentTravelState == InShallows;
}

bool Boat::IsSinking()
{
	return _currentTravelState == Sinking;
}

Cannon* Boat::GetCannon()
{
	return _cannon;
}

void Boat::BoatCollision(sf::Vector2f recoil, Boat* otherBoat)
{
	if (_currentTravelState != Collided && _currentTravelState != Sinking) {
		GetHit(otherBoat->GetDamage());
		_currentTravelState = Collided;
		_collisionTimer = 1.0f;
	}


	_targetLocation = sf::Vector2i(0, 0);
	SetVelocity(1000.0f*recoil);
}

void Boat::SetPreventCollision(Boat * otherBoat)
{
	_preventingCollision = otherBoat;
}

void Boat::Sink()
{
	_currentTravelState = Sinking;
	_HP = 0.0f;

	SetMaxSpeed(0.0f);
	SetVelocity(sf::Vector2f(0, 0));
	SetTargetLocation(sf::Vector2f(0.0f, 0.0f));

	new E_BoatSink(GetPosition(), inDeg(GetRotation()) - 90.0f);
}

void Boat::Strand(sf::Vector2f reverseVector)
{
	_targetLocation = sf::Vector2i(0, 0);
	SetMaxSpeed(GetStartingMaxSpeed() * 1.0f / 4.0f);
	SetFriction(GetStartingFriction() * 3.0f);

	_completelyStranded = false;
	_strandedVector = reverseVector;

	_currentTravelState = Stranded;
}

void Boat::Shallowed()
{
	SetMaxSpeed(GetStartingMaxSpeed() * 2.0f / 3.0f);
	SetFriction(GetStartingFriction() * 2.0f);
	_currentTravelState = InShallows;
}

void Boat::Sail()
{
	SetMaxSpeed(GetStartingMaxSpeed());
	SetFriction(GetStartingFriction());
	_currentTravelState = Sailing;
}

void Boat::PreventCollision()
{
	if (!IsAnchored() && _currentTravelState != Sinking && !_rushing) {
		Anchor(1.0f);

		if (_preventingCollision)
			SetTargetLocation(GetPosition() + (GetPosition() - _preventingCollision->GetPosition()));
	}
}

void Boat::Update(float elapsedTime)
{
	//Change texture to damaged at 2/3 and 1/3 HP
	if (!_damaged1 && _HP < _fullHP * (2.0f / 3.0f)) {
		_damaged1 = true;
		LoadTexture(FactionString() + "ShipDmg1");
	}
	else if (_HP < _fullHP / 3.0f && !_damaged2) {
		_damaged2 = true;
		LoadTexture(FactionString() + "ShipDmg2");
	}


	//Boat travel states
	////////////////////

	//First check for changes in travel state (collided happens before this update, in GameObjectManager)
	// Sink
	if (_currentTravelState != Sinking && _HP <= 0.0f) {
		Sink();
	}
	// Strand/inShallows
	else if (_currentTravelState == Collided) {
		if (_collisionTimer < 0.0f)
			_currentTravelState = Sailing;
	}
	else if (_currentTravelState != Sinking){

		//Post collision prevention waiting preventing
		if (_anchorTimer) {
			_anchorTimer -= elapsedTime;
			if (_anchorTimer < 0.0f) {
				_anchorTimer = 0.0f;
				Unanchor();
			}
		}
		if (_rushing) {
			_rushTimer -= elapsedTime;
			if (_rushTimer < 0.0f)
				_rushing = false;
		}

		//Land collision
		std::vector<Tile*> tiles = TileManager::GetOverlapTiles(GetRect());
		sf::Vector2f tileCollision = Collider::BoatTileColission(this, tiles);

		if (tileCollision != sf::Vector2f()) {

			bool isJustShallows = TileManager::IsShallows(tiles);
			//inShallows
			if (_currentTravelState != InShallows && isJustShallows)
				Shallowed();
			//Strand
			else if (_currentTravelState != Stranded && !isJustShallows)
				Strand(tileCollision);
		}
		else if (_currentTravelState != Sailing)
			Sail();
	}

	//Than get controlls and handle the state we're in
	controller_->Control(*this);

	switch (_currentTravelState) {
	case Sailing:
	case InShallows:
	{
		if (_preventingCollision)
			PreventCollision();

		std::vector<float> targetPath =
			pathfinder_->FollowTargetLocation(GetPosition(), GetTargetLocation(), GetRect());

		//targetPath[0] is False if the boat is standing still
		if (targetPath[0]) {
			SetVelocity(targetPath[0], targetPath[1]);
		}

		if (targetPath[0])
			Turn(elapsedTime);
		break;
	}
	case Stranded:
		if (!(int)GetSpeed() && !_completelyStranded)
			_completelyStranded = true;

		if (_completelyStranded) {
			SetVelocity(_strandedVector*10.0f);
		}

		break;

	case Collided:
		_collisionTimer -= elapsedTime;
		break;

	case Sinking:

		if (_deathTimer < 0.0f) {
			Anim sunkenSprite = Anim::DefaultShipSunken;
			switch (_faction) {
			case fRed:
				sunkenSprite = Anim::RedShipSunken;
				break;
			case fGreen:
				sunkenSprite = Anim::GreenShipSunken;
				break;
			case fBlue:
				sunkenSprite = Anim::BlueShipSunken;
				break;
			case fYellow:
				sunkenSprite = Anim::YellowShipSunken;
				break;
			case fBlack:
				sunkenSprite = Anim::BlackShipSunken;
				break;
			}
			AnimationFactory::Make(sunkenSprite, GetPosition(), GetRotation(), 1);
			AnimationFactory::Make(Anim::Splash, GetPosition() + rotated(sf::Vector2f(12.0f, 32.0f), inDeg(GetRotation()) - 90.0f));
			AnimationFactory::Make(Anim::Splash, GetPosition() + rotated(sf::Vector2f(0.0f, -50.0f), inDeg(GetRotation()) - 90.0f));
			AnimationFactory::Make(Anim::Splash, GetPosition() + rotated(sf::Vector2f(-10.0f, 20.0f), inDeg(GetRotation()) - 90.0f));

			SetDeletion();
		}

		_deathTimer -= elapsedTime;
		break;
	}

	MovableObject::Update(elapsedTime);
	_cannon->Update(elapsedTime);

	//Maintenance?
	_preventingCollision = NULL;
}

std::string Boat::FactionString()
{
	switch (_faction) {
	case fDefault:
		return "default";
		break;
	case fRed:
		return "red";
		break;
	case fGreen:
		return "green";
		break;
	case fBlue:
		return "blue";
		break;
	case fYellow:
		return "yellow";
		break;
	case fBlack:
		return "black";
		break;
	}
	return "default";
}