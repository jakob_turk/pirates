#include "stdafx.h"
#include "BackgroundPanel.h"

BackgroundPanel::BackgroundPanel(sf::Vector2f position, float width, float height, BtnColour colour)
{
	std::string colourString;

	switch (colour) {
	case Brown:
		colourString = "brown";
		break;
	case LBrown:
		colourString = "beige";
		break;
	case Gray:
		colourString = "gray";
		break;
	case Beige:
		colourString = "white";
		break;
	}

	Load(colourString + "BtnLongP");
	SetPosition(position);
	GetSprite().setScale(width / GetWidth(), height / GetHeight());
}
