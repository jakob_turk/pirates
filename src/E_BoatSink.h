#pragma once
#include "GameObject.h"

#include "AnimationFactory.h"

class E_BoatSink :
	public GameObject
{
public:
	E_BoatSink(sf::Vector2f position, float angle);
	~E_BoatSink();

	void Load();
	void Update(float elapsedTime);
private:
	sf::Vector2f _position;
	float _angle;

	float _timePassed;
	std::vector<bool> _doneAnims;
};