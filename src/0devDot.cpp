#include "stdafx.h"
#include "0devDot.h"

#include "Game.h"

Dot::Dot(int i, VisibleObject* obj, sf::Vector2f position, float scale, sf::Color color) :
	_i(i),
	_objectFollowing(obj)
{
	if (color == sf::Color::Red)
		Load("redDot");
	else if (color == sf::Color::Green)
		Load("greenDot");
	assert(IsLoaded());

	if (position != sf::Vector2f())
		SetPosition(position);

	if (scale != 1.0f)
		SetScale(sf::Vector2f(scale, scale));

	Game::GetGameObjectManager().Add("other", this);
}

Dot::~Dot()
{
}

void Dot::Update(float elapsedTime)
{
	if (!_objectFollowing)
		return;

	sf::Transform rotation;
	rotation.rotate(inDeg(_objectFollowing->GetRotation()) - 90.0f);

	SetPosition(_objectFollowing->GetPosition() + rotation.transformPoint(fVec(_objectFollowing->GetVertices()[_i]) - _objectFollowing->GetSprite().getOrigin()));
}