#include "stdafx.h"
#include "Tile.h"

#include "ServiceLocator.h"

#include "0devDot.h"

Tile::Tile(TileType tileType, sf::Vector2f position, sf::IntRect tileSize) :
	_tileType(tileType),
	_position(position)
{
	//tile size is only used for repeated textures (backgrounds and such)
	Load(tileSize);
}

Tile::~Tile()
{
}

void Tile::Load(sf::IntRect tileSize)
{
 	ServiceLocator::GetTextures()->SetTexture(GetTextureName(_tileType), GetSprite(), tileSize);
	GetSprite().setPosition(_position);

	SetVertices();
	SetRadius();

	SetLoaded();
}

void Tile::SetVertices()
{
	//Get the land collision vertices (length of this vector might be 0, which is fine,
	//collision will handle it correctly - otherwise it's 4, a simple collision box)
	std::vector<std::vector<int>> landVertices =
		ServiceLocator::GetTextures()->GetVertices(GetTextureName(_tileType));

	int i = 0;
	for (std::vector<int> vert : landVertices) {
		_vertices.push_back(sf::Vector2i(vert[0], vert[1]));
		i++;
	}
}

void Tile::Update(float elapsedTime) {}

sf::Vector2f Tile::GetPosition() {
	return _position;
}

Tile::TileType Tile::GetTyleType() {
	return _tileType;
}

std::string Tile::GetTextureName(Tile::TileType tileType) {

	std::string tileName;

	switch (tileType) {
	case Sea:
		tileName = "sea"; break;
	case BackgroundSea:
		tileName = "backgroundSea"; break;
	case Desert1:
		tileName = "desert1"; break;
	case Desert2:
		tileName = "desert2"; break;
	case Desert3:
		tileName = "desert3"; break;
	case DesertCoastNW:
		tileName = "desertCoastNW"; break;
	case DesertCoastN:
		tileName = "desertCoastN"; break;
	case DesertCoastNE:
		tileName = "desertCoastNE"; break;
	case DesertCoastE:
		tileName = "desertCoastE"; break;
	case DesertCoastSE:
		tileName = "desertCoastSE"; break;
	case DesertCoastS:
		tileName = "desertCoastS"; break;
	case DesertCoastSW:
		tileName = "desertCoastSW"; break;
	case DesertCoastW:
		tileName = "desertCoastW"; break;
	case DesertPondSE:
		tileName = "desertPondSE"; break;
	case DesertPondSW:
		tileName = "desertPondSW"; break;
	case DesertPondNW:
		tileName = "desertPondNW"; break;
	case DesertPondNE:
		tileName = "desertPondNE"; break;
	case GrassPondSE:
		tileName = "grassPondSE"; break;
	case GrassPondSW:
		tileName = "grassPondSW"; break;
	case GrassPondNW:
		tileName = "grassPondNW"; break;
	case GrassPondNE:
		tileName = "grassPondNE"; break;
	case GrassCoastNW:
		tileName = "grassCoastNW"; break;
	case GrassCoastN1:
		tileName = "grassCoastN1"; break;
	case GrassCoastN2:
		tileName = "grassCoastN2"; break;
	case GrassCoastNE:
		tileName = "grassCoastNE"; break;
	case GrassCoastE1:
		tileName = "grassCoastE1"; break;
	case GrassCoastE2:
		tileName = "grassCoastE2"; break;
	case GrassCoastSE:
		tileName = "grassCoastSE"; break;
	case GrassCoastS1:
		tileName = "grassCoastS1"; break;
	case GrassCoastS2:
		tileName = "grassCoastS2"; break;
	case GrassCoastSW:
		tileName = "grassCoastSW"; break;
	case GrassCoastW1:
		tileName = "grassCoastW1"; break;
	case GrassCoastW2:
		tileName = "grassCoastW2"; break;
	case Grass1:
		tileName = "grass1"; break;
	case Grass2:
		tileName = "grass2"; break;
	case Grass3:
		tileName = "grass3"; break;
	case Grass4:
		tileName = "grass4"; break;
	case ShallowCoastNW:
		tileName = "shallowCoastNW"; break;
	case ShallowCoastN:
		tileName = "shallowCoastN"; break;
	case ShallowCoastNE:
		tileName = "shallowCoastNE"; break;
	case ShallowCoastE:
		tileName = "shallowCoastE"; break;
	case ShallowCoastSE:
		tileName = "shallowCoastSE"; break;
	case ShallowCoastS:
		tileName = "shallowCoastS"; break;
	case ShallowCoastSW:
		tileName = "shallowCoastSW"; break;
	case ShallowCoastW:
		tileName = "shallowCoastW"; break;
	case ShallowIsleSE:
		tileName = "shallowIsleSE"; break;
	case ShallowIsleSW:
		tileName = "shallowIsleSW"; break;
	case ShallowIsleNW:
		tileName = "shallowIsleNW"; break;
	case ShallowIsleNE:
		tileName = "shallowIsleNE"; break;
	case Special1:
		tileName = "special1"; break;
	case Special2:
		tileName = "special2"; break;
	case Special3:
		tileName = "special3"; break;
	case Special4:
		tileName = "special4"; break;
	case Special5:
		tileName = "special5"; break;
	case Special6:
		tileName = "special6"; break;
	}

	return tileName;
}