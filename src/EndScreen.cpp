#include "stdafx.h"
#include "EndScreen.h"

#include "Game.h"

#include "BackgroundPanel.h"
#include "A_Static.h"

EndScreen::EndScreen(bool won) :
	_replayButton(NULL),
	_menuButton(NULL)
{
	Game::GetGameObjectManager().Add("UI", new BackgroundPanel(
		sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 9.0f / 20.0f),
		Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 7.0f / 10.0f, LBrown));

	if (won) {
		Game::GetGameObjectManager().Add("UI", new Text("Ye be winnin, YARR!!!!",
			sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 9.0f / 40.0f), 40));

		A_Static* boatSprite = new A_Static(
			sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 9.0f / 20.0f), 0.0f,
			Game::GetGameObjectManager().Get("PlayerBoat")->GetTextureName(), 11, true);
		boatSprite->SetScale(2.1f, 2.1f);
		Game::GetGameObjectManager().Add("UI", boatSprite);
	}
	else {
		Game::GetGameObjectManager().Add("UI", new Text("Aryy, ye lost, matey...",
			sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 9.0f / 40.0f), 40));

		//In case of loosing, the faction with most remaining boats is displayed (in case of multiple
		//tied, random one of those).
		//So first make statistics of remaining boats
		std::map<BoatFaction, int> factionStats;
		for (auto enemyBoat : Game::GetGameObjectManager().GetBoats()) {
			if (factionStats.find(enemyBoat.second->GetFaction()) != factionStats.end())
				factionStats[enemyBoat.second->GetFaction()]++;
			else
				factionStats.insert(std::pair<BoatFaction, int>(enemyBoat.second->GetFaction(), 1));
		}

		//Find winning faction
		BoatFaction winningFaction = fDefault;
		int mostBoats = 0;
		for (auto faction : factionStats) {
			if (faction.second > mostBoats) {
				mostBoats = faction.second;
				winningFaction = faction.first;
			}
		}

		//Traverse boats again and make a sprite of the first boat from winning faction encountered
		for (auto enemyBoat : Game::GetGameObjectManager().GetBoats()) {
			if (enemyBoat.second->GetFaction() == winningFaction) {
				A_Static* boatSprite = new A_Static(
					sf::Vector2f(Config::SCREEN_WIDTH / 2.0f, Config::SCREEN_HEIGHT * 9.0f / 20.0f), 0.0f,
					enemyBoat.second->GetTextureName(), 11, true);
				boatSprite->SetScale(2.1f, 2.1f);
				Game::GetGameObjectManager().Add("UI", boatSprite);
				break;
			}
		}
	}
	
	_menuButton = new Button(sf::Vector2f(Config::SCREEN_WIDTH * 15.0f / 40.0f, Config::SCREEN_HEIGHT * 27.0f / 40.0f),
							 Brown, sf::Vector2f(),
					new Text("Menu", sf::Vector2f(Config::SCREEN_WIDTH * 15.0f / 40.0f, Config::SCREEN_HEIGHT * 27.0f / 40.0f),
							 20));
	Game::GetGameObjectManager().Add("UI", _menuButton);

	if (!Config::TUTORIAL_STAGE) {
		_replayButton = new Button(sf::Vector2f(Config::SCREEN_WIDTH * 25.0f / 40.0f, Config::SCREEN_HEIGHT * 27.0f / 40.0f),
			Brown, sf::Vector2f(),
			new Text("Play Again", sf::Vector2f(Config::SCREEN_WIDTH * 25.0f / 40.0f, Config::SCREEN_HEIGHT * 27.0f / 40.0f),
				20));
		Game::GetGameObjectManager().Add("UI", _replayButton);
	}
}

void EndScreen::Update(float elapsedTime)
{
	sf::Vector2i mousePos = ServiceLocator::GetController()->GetMousePosition();

	if (_menuButton->GetSprite().getGlobalBounds().contains((float)mousePos.x, (float)mousePos.y)) {
		if (!ServiceLocator::GetController()->IsMousePressed()) {
			if (_menuButton->Hover()) {
				Game::ChangeGameState(GameState::Menu);
			}
		}
		else {
			_menuButton->Click();
		}
	}
	else if (_replayButton && _replayButton->GetSprite().getGlobalBounds().contains((float)mousePos.x, (float)mousePos.y)) {
		if (!ServiceLocator::GetController()->IsMousePressed()) {
			if (_replayButton->Hover()) {
				Game::ChangeGameState(GameState::Load);
			}
		}
		else {
			_replayButton->Click();
		}
	}
}
