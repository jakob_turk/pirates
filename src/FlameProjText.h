#pragma once
#include "Text.h"

class FlameProjText :
	public Text
{
public:
	FlameProjText(sf::Vector2f position, int textSize = 33, sf::Color textColor = sf::Color::Black);

	void Update(float elapsedTime);
};