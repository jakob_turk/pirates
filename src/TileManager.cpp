#include "stdafx.h"
#include "TileManager.h"

#include "Game.h"

//Static member initializations
std::vector<std::vector<Tile*>> TileManager::_tileMap;
Tile* TileManager::_seaTile;
//_TILEDICT and _SHALLOW_TILES at the end of this file

TileManager::TileManager()
{
}

TileManager::~TileManager()
{
}

void TileManager::Load(std::vector<std::vector<std::string>> textMap) {

	//Whole fucntion does nothing if textMap.size() is 0, so check for it and don't worry later...
	if (!textMap.size())
		return;

	//Reset _tileMap
	Empty();

	//SEA BACKGROUND
	////////////////
	//Simply load the sea background Tile (the textMap[0] fails if textMap.size() is 0)
	//LeadBackground also creates a map of empty tiles (nones)
	LoadBackground(textMap[0].size() * Config::TILE_SIZE, textMap.size() * Config::TILE_SIZE);
	
	//TILES
	///////
	//Walk over the textMap and fill _tileMap with Tiles
	for (std::vector<std::vector<std::string>>::iterator i = textMap.begin(); i != textMap.end(); ++i) {

		for (std::vector<std::string>::iterator j = i->begin(); j != i->end(); ++j) {

			int intI = i - textMap.begin();
			int intJ = j - i->begin();
			Tile::TileType tileType = GetTileName(*j);

			if (tileType != Tile::Sea) {
				_tileMap[intI][intJ] = new Tile(tileType,
									   sf::Vector2f((float)((j - i->begin()) * Config::TILE_SIZE),
													(float)(intI			 * Config::TILE_SIZE)));
			}
		}
	}

	// So static class constructors don't really get called? populate 
}

void TileManager::LoadBackground(int x, int y)
{
	_seaTile = new Tile(Tile::BackgroundSea, sf::Vector2f(0.0f, 0.0f),
		sf::IntRect(0, 0, x, y));

	Config::MAP_WIDTH = x;
	Config::MAP_HEIGHT = y;

	for (unsigned int fillI = 0; fillI < Config::MAP_HEIGHT * 1.0f / Config::TILE_SIZE * 1.0f; ++fillI) {
		_tileMap.push_back(std::vector<Tile*>());

		for (unsigned int fillJ = 0; fillJ < Config::MAP_WIDTH * 1.0f / Config::TILE_SIZE * 1.0f; ++fillJ)
			_tileMap[fillI].push_back(NULL);
	}
}

Tile * TileManager::GetTileAt(sf::Vector2f position)
{
	return nullptr;
}

std::vector<Tile*> TileManager::GetOverlapTiles(sf::FloatRect objectRectFloat)
{
	std::vector<Tile*> overlapTiles = std::vector<Tile*>();

	// Turn the floatRect into intRect, as with tiles we're dealing in ints. Also clip it to level map
	sf::IntRect objectRect = sf::IntRect(objectRectFloat);
	if (objectRect.top < 0) {
		objectRect.height = objectRect.height + objectRect.top;
		objectRect.top = 0;
	}
	else if (objectRect.top + objectRect.height > Config::MAP_HEIGHT) {
		objectRect.height = Config::MAP_HEIGHT - objectRect.top;
	}
	if (objectRect.left < 0) {
		objectRect.width = objectRect.width + objectRect.left;
		objectRect.left = 0;
	}
	else if (objectRect.left + objectRect.width > Config::MAP_WIDTH) {
		objectRect.width = Config::MAP_WIDTH - objectRect.left;
	}
	//If the whole object is off map, width or height have above been set below zero
	if (objectRect.width < 0 || objectRect.height < 0)
		return overlapTiles;

	
	// Loop over tiles starting top left, line by line, bounded by width and height of the rect
	int tileY = objectRect.top;
	while (tileY < objectRect.top + objectRect.height) {
		
		int tileX = objectRect.left;
		while (tileX < objectRect.left + objectRect.width) {
			
			//Only add tile to return vect, if not null (not sea)
			Tile* tileAtPos = _tileMap[tileY / Config::TILE_SIZE][tileX / Config::TILE_SIZE];
			if (tileAtPos)
				overlapTiles.push_back(tileAtPos);
			
			tileX = tileX + Config::TILE_SIZE;
		}

		tileY = tileY + Config::TILE_SIZE;
	}

	return overlapTiles;
}

bool TileManager::IsLand(std::vector<Tile*> tiles)
{
	//This returns true if any tile in tiles is land

	for (Tile* tile : tiles) {
		//return true at first non-shallow tile
		if (IsLand(tile))
			return true;
	}

	return false;
}

bool TileManager::IsLand(Tile * tile)
{
	if (tile == NULL)
		return false;

	if (std::find(_SHALLOW_TILES.begin(), _SHALLOW_TILES.end(), tile->GetTyleType()) != _SHALLOW_TILES.end())
		return false;
	
	return true;
}

bool TileManager::IsShallows(std::vector<Tile*> tiles)
{
	if (!tiles.size())
		return false;

	for (Tile* tile : tiles) {
		if (std::find(_SHALLOW_TILES.begin(), _SHALLOW_TILES.end(), tile->GetTyleType()) == _SHALLOW_TILES.end())
			return false;
	}

	return true;
}

void TileManager::DrawAll(sf::RenderWindow& renderWindow)
{
	//Draw the sea tile first
	_seaTile->Draw(renderWindow);

	//Then cover the whole tile map (transparent sprites are not make on open sea - the NULLS)
	for (std::vector<std::vector<Tile*>>::iterator i = _tileMap.begin(); i != _tileMap.end(); ++i) {
		for (std::vector<Tile*>::iterator j = i->begin(); j != i->end(); ++j) {
			if (*j != NULL)
				(*j)->Draw(renderWindow);
		}
	}
}

void TileManager::Empty()
{
	if (_seaTile) {
		delete _seaTile;
		_seaTile = NULL;
	}

	for (std::vector<std::vector<Tile*>>::iterator i = _tileMap.begin(); i != _tileMap.end(); ++i) {
		for (std::vector<Tile*>::iterator j = i->begin(); j != i->end(); ++j) {
			delete *j;
		}
	}
	_tileMap.clear();
}

Tile::TileType TileManager::GetTileName(std::string tag)
{
	std::map<std::string, Tile::TileType>::const_iterator searchItr = _TILEDICT.find(tag);
	if (searchItr == _TILEDICT.end())
		return _TILEDICT["s"];
	else
		return searchItr->second;
}

//The _TILEDICT map of map tags to TileTypes
//The first 
std::map<std::string, Tile::TileType> TileManager::_TILEDICT = {
	{"d1", Tile::Desert1},
	{"d2", Tile::Desert2},
	{"d3", Tile::Desert3},
	{"g1", Tile::Grass1},
	{"g2", Tile::Grass2},
	{"g3", Tile::Grass3},
	{"g4", Tile::Grass4},
	{"s", Tile::Sea},
	{"dcNW", Tile::DesertCoastNW},
	{"dcN",  Tile::DesertCoastN},
	{"dcNE", Tile::DesertCoastNE},
	{"dcE",  Tile::DesertCoastE},
	{"dcSE", Tile::DesertCoastSE},
	{"dcS",  Tile::DesertCoastS},
	{"dcSW", Tile::DesertCoastSW},
	{"dcW",  Tile::DesertCoastW},
	{"dpSE", Tile::DesertPondSE},
	{"dpSW", Tile::DesertPondSW},
	{"dpNW", Tile::DesertPondNW},
	{"dpNE", Tile::DesertPondNE},
	{"gpSE", Tile::GrassPondSE},
	{"gpSW", Tile::GrassPondSW},
	{"gpNW", Tile::GrassPondNW},
	{"gpNE", Tile::GrassPondNE},
	{"gcNW", Tile::GrassCoastNW},
	{"gcN1", Tile::GrassCoastN1},
	{"gcN2", Tile::GrassCoastN2},
	{"gcNE", Tile::GrassCoastNE},
	{"gcE1", Tile::GrassCoastE1},
	{"gcE2", Tile::GrassCoastE2},
	{"gcSE", Tile::GrassCoastSE},
	{"gcS1", Tile::GrassCoastS1},
	{"gcS2", Tile::GrassCoastS2},
	{"gcSW", Tile::GrassCoastSW},
	{"gcW1", Tile::GrassCoastW1},
	{"gcW2", Tile::GrassCoastW2},
	{"scNW", Tile::ShallowCoastNW},
	{"scN",  Tile::ShallowCoastN},
	{"scNE", Tile::ShallowCoastNE},
	{"scE",  Tile::ShallowCoastE},
	{"scSE", Tile::ShallowCoastSE},
	{"scS",  Tile::ShallowCoastS},
	{"scSW", Tile::ShallowCoastSW},
	{"scW",  Tile::ShallowCoastW},
	{"siSE", Tile::ShallowIsleSE},
	{"siSW", Tile::ShallowIsleSW},
	{"siNW", Tile::ShallowIsleNW},
	{"siNE", Tile::ShallowIsleNE},
	{"x1", Tile::Special1},
	{"x2", Tile::Special2},
	{"x3", Tile::Special3},
	{"x4", Tile::Special4},
	{"x5", Tile::Special5},
	{"x6", Tile::Special6}
};

std::vector<Tile::TileType> TileManager::_SHALLOW_TILES{
	Tile::ShallowCoastNW,
	Tile::ShallowCoastN,
	Tile::ShallowCoastNE,
	Tile::ShallowCoastE,
	Tile::ShallowCoastSE,
	Tile::ShallowCoastS,
	Tile::ShallowCoastSW,
	Tile::ShallowCoastW,
	Tile::ShallowIsleSE,
	Tile::ShallowIsleSW,
	Tile::ShallowIsleNW,
	Tile::ShallowIsleNE,
	Tile::Sea
};