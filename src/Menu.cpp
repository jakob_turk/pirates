#include "stdafx.h"
#include "Menu.h"

#include "Game.h"

Menu::Menu():
	_menuSheet(NULL)
{}

Menu::~Menu()
{
	for (auto menuButton : _buttons)
		delete menuButton.second;

	_buttons.clear();
}

void Menu::Call()
{
}

void Menu::Update(float elapsedTime)
{
	Game::GetGameObjectManager().UpdateAll(elapsedTime);

	sf::Vector2i mousePos = ServiceLocator::GetController()->GetMousePosition();

	for (auto menuButton : _buttons) {
		if (menuButton.second->GetSprite().getGlobalBounds().contains((float)mousePos.x, (float)mousePos.y)) {
			if (!ServiceLocator::GetController()->IsMousePressed()) {
				if (menuButton.second->Hover()) {
					DealWithButtonClick(menuButton.first);
				}
			}
			else {
				menuButton.second->Click();
			}
		}
	}
}

void Menu::Draw(sf::RenderWindow& window)
{
	Game::GetGameObjectManager().DrawAll(window);

	if (_menuSheet)
		_menuSheet->Draw(window);

	for (auto menuButton : _buttons) {
		menuButton.second->Draw(window);
	}

	for (auto menuText : _texts) {
		menuText->Draw(window);
	}
}
