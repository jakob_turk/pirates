#include "stdafx.h"
#include "BoatFactory.h"

#include "Game.h"

#include "Controller_AI.h"
#include "Controller_Player.h"

int BoatFactory::_genericId = 0;

Boat* BoatFactory::Make(sf::Vector2f position, BoatController controller, BoatFaction faction,
						float angle, sf::Vector2f velocity, std::string name)
{
	Controller* newController;

	if (name == "") {
		name = "Boat_" + std::to_string(_genericId);
		++_genericId;
	}

	switch (controller) {

	case AI:
		newController = new Controller_AI();
		break;
	case Player:
		newController = new Controller_Player();
		break;
	}

	Boat* newBoat = new Boat(name, newController, faction);
	newBoat->SetPosition(position);
	newBoat->SetRotation(angle);
	Game::GetGameObjectManager().Add(name, newBoat);

	return newBoat;
}