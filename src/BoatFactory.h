#pragma once
#include "Boat.h"

enum BoatController {
	AI, Player
};

class BoatFactory
{
public:
	static Boat* Make(sf::Vector2f position = sf::Vector2f(),
					  BoatController controller = AI,
					  BoatFaction faction = fDefault,
					  float angle = 0.0f,
					  sf::Vector2f velocity = sf::Vector2f(),
					  std::string name = "");

private:
	static int _genericId;
};