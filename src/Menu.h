#pragma once

#include "BackgroundSheet.h"
#include "Button.h"
#include "Text.h"

class Menu
{
public:
	Menu();
	~Menu();

	enum MenuType {
		Main, Settings, Level, Sandbox
	};

	//This contains all the manu buttons - which is rather clunky
	enum ButtonType {
		Play, SettingsButton, Quit, Back, Tutorial,
		nFactions1, nFactions2, nFactions3,
		nBoats1, nBoats2, nBoats3,
		HP1, HP2, HP3,
		speed1, speed2, speed3,
		factionAggression
	};

	virtual void Call();

	virtual void Update(float elapsedTime);
	virtual void Draw(sf::RenderWindow& window);

protected:
	virtual void MakeButtons() = 0;
	virtual void DealWithButtonClick(ButtonType button) = 0;

	BackgroundSheet* _menuSheet;
	std::map<ButtonType, Button*> _buttons;
	std::vector<Text*> _texts;
};