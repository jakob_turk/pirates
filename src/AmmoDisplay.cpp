#include "stdafx.h"
#include "AmmoDisplay.h"

#include "Game.h"

AmmoDisplay::AmmoDisplay(sf::Vector2f position, Projectile::ProjectileType ammoType) :
	_nProjectiles(0),
	_position(position),
	_ammoType(ammoType)
{
	Reload(Game::GetGameObjectManager().Get("PlayerBoat")->GetCannon()->GetAmmo(_ammoType));

	SetLoaded();
}

void AmmoDisplay::Reload(int nNewProjectiles)
{
	int i = 0;
	while (i < nNewProjectiles) {

		//Spacing between projectile images - depends on ammo type
		float ammoSpacing = 0.0f;
		if (_ammoType == Projectile::NormalProj)
			ammoSpacing = Config::SCREEN_WIDTH / 30.0f;
		else if (_ammoType == Projectile::ExplosiveProj)
			ammoSpacing = Config::SCREEN_WIDTH / 25.0f;

		//Add extra explosion sprite in case of explosive ammo
		if (_ammoType == Projectile::ExplosiveProj) {
			A_Static* newProjX = new A_Static(_position + sf::Vector2f((_nProjectiles + i)*ammoSpacing, 0.0f),
											  0.0f, "explosion2", 11, true);
			newProjX->SetScale(0.9f, 0.9f);
			Game::GetGameObjectManager().Add("UI", newProjX);
			_projectiles.push_back(newProjX);
		}

		//Add the projectile sprite
		A_Static* newProj = new A_Static(_position + sf::Vector2f((_nProjectiles+i)*ammoSpacing, 0.0f),
										 0.0f, "projectile", 11, true);
		newProj->SetScale(2.6f, 2.6f);
		Game::GetGameObjectManager().Add("UI", newProj);
		_projectiles.push_back(newProj);
		i++;
	}

	_nProjectiles += nNewProjectiles;
}

void AmmoDisplay::Update(float elapsedTIme)
{
	Boat* playerPtr = Game::GetGameObjectManager().Get("PlayerBoat");
	if (playerPtr) {
		if (playerPtr->GetCannon()->GetAmmo(_ammoType) < _nProjectiles) {
			_nProjectiles--;
			//Remove the projectile sprite
			_projectiles.back()->SetDeletion();
			_projectiles.pop_back();

			//Remove the extra explosion sprite in case of explosive ammo
			if (_ammoType == Projectile::ExplosiveProj) {
				_projectiles.back()->SetDeletion();
				_projectiles.pop_back();
			}
		}
		else if (playerPtr->GetCannon()->GetAmmo(_ammoType) > _nProjectiles) {
			Reload(playerPtr->GetCannon()->GetAmmo(_ammoType) - _nProjectiles);
		}
	}
}
