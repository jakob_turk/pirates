#include "stdafx.h"
#include "RadioButton.h"

RadioButton::RadioButton(sf::Vector2f position, sf::Vector2f size) :
	Button(position, Brown, size)
{
	LoadButtonColor(Brown);
}

void RadioButton::LoadButtonColor(BtnColour color)
{
	switch (color) {
	case Brown:
		_releaseTexture = "brownRadio";
		_hoverTexture = "brownRadio";
		_clickTexture = "brownRadio";
		break;
	case LBrown:
		_releaseTexture = "brownRadioP";
		_hoverTexture = "brownRadioP";
		_clickTexture = "brownRadioP";
		break;
	}

	Load(_releaseTexture);
}
