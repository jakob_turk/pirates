#include "stdafx.h"
#include "A_Crew.h"

A_Crew::A_Crew(sf::Vector2f position, sf::Vector2f velocity, float length, int crewMember, float frameLen) :
	_length(length),
	_duration(0.0f)
{
	_frameLen = frameLen;
	SetPosition(position);
	SetVelocity(velocity);

	if (crewMember == 0)
		_crewMember = rand() % 6 + 1;
	else if (crewMember > 6)
		_crewMember = 6;
	else
		_crewMember = crewMember;

	Load();
}

A_Crew::~A_Crew()
{
}

void A_Crew::Load()
{
	LoadFrameSprite("crew" + std::to_string(_crewMember),		GetPosition());
	LoadFrameSprite("crew" + std::to_string(_crewMember) + "f", GetPosition());

	Animation::Load(3);
}

void A_Crew::Update(float timeDelta)
{
	for (std::vector<sf::Sprite>::iterator fItr = _frames.begin(); fItr != _frames.end(); fItr++) {
		fItr->move(GetVelocity() * timeDelta);
	}

	_sinceFrameUpdate += timeDelta;
	_duration += timeDelta;

	if (_duration > _length) SetDeletion();
	else if (_sinceFrameUpdate > _frameLen) {
		_sinceFrameUpdate = 0;
		NextFrame(true);
	}
}