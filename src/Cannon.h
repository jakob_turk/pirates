#pragma once

#include "Projectile.h"

class Cannon
{
public:
	Cannon();

	void SetCannonType(Projectile::ProjectileType type);
	Projectile::ProjectileType GetCannonType();

	void AddAmmo(Projectile::ProjectileType typeToAdd, int amountToAdd);
	int GetAmmo(Projectile::ProjectileType type);
	float GetCurrentCD();

	//Shoot
	//position is position of cannon, target is target location - projectile angle and velocity
	//are calculated from this. Accuracy equals degree of randomness (in degrees of the off shot
	//cone) where 0.0 is perfect accuracy. Owner is the boat that shot, so as not to shoot itself.
	void Shoot(sf::Vector2f position, sf::Vector2f target, BoatFaction ownerFaction, int accuracy = 0.0f);
	void Update(float elapsedTime);

private:
	Projectile::ProjectileType _cannonType;

	//<Ammo, CD>
	std::map<Projectile::ProjectileType, std::pair<int, float>> _ammo;
};