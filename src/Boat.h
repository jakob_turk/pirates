#pragma once
#include "MovableObject.h"

#include "Controller.h"
#include "Pathfinder.h"
#include "Cannon.h"
#include "E_BoatSink.h"

class Boat :
	public MovableObject
{
public:
	Boat(std::string id, Controller* controller, BoatFaction faction = fDefault);
	~Boat();

	void Update(float elapsedTime);

	void BoatCollision(sf::Vector2f recoil, Boat* otherBoat);
	void SetPreventCollision(Boat* otherBoat);
	void GetHit(float damage);

	void Anchor(float timer = 0.0f);
	void Unanchor();
	bool IsAnchored();
	bool IsSailing();
	bool IsSinking();
	Cannon* GetCannon();

	std::string GetID();
	BoatFaction GetFaction();
	float GetDamage();
	float GetFullHP();
	float GetHP();
	virtual std::string GetTextureName();

	//Must be public for componenets:
	sf::Vector2f GetTargetLocation();
	void		 SetTargetLocation(sf::Vector2f newTarget);

private:
	//init
	void SetVertices();

	//State functions
	void Sink();
	void Strand(sf::Vector2f reverseVector);
	void Shallowed();
	void Sail();
	void PreventCollision();

	//Components
	Controller* controller_;
	Pathfinder* pathfinder_;

	Cannon* _cannon;

	//Boat data
	std::string _id;
	BoatFaction _faction;
	float _fullHP;
	float _HP;
	float _damage; //Damage it exerts on the other ship on collision
	sf::Vector2i _targetLocation;

	//Damaged textures
	bool _damaged1;
	bool _damaged2;

	//Travel states
	enum BoatTravelState {
		Sailing, InShallows, Stranded, Collided, Sinking
	};
	BoatTravelState _currentTravelState;

	sf::Vector2f _strandedVector;
	bool _completelyStranded;
	bool _rushing;
	float _rushTimer;
	float _anchorTimer;
	Boat* _preventingCollision;
	float _collisionTimer;
	float _deathTimer;

	//Ability states
	bool _anchored;

	//Utility funcs
	std::string FactionString();
};