#pragma once
#include "UIelement.h"

#include "Game.h"
#include "Animation.h"

enum StatusIcon {
	Idle, Sailing, Anchored, Sinking
};

class StatusDisplay :
	public UIelement
{
public:

	StatusDisplay(sf::Vector2f position);

	void Draw(sf::RenderWindow & renderWindow);
	void Update(float elapsedTime);

private:
	StatusIcon _currentStatus;
	std::map<StatusIcon, Animation*> _icons;
};