#include "stdafx.h"
#include "SFMLWindow.h"

SFMLWindow::SFMLWindow() :
	_zoom(1.0f)
{
	_window.create(sf::VideoMode::getFullscreenModes()[0], Config::GAME_NAME, sf::Style::Fullscreen);
	_viewPort = _window.getDefaultView();
	Config::SCREEN_WIDTH = _window.getSize().x;
	Config::SCREEN_HEIGHT = _window.getSize().y;
	_minimapView.reset(sf::FloatRect(0.0f, 0.0f, 0.0f, 0.0f));
}

sf::RenderWindow& SFMLWindow::GetWindow()
{
	return _window;
}

sf::View& SFMLWindow::GetViewport()
{
	return _viewPort;
}

sf::View & SFMLWindow::GetMinimapViewPort()
{
	return _minimapView;
}

void SFMLWindow::SetUpMinimapViewPort(float width, float height)
{
	_minimapView.reset(sf::FloatRect(0.0f, 0.0f, (float)Config::MAP_WIDTH, (float)Config::MAP_HEIGHT));
	_minimapView.setViewport(sf::FloatRect(width*0.05f, 1 - height + 0.05f*height, width*0.9f, height*0.9f));
}

void SFMLWindow::MoveViewport(sf::Vector2f moveVector, float factor)
{
	_viewPort.move(moveVector * factor);
	_window.setView(_viewPort);
}

void SFMLWindow::MoveViewportTo(sf::Vector2f newPosition)
{
	_viewPort.setCenter(newPosition);
	_window.setView(_viewPort);
}

void SFMLWindow::SetViewport(Viewport newViewport)
{
	switch (newViewport) {
	case Default:
		_window.setView(_window.getDefaultView());
		break;
	case Game:
		_window.setView(_viewPort);
		break;
	case Minimap:
		_window.setView(_minimapView);
		break;
	}
}

void SFMLWindow::ResetZoom()
{
	_zoom = 1.0f;
	_viewPort.setSize((float)Config::SCREEN_WIDTH, (float)Config::SCREEN_HEIGHT);
}

void SFMLWindow::ZoomViewport(float factor)
{
	_zoom *= factor; 

	//Now this is kinda not correct zoom is actually close to limit, but not exactly there, and you kinda just leave it there...
	//However, without accessing _viewPort twice (reset + zoom for perfect precision) this would command a ton of
	//floating point mess without achieveing a relevant gameplay effect.
	if (_zoom > 2.0f)
		_zoom = 2.0f;
	else if (_zoom < 0.2f)
		_zoom = 0.2f;
	else {
		_viewPort.zoom(factor);
		_window.setView(_viewPort);
	}
}

float SFMLWindow::GetZoom()
{
	return _zoom;
}
