#pragma once
#include "ValueBar.h"

class CDbar :
	public ValueBar
{
public:
	CDbar(sf::Vector2f position = sf::Vector2f(), float length = 100.0f, float scaleAcross = 1.0f);

	void Update(float elapsedTime);
};