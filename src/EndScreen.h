#pragma once
#include "GameObject.h"

#include "Button.h"

class EndScreen :
	public GameObject
{
public:
	EndScreen(bool won);

	void Update(float elapsedTime);
private:
	Button* _menuButton;
	Button* _replayButton;
};