#pragma once
#include "GameObject.h"

#include "Projectile.h"
#include "A_Static.h"

class AmmoDisplay :
	public GameObject
{
public:
	AmmoDisplay(sf::Vector2f position, Projectile::ProjectileType ammoType);

	void Update(float elapsedTIme);
private:
	void Reload(int nNewProjectiles);

	sf::Vector2f _position;
	Projectile::ProjectileType _ammoType;

	int _nProjectiles;
	std::vector<A_Static*> _projectiles;
};