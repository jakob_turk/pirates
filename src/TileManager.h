#pragma once
#include "Tile.h"

class TileManager
{
public:
	TileManager();
	~TileManager();

	static void Load(std::vector<std::vector<std::string>> map);
	static void DrawAll(sf::RenderWindow& renderWindow);

	static void Empty();

	static void LoadBackground(int x, int y);

	static Tile* GetTileAt(sf::Vector2f position);
	static std::vector<Tile*> GetOverlapTiles(sf::FloatRect objectRectFloat);
	static bool IsLand(std::vector<Tile*> tiles);
	static bool IsLand(Tile* tile);
	static bool IsShallows(std::vector<Tile*> tiles);

private:
	static Tile::TileType GetTileName(std::string tag);

	static Tile* _seaTile;
	static std::vector<std::vector<Tile*>> _tileMap;

	static std::map<std::string, Tile::TileType> _TILEDICT;
	static std::vector<Tile::TileType> _SHALLOW_TILES;
};