#include "stdafx.h"
#include "GameState_Playing.h"

#include "Game.h"

GS_Playing::GS_Playing() {
	_myGameStateType = Playing;
}

GS_Playing::~GS_Playing() {
}

void GS_Playing::Call() {
}

void GS_Playing::Update(float elapsedTime) {
	Game::GetGameObjectManager().UpdateAll(elapsedTime);
}

void GS_Playing::Draw(sf::RenderWindow& window) {
	Game::GetGameObjectManager().DrawAll(window);
}