#pragma once
#include "Animation.h"

class A_Explosion :
	public Animation
{
public:
	A_Explosion(sf::Vector2f position, sf::Vector2f velocity = sf::Vector2f(),
		float frameLen = 0.2f, bool thirdFrame = true);

	void Load();
	void Update(float timeDelta);
private:
	bool _thirdFrame;
};