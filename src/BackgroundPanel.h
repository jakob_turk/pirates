#pragma once
#include "UIelement.h"

class BackgroundPanel :
	public UIelement
{
public:
	BackgroundPanel(sf::Vector2f position, float width, float height, BtnColour colour = Brown);
};