#pragma once
#include "VisibleObject.h"

class Animation :
	public VisibleObject
{
public:
	Animation(sf::Vector2f position = sf::Vector2f(), sf::Vector2f velocity = sf::Vector2f());
	virtual ~Animation();

	virtual void Load(int zLevel = 10, bool noAutoAdd = false);
	virtual void Draw(sf::RenderWindow& window);
	virtual void Update(float timeDelta) = 0;
	void LoadFrameSprite(std::string textureName, sf::Vector2f position, float angle = 0.0f);

	void SetScale(float xScale, float yScale);
	void SetRotation(float angle);
	void SetAngle(float angle);
	void SetPosition(sf::Vector2f position);
	void SetVelocity(sf::Vector2f veclocity);
	float GetAngle();
	sf::Vector2f GetPosition();
	sf::Vector2f GetVelocity();

protected:
	void NextFrame(bool cicle = false);

	std::vector<sf::Sprite> _frames;
	float _frameLen;
	float _sinceFrameUpdate;

private:
	float _angle;
	sf::Vector2f _position;
	sf::Vector2f _velocity;

	unsigned int _currentFrame;
};