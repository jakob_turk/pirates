#include "stdafx.h"
#include "GameObject.h"

GameObject::GameObject() :
	_isLoaded(false),
	_wantsDeletion(false)
{}

GameObject::~GameObject() {}

bool GameObject::IsLoaded() const {
	return _isLoaded;
}

bool GameObject::WantsDeletion() const {
	return _wantsDeletion;
}

void GameObject::SetLoaded() {
	_isLoaded = true;
}

void GameObject::SetDeletion() {
	_wantsDeletion = true;
}