#pragma once
#include "GameObject.h"

class Tutorial :
	public GameObject
{
public:
	Tutorial();

	void Update(float ElapsedTime);
private:
	int _tutorialStage;
};