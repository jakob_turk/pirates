#pragma once
#include "Text.h"

class HPtext :
	public Text
{
public:
	HPtext(std::string text, std::string followedObject, sf::Vector2f position, int textSize = 20,
		sf::Color textColor = sf::Color::Black);

	void Update(float elapsedTime);
private:
	std::string _followedObject;
};