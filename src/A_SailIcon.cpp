#include "stdafx.h"
#include "A_SailIcon.h"

A_SailIcon::A_SailIcon(sf::Vector2f position) :
	_offset(sf::Vector2f(-7.0f, -7.0f)),
	_atOffset(false)
{
	_frameLen = 0.5f;
	SetPosition(position + sf::Vector2f(3.0f, 3.0f));
	Load();
}

void A_SailIcon::Load()
{
	//Load Simple VisibleObject
	LoadFrameSprite("defaultSail", GetPosition());

	//Never auto add to Manager, this is UI
	Animation::Load(11, true);
}

void A_SailIcon::Update(float elapsedTime)
{
	_sinceFrameUpdate += elapsedTime;

	//Instead of changing frame as with usual animations, move to offset / back to original _position
	//Note that _position does not change while doing these updates
	if (_sinceFrameUpdate > _frameLen) {
		_sinceFrameUpdate = 0;
		if (_atOffset)
			_frames[0].setPosition(GetPosition());
		else
			_frames[0].setPosition(GetPosition() + _offset);
		_atOffset = !_atOffset;
	}
}
