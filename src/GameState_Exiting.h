#pragma once
#include "GameState.h"

class GS_Exiting :
	public GameState
{
public:
	GS_Exiting();
	~GS_Exiting();

	void Call();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& window);
};