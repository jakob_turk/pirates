#include "stdafx.h"
#include "GameState_Load.h"

#include "Game.h"
#include "BoatFactory.h"
#include "UIfactory.h"
#include "Scorer.h"
#include "ViewControll.h"
#include "Tutorial.h"

GS_Load::GS_Load() {
	_myGameStateType = Load;
}

GS_Load::~GS_Load() {
}

void GS_Load::Call()
{
	Game::GetGameObjectManager().Empty();

	std::ifstream levelFile("data/levels/test_level");
	std::string line;

	assert(levelFile.is_open());

	while (std::getline(levelFile, line)) {
		if (line[0] == '$') {
			if (line.substr(1) == "LEVEL_NAME") {

			}
			//Set up map
			////////////
			else if (line.substr(1) == "LEVEL_MAP") {
				std::getline(levelFile, line);

				int mapHeight = std::stoi(line);
				std::vector<std::vector<std::string>> map;

				for (int i = 0; i < mapHeight; ++i) {
					std::getline(levelFile, line);

					std::istringstream buffer(line);
					std::istream_iterator<std::string> begin(buffer), end;
					map.push_back(std::vector<std::string>(begin, end));
				}

				Game::GetGameObjectManager().GetTileManager()->Load(map);
			}
			else if (line.substr(1) == "LEVEL_OBJECTS") {

			}
		}
	}

	//Set up minimap
	////////////////
	float MMapWidth = 1 / 5.0f;
	float MMapHeight = ((float)Config::MAP_WIDTH / (float)Config::MAP_HEIGHT) * MMapWidth * 
					   ((float)Config::SCREEN_HEIGHT / (float)Config::SCREEN_WIDTH);
	ServiceLocator::GetViewer()->SetUpMinimapViewPort(MMapWidth, MMapHeight);

	//Make boats
	////////////
	//Player
	sf::Vector2f playerPosition;
	if (Config::TUTORIAL)
		playerPosition = sf::Vector2f(Config::MAP_WIDTH *3.0f / 8.0f, Config::MAP_HEIGHT / 4.0f);
	else
		playerPosition = sf::Vector2f(Config::MAP_WIDTH / 2.0f, Config::MAP_HEIGHT / 2.0f);
	Boat* playerBoat = BoatFactory::Make(playerPosition, Player, BoatFaction::fDefault,
										 0.0f, sf::Vector2f(), "PlayerBoat");
	UIfactory::Make(UIfactory::playerHPbar);
	Game::GetGameObjectManager().Add(new Scorer());
	Game::GetGameObjectManager().Add(new ViewControll());
	ServiceLocator::GetViewer()->MoveViewportTo(playerBoat->GetPosition());

	if (!Config::TUTORIAL) {

		//NPCs
		int nBoats = 0;
		for (int i = 1; i < Config::SANDBOX_FACTIONS_N + 1; ++i) {
			for (int j = 0; j < Config::SANDBOX_BOATS_N; ++j) {
				BoatFactory::Make(sf::Vector2f(100.0f + std::min((i) % 3, 1) * 2700.0f + j * 200.0f,
					100.0f + ((i) % 2) * 1400.0f),
					AI,
					BoatFaction(i % (BoatFaction::N_FACTIONS + 1)),
					inRad(90.0f + ((i) % 2)*180.0f),
					sf::Vector2f(),
					"AIBoat" + std::to_string(nBoats));
				nBoats++;
			}
		}
	
	}
	else {
		Game::GetGameObjectManager().Add(new Tutorial());
	}

	//Go to playing
	///////////////
	Game::ChangeGameState(GameState::Playing);
}

void GS_Load::Update(float elapsedTime) {}

void GS_Load::Draw(sf::RenderWindow& window) {}