#pragma once
#include "GameObject.h"

#include "Text.h"
#include "BackgroundPanel.h"
#include "Button.h"

class PopUp :
	public GameObject
{
public:
	PopUp(sf::Vector2f position, sf::Vector2f size, std::string text,
		std::function<void()> endFunction, int killStage = 0);

	void Update(float elapsedTime);
private:
	void Kill();

	BackgroundPanel* _popUpPanel;
	Text* _popUpText;
	Button* _okButton;
	std::function<void()> _okFunction;
	int _killStage;
};