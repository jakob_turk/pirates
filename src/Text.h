#pragma once

#include "VisibleObject.h"

class Text :
	public VisibleObject
{
public:
	Text(std::string text, sf::Vector2f position, int textSize = 20, sf::Color textColor = sf::Color::Black);

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow & renderWindow);

	std::string GetText();
	void UpdateText(std::string newText);
	void UpdatePosition(sf::Vector2f moveVector);
	void SetPosition(sf::Vector2f newPosition);
	sf::Vector2f GetPosition();

private:
	sf::Text _text;
	sf::Font _font;
};