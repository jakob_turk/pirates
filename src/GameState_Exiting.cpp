#include "stdafx.h"
#include "GameState_Exiting.h"

GS_Exiting::GS_Exiting() {
	_myGameStateType = Exiting;
}

GS_Exiting::~GS_Exiting() {
}

void GS_Exiting::Call() {}

void GS_Exiting::Update(float elapsedTime) {}

void GS_Exiting::Draw(sf::RenderWindow& window) {}