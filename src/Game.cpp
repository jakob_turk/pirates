#include "stdafx.h"
#include "Game.h"

#include "GameConfig.h"

#include "SFMLController.h"
#include "SFMLTextures.h"
#include "SFMLWindow.h"
#include "GameState_Uninitialized.h"
#include "GameState_Playing.h"
#include "GameState_Exiting.h"
#include "GameState_Load.h"
#include "GameState_Menu.h"

// STATIC MEMEBER INITIALIZATIONS
GameObjectManager Game::_gameObjectManager;
MenuManager Game::_menuManager;
sf::Clock Game::_clock;
std::map<GameState::GameStateType, GameState*> Game::_gameStates = { { GameState::Uninitialized, new GS_Unitialized},
																	 { GameState::Playing,		 new GS_Playing},
																	 { GameState::Exiting,		 new GS_Exiting},
																	 { GameState::Load,			 new GS_Load},
																	 { GameState::Menu,			 new GS_Menu}  };

GameState::GameStateType Game::_newGameState = GameState::Menu;
GameState* Game::_gameState = Game::_gameStates.find(GameState::Uninitialized)->second;

void Game::Start()
{
	if (_gameState->GetGameStateType() != GameState::Uninitialized) return;

	SFMLWindow viewer;
	SFMLController controller;
	SFMLTextures textures;

	ServiceLocator::RegisterService(&viewer);
	ServiceLocator::RegisterService(&controller);
	ServiceLocator::RegisterService(&textures);

	std::srand((int)std::time(0));

	_menuManager.LoadMenus();
	SetGameState(GameState::Menu);


	while (!IsExiting())
	{
		ServiceLocator::GetViewer()->GetWindow().clear();

		GameLoop();

		ServiceLocator::GetViewer()->GetWindow().display();
	}

	ServiceLocator::GetViewer()->GetWindow().close();

}

void Game::GameLoop()
{
	ServiceLocator::GetController()->PollEvents();

	_gameState->Update(_clock.restart().asSeconds());
	_gameState->Draw(ServiceLocator::GetViewer()->GetWindow());

	if (_newGameState != _gameState->GetGameStateType()) {
		SetGameState(_newGameState);
	}
}

GameState::GameStateType Game::GetGameState()
{
	return _gameState->GetGameStateType();
}

GameObjectManager& Game::GetGameObjectManager()
{
	return _gameObjectManager;
}

MenuManager& Game::GetMenuManager()
{
	return _menuManager;
}

void Game::ChangeGameState(GameState::GameStateType newGameState)
{
	if (newGameState != _gameState->GetGameStateType()) {
		_newGameState = newGameState;
	}
}

void Game::SetGameState(GameState::GameStateType newGameState)
{

	std::map<GameState::GameStateType, GameState*>::iterator findNewgamesState = _gameStates.find(newGameState);
	if (findNewgamesState != _gameStates.end()) {

		_gameState = findNewgamesState->second;
		_gameState->Call();

	}
}

bool Game::IsExiting()
{
	if (_gameState->GetGameStateType() == GameState::Exiting)
		return true;
	else 
		return false;
}
