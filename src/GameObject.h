#pragma once

class GameObject
{
public:
	GameObject();
	virtual ~GameObject();

	virtual bool IsLoaded() const;
	virtual bool WantsDeletion() const;
	virtual void SetDeletion();

	virtual void Update(float elapsedTime) = 0;

protected:
	virtual void SetLoaded();

private:
	bool _isLoaded;
	bool _wantsDeletion;
};