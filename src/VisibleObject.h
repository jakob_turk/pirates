#pragma once
#include "GameObject.h"

class VisibleObject :
	public GameObject
{
public:
	VisibleObject();
	virtual ~VisibleObject();

	virtual void Load(std::string mainTextureName);
	virtual void Draw(sf::RenderWindow & window);
	//virtual void Update(float elapsedTime) = 0; in GameObject makes this pure virtual

	virtual float GetWidth() const;
	virtual float GetHeight() const;
	virtual float GetRadius() const;
	virtual sf::FloatRect GetRect() const;
	virtual sf::Vector2f GetPosition() const;
	virtual float GetRotation() const;
	virtual int GetZLevel() const;
	virtual void SetPosition(float x, float y);
	virtual void SetPosition(sf::Vector2f position);
	virtual void SetRotation(float angle);
	virtual void SetZLevel(int zLevel);
	virtual void SetScale(sf::Vector2f scale);
	virtual void SetSize(sf::Vector2f size);
	virtual std::vector<sf::Vector2i> GetVertices();

	virtual sf::Sprite& GetSprite();

protected:
	void LoadTexture(std::string mainTextureName);
	void LoadTextureToSprite(std::string mainTextureName, sf::Sprite& sprite);
	virtual void SetVertices();
	virtual void SetRadius();
	std::vector<sf::Vector2i> _vertices;
	float _radius;

private:
	int _zLevel;
	sf::Sprite _sprite;
};
