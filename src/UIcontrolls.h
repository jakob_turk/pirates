#pragma once
#include "GameObject.h"

//AbilityButton only needed to be able to refister them
class AbilityButton;

enum Ability {
	None, SetNormalProj, SetFireProj, SetExplosiveProj, Anchor
};

class UIcontrolls:
	public GameObject
{
public:
	static void AddAbilityButton(AbilityButton* newAbilityButton);
	static std::vector<AbilityButton*> GetAbilityButtons();
	static void ClearAbilityButtons();

	static void ClickAbility(Ability ability);
	static std::vector<Ability> GetClickedAbilities();

private:
	static std::vector<Ability> _clickedAbilities;

	//Vector of all ability bottons still held, for button recoloring
	static std::vector<AbilityButton*> _abilityButtons;
};