#pragma once
#include "GameObject.h"

class Scorer :
	public GameObject
{
public:
	Scorer();

	void Update(float elapsedTime);
private:
	bool End();
	void Loose();
	void Win();
};