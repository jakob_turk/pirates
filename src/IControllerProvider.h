#pragma once

enum Key {
	Space, Shift, Esc, A, Q, W, E
};

class IControllerProvider
{
public:
	virtual void PollEvents() = 0;
	virtual std::vector<sf::Event>& GetEvents() = 0;	// That's...yeah...defeats the purpose...TO-DO: fix
	virtual sf::Vector2i GetMousePosition() = 0;
	virtual bool IsMousePressed() = 0;
	virtual bool IsKeyPressed(Key key) = 0;
	virtual float GetWheelScroll() = 0;

protected:

};