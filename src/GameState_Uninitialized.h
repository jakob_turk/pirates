#pragma once
#include "GameState.h"

class GS_Unitialized :
	public GameState
{
public:
	GS_Unitialized();
	~GS_Unitialized();

	void Call();

	void Update(float elapsedTime);
	void Draw(sf::RenderWindow& window);
};