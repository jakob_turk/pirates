#include "StdAfx.h"
#include "MovableObject.h"

MovableObject::MovableObject() :
	_speed(0.0f),
	_angle(0.0f),
	_startingMaxSpeed(0.0f),
	_maxSpeed(0.0f),
	_minSpeed(0.0f),
	_angularVel(0.0f),
	_maxAngleChange(0.3f),
	_startingFriction(0.0f),
	_friction(0.0f)
{}

MovableObject::~MovableObject() {}

float MovableObject::GetSpeed()
{
	return _speed;
}

float MovableObject::GetAngle()
{
	return _angle;
}

sf::Vector2f MovableObject::GetVelocity()
{
	return sf::Vector2f(std::cos(_angle) * _speed, std::sin(_angle) * _speed);
}

void MovableObject::SetVelocity(float newSpeed = 0.0f, float newAngle = 0.0f)
{
	if (newSpeed) {
		//Apply max speed
		if (_maxSpeed && newSpeed > _maxSpeed)
			newSpeed = _maxSpeed;

		_speed = newSpeed;
	}
	if (newAngle) {
		_angle = newAngle;
	}
}

void MovableObject::SetVelocity(sf::Vector2f newVelocity)
{
	float newSpeed = std::sqrt(newVelocity.x * newVelocity.x + newVelocity.y * newVelocity.y);
	float newAngle = std::atan2(newVelocity.y, newVelocity.x);

	SetVelocity(newSpeed, newAngle);
}

void MovableObject::SetStartingMaxSpeed(float startingMaxSpeed)
{
	_startingMaxSpeed = startingMaxSpeed;
}

void MovableObject::SetMaxSpeed(float maxSpeed)
{
	//Default negative maxSpeed means we take original max speed
	if (maxSpeed < 0.0f)
		_maxSpeed = _startingMaxSpeed;
	else
		_maxSpeed = maxSpeed;
}

void MovableObject::SetAngularVel(float maxAngularVel)
{
	_angularVel = maxAngularVel;
}

void MovableObject::SetStartingFriction(float friction)
{
	_startingFriction = friction;
}

void MovableObject::SetFriction(float friction)
{
	_friction = friction;
}

float MovableObject::GetStartingMaxSpeed()
{
	return _startingMaxSpeed;
}

float MovableObject::GetMaxSpeed()
{
	return _maxSpeed;
}

float MovableObject::GetStartingFriction()
{
	return _startingFriction;
}

void MovableObject::Update(float elapsedTime)
{
	//Apply friction
	if (_friction && _speed > _minSpeed)
		_speed -= _friction;

	//Set speed to 0 under a certain threshold
	//_minSpeed should in most cases just be a static value
	if (_speed < _minSpeed)
		_speed = 0.0f;

	//last minute max speed correction to deal with any errors...
	if (_speed > _maxSpeed)
		_speed = _maxSpeed;

	GetSprite().move(GetVelocity() * elapsedTime);
}

void MovableObject::Turn(float elapsedTime) {

	//Apply max angular velocity
	if (_maxAngleChange) {
		float diff = (float)((int)(inDeg(GetRotation() - _angle)) % 360);

		if		(diff < -180.0f) diff =  360.0f + diff;
		else if (diff >  180.0f) diff = -360.0f + diff;

		if (diff > _maxAngleChange) {
			_angle = GetRotation() - inRad(_angularVel * elapsedTime);
		}
		else if (diff < -_maxAngleChange) {
			_angle = GetRotation() + inRad(_angularVel * elapsedTime);
		}
		//else: dont change _angle
	}

	SetRotation(_angle);
}