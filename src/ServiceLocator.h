#pragma once
#include "IControllerProvider.h"
#include "IAudioProvider.h"
#include "ITextureProvider.h"
#include "IWindowProvider.h"

class ServiceLocator
{
public:
	enum Viewport {
		Default, Game, Minimap
	};

	static void RegisterService(IControllerProvider *provider);
	static void RegisterService(IAudioProvider *provider);
	static void RegisterService(ITextureProvider *provider);
	static void RegisterService(IWindowProvider *provider);

	static IControllerProvider* GetController();
	static IAudioProvider* GetAudio();
	static ITextureProvider* GetTextures();
	static IWindowProvider* GetViewer();

private:
	static IControllerProvider* _controllerProvider;
	static IAudioProvider* _audioProvider;
	static ITextureProvider* _textureProvider;
	static IWindowProvider* _windowProvider;
};