#include "stdafx.h"
#include "Cannon.h"

Cannon::Cannon() :
	_cannonType(Projectile::NormalProj)
{
	_ammo.insert(std::pair<Projectile::ProjectileType, std::pair<int, float>>
		(Projectile::NormalProj, std::pair<int, float>(10, 0.0f)));
	_ammo.insert(std::pair<Projectile::ProjectileType, std::pair<int, float>>
		(Projectile::FireProj, std::pair<int, float>(20, 0.0f)));
	_ammo.insert(std::pair<Projectile::ProjectileType, std::pair<int, float>>
		(Projectile::ExplosiveProj, std::pair<int, float>(3, 0.0f)));
}

void Cannon::SetCannonType(Projectile::ProjectileType type)
{
	_cannonType = type;
}

Projectile::ProjectileType Cannon::GetCannonType()
{
	return _cannonType;
}

void Cannon::AddAmmo(Projectile::ProjectileType typeToAdd, int amountToAdd)
{
	std::map<Projectile::ProjectileType, std::pair<int, float>>::iterator existingStock = _ammo.find(typeToAdd);
	if (existingStock == _ammo.end())
		return;
	else
		existingStock->second.first += amountToAdd;
}

int Cannon::GetAmmo(Projectile::ProjectileType type)
{
	std::map<Projectile::ProjectileType, std::pair<int, float>>::iterator ammoAmount = _ammo.find(type);
	if (ammoAmount == _ammo.end() || ammoAmount->second.first == 0)
		return 0;
	else
		return ammoAmount->second.first;
}

//Returns remaining CD time divided by full current full CD
//This returns ratio not an absolute value!
float Cannon::GetCurrentCD()
{
	float fullCD;
	switch (_cannonType) {
	case Projectile::NormalProj:
		if (_ammo[Projectile::NormalProj].first == 0)
			fullCD = 2.0f;
		else
			fullCD = 0.25f;
		break;
	case Projectile::FireProj:
		fullCD = 0.7f;
		break;
	case Projectile::ExplosiveProj:
		fullCD = 2.0f;
		break;
	default:
		fullCD = 1.0f;
		break;
	}

	return _ammo[_cannonType].second / fullCD;
}

void Cannon::Shoot(sf::Vector2f position, sf::Vector2f target, BoatFaction ownerFaction, int accuracy)
{
	std::map<Projectile::ProjectileType, std::pair<int, float>>::iterator shotProj = _ammo.find(_cannonType);
	if (shotProj == _ammo.end() || shotProj->second.first == 0)
		return;

	//Reduce ammo
	/////////////
	else if (shotProj->second.first > 0) {
		//Only decrease ammo amount if above 0; -1 thus means infinity
		//_CDs and _ammo must always have identical
		if (shotProj->second.second > 0.0f)
			return;

		shotProj->second.first -= 1;
	}

	//Make the projectile
	/////////////////////
	//Inaccuracy
	float offTarget = 0.0f;
	if (accuracy != 0)
		offTarget = (float)(std::rand() % (accuracy + 1)) - accuracy / 2.0f;
	//New Projectile
	new Projectile(position,
				   std::atan2(target.y - position.y, target.x - position.x) + inRad(offTarget),
				   ownerFaction,
				   _cannonType);

	//Set cooldown
	//////////////
	switch (_cannonType) {
	case Projectile::NormalProj:
		if (shotProj->second.first == 0)
			shotProj->second.second = 2.0f;
		else
			shotProj->second.second = 0.25f;
		break;
	case Projectile::FireProj:
	{
		shotProj->second.second = 0.7f;
		break;
	}
	case Projectile::ExplosiveProj:
		shotProj->second.second = 2.0f;
		break;
	}
}

void Cannon::Update(float elapsedTime)
{
	for (std::map<Projectile::ProjectileType, std::pair<int, float>>::iterator ammoType = _ammo.begin(); ammoType != _ammo.end(); ammoType++) {
		if (ammoType->second.second > 0.0f) {
			if (ammoType->first == Projectile::NormalProj) {
				ammoType->second.second -= elapsedTime;

				if (ammoType->second.second < 0.0f) {
					ammoType->second.second = 0.0f;

					//Reload normal ammo if at 0 normal ammo and CD over
					if (ammoType->second.first == 0)
						AddAmmo(Projectile::NormalProj, 10);
				}

			}
			else
				//For other ammos just reduce CD
				ammoType->second.second = std::max(0.0f, ammoType->second.second - elapsedTime);
		}
	}
}